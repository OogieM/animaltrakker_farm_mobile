package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AnimalParentage(
    val sireAnimalId: Int,
    val sireName: String,
    val sireOwnerType: Owner.Type,
    val sireOwnerId: Int,
    val sireOwnerName: String,
    val sireFlockPrefixId: Int,
    val sireFlockPrefix: String,
    val damAnimalId: Int,
    val damName: String,
    val damOwnerType: Owner.Type,
    val damOwnerId: Int,
    val damOwnerName: String,
    val damFlockPrefixId: Int,
    val damFlockPrefix: String,
) : Parcelable
