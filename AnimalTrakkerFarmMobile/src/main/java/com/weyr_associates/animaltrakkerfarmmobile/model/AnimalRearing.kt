package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AnimalRearing(
    val birthOrder: Int,
    val birthTypeId: Int,
    val birthType: String,
    val rearTypeId: Int,
    val rearType: String?
) : Parcelable
