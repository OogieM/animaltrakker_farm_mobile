package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UnitOfMeasure(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val type: Type,
    val order: Int = Int.MAX_VALUE
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation {
    companion object {

        const val SALE_PRICE_US_DOLLARS = 8

        const val TIME_UNIT_DAYS = 5
        const val TIME_UNIT_HOURS = 6
        const val TIME_UNIT_YEARS = 21
        const val TIME_UNIT_MONTHS = 22
        const val TIME_UNIT_WEEKS = 23
        const val TIME_UNIT_SECONDS = 24

        val NONE = UnitOfMeasure(
            id = 0,
            name = "None",
            abbreviation = "None",
            type = Type.NONE,
            order = Int.MAX_VALUE
        )
    }

    @Parcelize
    data class Type(
        val id: Int,
        val name: String,
        val order: Int = Int.MAX_VALUE
    ) : Parcelable {
        companion object {
            val NONE = Type(
                id = 0,
                name = "None",
                order = Int.MAX_VALUE
            )
            const val ID_WEIGHT = 1
            const val ID_CURRENCY = 3
            const val ID_TIME = 4
        }
        val isWeight: Boolean
            get() = when (id) {
                ID_WEIGHT -> true
                else -> false
            }
    }
}
