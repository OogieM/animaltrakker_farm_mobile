package com.weyr_associates.animaltrakkerfarmmobile.model

object EvalTraitType {
    const val ID_SCORED_1_TO_5 = 1
    const val ID_REAL_VALUE = 2
    const val ID_USER_LIST = 3
    const val ID_REAL_VALUE_CALCULATED = 4
}
