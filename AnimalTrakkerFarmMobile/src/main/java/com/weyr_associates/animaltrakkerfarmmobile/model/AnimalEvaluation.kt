package com.weyr_associates.animaltrakkerfarmmobile.model

import java.time.LocalDate
import java.time.LocalTime

data class AnimalEvaluation(
    override val id: Int,
    val animalId: Int,
    val traits: List<Entry>,
    val rank: Int,
    val numberRanked: Int,
    val evalDate: LocalDate,
    val evalTime: LocalTime,
) : HasIdentity<Int> {

    sealed interface Entry : HasIdentity<Int> {
        override val id get() = traitId
        val traitId: Int
        val traitName: String
    }

    data class ScoreEntry(
        override val traitId: Int,
        override val traitName: String,
        val traitScore: Int
    ) : Entry

    data class UnitsEntry(
        override val traitId: Int,
        override val traitName: String,
        val traitScore: Float,
        val unitsId: Int,
        val unitsAbbreviation: String
    ) : Entry

    data class OptionEntry(
        override val traitId: Int,
        override val traitName: String,
        val optionId: Int,
        val optionName: String
    ) : Entry
}
