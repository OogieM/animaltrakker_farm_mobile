package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Trait(
    override val id: Int,
    override val name: String,
    val typeId: Int,
    val unitsTypeId: Int,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName {
    companion object {
        const val TYPE_ID_BASIC = 1
        const val TYPE_ID_UNIT = 2
        const val TYPE_ID_OPTION = 3
    }
}
