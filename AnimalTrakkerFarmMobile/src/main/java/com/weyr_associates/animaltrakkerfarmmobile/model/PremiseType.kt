package com.weyr_associates.animaltrakkerfarmmobile.model

object PremiseType {
    const val ID_PHYSICAL = 1
    const val ID_MAILING = 2
    const val ID_BOTH = 3
}
