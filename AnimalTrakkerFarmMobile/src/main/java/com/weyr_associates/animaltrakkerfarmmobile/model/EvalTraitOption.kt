package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EvalTraitOption(
    override val id: Int,
    val traitId: Int,
    override val name: String,
    val order: Int = Int.MAX_VALUE
) : Parcelable, HasIdentity<Int>, HasName {
    companion object {
        const val ID_SIMPLE_SORT_KEEP = 40
        const val ID_SIMPLE_SORT_SHIP = 41
        const val ID_SIMPLE_SORT_CULL = 42
        const val ID_SIMPLE_SORT_OTHER = 43

        const val ID_CUSTOM_SCROTAL_PALPATION_SATISFACTORY = 44
    }
}
