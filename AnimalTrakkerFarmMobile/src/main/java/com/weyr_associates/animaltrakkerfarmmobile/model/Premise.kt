package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Premise(
    override val id: Int
) : Parcelable, HasIdentity<Int> {

    companion object {
        const val ID_PREMISE_UNKNOWN = 82
    }
}
