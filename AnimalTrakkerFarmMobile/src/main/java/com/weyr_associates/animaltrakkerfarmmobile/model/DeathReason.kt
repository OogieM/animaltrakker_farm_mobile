package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeathReason(
    override val id: Int,
    val reason: String,
    val userId: Int,
    val userType: UserType,
    val order: Int
) : Parcelable, HasIdentity<Int> {
    companion object {
        const val ID_DEATH_REASON_UNKNOWN = 14
        const val DEATH_REASON_MISSING = "Death Reason Missing"
    }
}
