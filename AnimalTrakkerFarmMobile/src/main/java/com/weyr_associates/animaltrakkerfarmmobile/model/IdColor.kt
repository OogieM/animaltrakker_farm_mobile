package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdColor(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation {
    companion object {
        const val ID_COLOR_ID_UNKNOWN = 14
        const val ID_COLOR_ID_NOT_APPLICABLE = 15
    }
}
