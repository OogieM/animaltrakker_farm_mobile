package com.weyr_associates.animaltrakkerfarmmobile.model

sealed interface EvalTrait {
    val id: Int
    val name: String
    val typeId: Int
    val order: Int
    val isEmpty: Boolean
    val isOptional: Boolean
    val isDeferred: Boolean

    companion object {
        const val TRAIT_ID_RAM_LAMBS_BORN = 68
        const val TRAIT_ID_WETHER_LAMBS_BORN = 69
        const val TRAIT_ID_EWE_LAMBS_BORN = 70
        const val TRAIT_ID_UNK_SEX_LAMBS_BORN = 71
        const val TRAIT_ID_STILL_BORN_LAMBS_BORN = 72
        const val TRAIT_ID_ABORTED_LAMBS = 73
        const val TRAIT_ID_ADOPTED_LAMBS = 74
        const val UNIT_TRAIT_ID_SCROTAL_CIRCUMFERENCE = 15
        const val UNIT_TRAIT_ID_WEIGHT = 16
        const val UNIT_TRAIT_ID_BODY_CONDITION_SCORE = 51
        const val OPTION_TRAIT_ID_SIMPLE_SORT = 54
        const val OPTION_TRAIT_ID_CUSTOM_SCROTAL_PALPATION = 80
    }
}

data class BasicEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = BasicEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false
        )
    }
}

data class UnitsEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val units: UnitOfMeasure,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE,
) : EvalTrait {
    companion object {
        val EMPTY = UnitsEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            units = UnitOfMeasure.NONE
        )
    }
}

data class CustomEvalTrait(
    override val id: Int,
    override val name: String,
    override val typeId: Int,
    override val isOptional: Boolean,
    override val isDeferred: Boolean,
    val options: List<EvalTraitOption>,
    override val isEmpty: Boolean = false,
    override val order: Int = Int.MAX_VALUE
) : EvalTrait {
    companion object {

        val EMPTY = CustomEvalTrait(
            id = 0,
            name = "N/A",
            typeId = EvalTraitType.ID_SCORED_1_TO_5,
            isEmpty = true,
            isOptional = false,
            isDeferred = false,
            options = emptyList()
        )
    }
}
