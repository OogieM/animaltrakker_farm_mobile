package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.LocalDate

@Parcelize
data class Drug(
    override val id: Int,
    val typeId: Int,
    val officialName: String,
    val genericName: String,
    val lot: String,
    val isGone: Boolean,
    val isRemovable: Boolean,
    val meatWithdrawal: Int,
    val meatWithdrawalUnitsId: Int,
    val userMeatWithdrawal: Int,
    val milkWithdrawal: Int,
    val milkWithdrawalUnitsId: Int,
    val userMilkWithdrawal: Int,
    val officialDrugDosage: String,
    val userDrugDosage: String,
    val userTaskName: String
) : Parcelable, HasIdentity<Int>, HasName {

    override val name: String
        get() = genericName

    val nameAndLot: String
        get() = "$name Lot $lot"
}
