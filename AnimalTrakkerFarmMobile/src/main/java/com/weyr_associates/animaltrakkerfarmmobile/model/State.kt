package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class State(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val countryId: Int,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation
