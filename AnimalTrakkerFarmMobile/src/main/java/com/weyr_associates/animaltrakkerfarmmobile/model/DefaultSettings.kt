package com.weyr_associates.animaltrakkerfarmmobile.model

data class DefaultSettings(
    val id: Int,
    val name: String,
    val ownerContactId: Int,
    val ownerCompanyId: Int,
    val ownerPremiseId: Int,
    val breederContactId: Int,
    val breederCompanyId: Int,
    val breederPremiseId: Int,
    val vetContactId: Int,
    val vetPremiseId: Int,
    val labCompanyId: Int,
    val labPremiseId: Int,
    val registryCompanyId: Int,
    val registryPremiseId: Int,
    val stateId: Int,
    val countyId: Int,
    val flockPrefixId: Int,
    val speciesId: Int,
    val breedId: Int,
    val sexId: Int,
    val idTypeIdPrimary: Int,
    val idTypeIdSecondary: Int,
    val idTypeIdTertiary: Int,
    val eidKeepMaleFemaleIdColorSame: Boolean,
    val eidColorMale: Int,
    val eidColorFemale: Int,
    val eidIdLocation: Int,
    val farmKeepMaleFemaleIdColorSame: Boolean,
    val farmIdBasedOnEid: Boolean,
    val farmIdNumberDigitsFromEid: Boolean,
    val farmIdColorMale: Int,
    val farmIdColorFemale: Int,
    val farmIdLocation: Int,
    val fedKeepMaleFemaleIdColorSame: Boolean,
    val fedIdColorMale: Int,
    val fedIdColorFemale: Int,
    val fedIdLocation: Int,
    val nuesMaleFemaleKeepIdColorSame: Boolean,
    val nuesIdColorMale: Int,
    val nuesIdColorFemale: Int,
    val nuesIdLocation: Int,
    val trichKeepMaleFemaleIdColorSame: Boolean,
    val trichIdColorMale: Int,
    val trichIdColorFemale: Int,
    val trichIdLocation: Int,
    val trichIdAutoIncrement: Boolean,
    val trichNextIdNumber: Int,
    val bangsIdKeepMaleFemaleIdColorSame: Boolean,
    val bangsIdColorMale: Int,
    val bangsIdColorFemale: Int,
    val bangsIdLocation: Int,
    val saleOrderKeepMaleFemaleIdColorSame: Boolean,
    val saleOrderIdColorMale: Int,
    val saleOrderIdColorFemale: Int,
    val saleOrderIdLocation: Int,
    val usePaintMarks: Boolean,
    val paintMarkColor: Int,
    val paintMarkLocation: Int,
    val tattooColor: Int,
    val tattooLocation: Int,
    val freezeBrandLocation: Int,
    val removeReasonId: Int,
    val tissueSampleTypeId: Int,
    val tissueTestId: Int,
    val tissueSampleContainerTypeId: Int,
    val birthTypeId: Int,
    val rearTypeId: Int,
    val minimumBirthWeight: Float,
    val maximumBirthWeight: Float,
    val birthWeightUnitsId: Int,
    val weightUnitsId: Int,
    val salePriceUnitsId: Int,
    val evaluationUpdateAlert: Int,
    val deathReasonId: Int,
    val deathReasonContactId: Int,
    val deathReasonCompanyId: Int,
    val transferReasonId: Int,
    val transferReasonContactId: Int,
    val transferReasonCompanyId: Int,
    val userSystemSerialNumber: Int
) {
    companion object {
        const val SETTINGS_ID_DEFAULT = 1
        const val SPECIES_ID_DEFAULT = 1
        const val ID_COLOR_ID_DEFAULT = IdColor.ID_COLOR_ID_NOT_APPLICABLE //YELLOW
        const val ID_LOCATION_ID_DEFAULT = IdLocation.ID_LOCATION_ID_UNKNOWN
    }

    val userId: Int?
        get() = when {
            ownerContactId != 0 -> ownerContactId
            ownerCompanyId != 0 -> ownerCompanyId
            else -> null
        }

    val userType: UserType?
        get() = when {
            ownerContactId != 0 -> UserType.CONTACT
            ownerCompanyId != 0 -> UserType.COMPANY
            else -> null
        }

    val ownerId: Int?
        get() = when {
            ownerContactId != 0 -> ownerContactId
            ownerCompanyId != 0 -> ownerCompanyId
            else -> null
        }

    val ownerType: Int?
        get() = when {
            ownerContactId != 0 -> Owner.Type.CONTACT.id
            ownerCompanyId != 0 -> Owner.Type.COMPANY.id
        else -> null
    }

    val breederId: Int?
        get() = when {
            breederContactId != 0 -> breederContactId
            breederCompanyId != 0 -> breederCompanyId
            else -> null
        }

    val breederType: Int?
        get() = when {
            breederContactId != 0 -> Breeder.Type.CONTACT.id
            breederCompanyId != 0 -> Breeder.Type.COMPANY.id
            else -> null
        }

    fun defaultIdColorFromIdType(idTypeId: Int): Int =
        when (idTypeId) {
            IdType.ID_TYPE_ID_FED -> fedIdColorMale
            IdType.ID_TYPE_ID_EID -> eidColorMale
            IdType.ID_TYPE_ID_PAINT -> paintMarkColor
            IdType.ID_TYPE_ID_FARM -> farmIdColorMale
            IdType.ID_TYPE_ID_TATTOO -> tattooColor
            IdType.ID_TYPE_ID_TRICH -> trichIdColorMale
            IdType.ID_TYPE_ID_NUES -> nuesIdColorMale
            IdType.ID_TYPE_ID_SALE_ORDER -> saleOrderIdColorMale
            IdType.ID_TYPE_ID_BANGS -> bangsIdColorMale
            else -> ID_COLOR_ID_DEFAULT
        }

    fun defaultIdLocationFromIdType(idTypeId: Int): Int =
        when (idTypeId) {
            IdType.ID_TYPE_ID_FED -> fedIdLocation
            IdType.ID_TYPE_ID_EID -> eidIdLocation
            IdType.ID_TYPE_ID_PAINT -> paintMarkLocation
            IdType.ID_TYPE_ID_FARM -> farmIdLocation
            IdType.ID_TYPE_ID_TATTOO -> tattooLocation
            IdType.ID_TYPE_ID_TRICH -> trichIdLocation
            IdType.ID_TYPE_ID_NUES -> nuesIdLocation
            IdType.ID_TYPE_ID_SALE_ORDER -> saleOrderIdLocation
            IdType.ID_TYPE_ID_BANGS -> bangsIdLocation
            else -> ID_LOCATION_ID_DEFAULT
        }
}
