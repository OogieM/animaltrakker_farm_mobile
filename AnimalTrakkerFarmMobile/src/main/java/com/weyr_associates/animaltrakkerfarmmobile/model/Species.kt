package com.weyr_associates.animaltrakkerfarmmobile.model

data class Species(
    val id: Int,
    val commonName: String
) {
    companion object {

        const val ID_SHEEP = 1
        const val ID_GOAT = 2
        const val ID_CATTLE = 3
        const val ID_HORSE = 4
        const val ID_DONKEY = 5
        const val ID_PIG = 6

        const val ANIMAL_ID_UNKNOWN_SIRE_SHEEP = 14754
        const val ANIMAL_ID_UNKNOWN_SIRE_GOAT = 14854
        const val ANIMAL_ID_UNKNOWN_SIRE_CATTLE = 14856
        const val ANIMAL_ID_UNKNOWN_SIRE_HORSE = 14858
        const val ANIMAL_ID_UNKNOWN_SIRE_DONKEY = 14860
        const val ANIMAL_ID_UNKNOWN_SIRE_PIG = 14862

        const val ANIMAL_ID_UNKNOWN_DAM_SHEEP = 14755
        const val ANIMAL_ID_UNKNOWN_DAM_GOAT = 14855
        const val ANIMAL_ID_UNKNOWN_DAM_CATTLE = 14857
        const val ANIMAL_ID_UNKNOWN_DAM_HORSE = 14859
        const val ANIMAL_ID_UNKNOWN_DAM_DONKEY = 14861
        const val ANIMAL_ID_UNKNOWN_DAM_PIG = 14863

        fun unknownSireIdForSpecies(speciesId: Int): Int = when (speciesId) {
            ID_SHEEP -> ANIMAL_ID_UNKNOWN_SIRE_SHEEP
            ID_GOAT -> ANIMAL_ID_UNKNOWN_SIRE_GOAT
            ID_CATTLE -> ANIMAL_ID_UNKNOWN_SIRE_CATTLE
            ID_HORSE -> ANIMAL_ID_UNKNOWN_SIRE_HORSE
            ID_DONKEY -> ANIMAL_ID_UNKNOWN_SIRE_DONKEY
            ID_PIG -> ANIMAL_ID_UNKNOWN_SIRE_PIG
            else -> 0
        }

        fun unknownDamIdForSpecies(speciesId: Int): Int = when (speciesId) {
            ID_SHEEP -> ANIMAL_ID_UNKNOWN_DAM_SHEEP
            ID_GOAT -> ANIMAL_ID_UNKNOWN_DAM_GOAT
            ID_CATTLE -> ANIMAL_ID_UNKNOWN_DAM_CATTLE
            ID_HORSE -> ANIMAL_ID_UNKNOWN_DAM_HORSE
            ID_DONKEY -> ANIMAL_ID_UNKNOWN_DAM_DONKEY
            ID_PIG -> ANIMAL_ID_UNKNOWN_DAM_PIG
            else -> 0
        }
    }
}
