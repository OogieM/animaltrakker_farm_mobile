package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Contact(
    override val id: Int,
    val firstName: String,
    val middleName: String,
    val lastName: String,
    val titleId: Int
) : Parcelable, HasIdentity<Int>, HasName {

    override val name: String
        get() = firstAndLastName

    val firstAndLastName: String
        get() = "$firstName $lastName"
}
