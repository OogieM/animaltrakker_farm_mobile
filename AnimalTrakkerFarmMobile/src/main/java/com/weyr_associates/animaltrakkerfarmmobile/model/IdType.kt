package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdType(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation {

    companion object {

        const val ID_TYPE_ID_FED = 1
        const val ID_TYPE_ID_EID = 2
        const val ID_TYPE_ID_PAINT = 3
        const val ID_TYPE_ID_FARM = 4
        const val ID_TYPE_ID_TATTOO = 5
        const val ID_TYPE_ID_SPLIT = 6
        const val ID_TYPE_ID_NOTCH = 7
        const val ID_TYPE_ID_NAME = 8
        const val ID_TYPE_ID_FREEZE_BRAND = 9
        const val ID_TYPE_ID_TRICH = 10
        const val ID_TYPE_ID_NUES = 11
        const val ID_TYPE_ID_SALE_ORDER = 12
        const val ID_TYPE_ID_BANGS = 13
        const val ID_TYPE_ID_UNKNOWN = 14
        const val ID_TYPE_ID_CARCASS_TAG = 15
        const val ID_TYPE_ID_FED_CANADIAN = 16

        fun isOfficialId(id: Int): Boolean = when (id) {
            ID_TYPE_ID_FED,
            ID_TYPE_ID_NUES,
            ID_TYPE_ID_BANGS -> true
            else -> false
        }
    }
}
