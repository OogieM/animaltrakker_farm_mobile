package com.weyr_associates.animaltrakkerfarmmobile.model

object Breeder {

    const val TYPE_ID_CONTACT = 0
    const val TYPE_ID_COMPANY = 1

    enum class Type(val id: Int) {
        CONTACT(TYPE_ID_CONTACT),
        COMPANY(TYPE_ID_COMPANY);

        companion object {
            fun fromId(id: Int): Type = when(id) {
                CONTACT.id -> CONTACT
                COMPANY.id -> COMPANY
                else -> throw IllegalArgumentException(
                    "$id is not a valid code for ${Type::class.simpleName}."
                )
            }
        }
    }
}
