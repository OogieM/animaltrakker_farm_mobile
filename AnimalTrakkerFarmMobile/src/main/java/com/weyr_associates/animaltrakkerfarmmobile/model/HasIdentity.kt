package com.weyr_associates.animaltrakkerfarmmobile.model

interface HasIdentity<T> where T : Comparable<T> {
    val id: T
}
