package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Breed(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val order: Int,
    val speciedId: Int
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation
