package com.weyr_associates.animaltrakkerfarmmobile.model

object BirthType {
    const val ID_UNKNOWN = 42
    const val BIRTH_TYPE_MISSING = "Missing Birth Type"
    const val REAR_TYPE_MISSING = "Missing Rear Type"
}
