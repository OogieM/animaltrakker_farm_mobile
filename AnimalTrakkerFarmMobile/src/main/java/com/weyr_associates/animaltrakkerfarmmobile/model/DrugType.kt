package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DrugType(
    override val id: Int,
    override val name: String,
    val order: Int
) : Parcelable, HasIdentity<Int>, HasName {
    companion object {
        const val ID_DEWORMER = 1
        const val ID_VACCINE = 2
        const val ID_ANTIBIOTIC = 3
        const val ID_HORMONE = 4
        const val ID_COCCIDIOSTAT = 5
        const val ID_FEED_SUPPLEMENT = 6
        const val ID_ANALGESIC = 7
    }
}
