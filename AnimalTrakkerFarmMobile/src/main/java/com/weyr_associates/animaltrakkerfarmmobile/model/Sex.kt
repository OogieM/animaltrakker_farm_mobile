package com.weyr_associates.animaltrakkerfarmmobile.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sex(
    override val id: Int,
    override val name: String,
    override val abbreviation: String,
    val standard: String,
    val standardAbbreviation: String,
    val order: Int,
    val speciesId: Int
) : Parcelable, HasIdentity<Int>, HasName, HasAbbreviation {
    companion object {
        const val ID_SHEEP_RAM = 1
        const val ID_SHEEP_EWE = 2
        const val ID_SHEEP_WETHER = 3
        const val ID_SHEEP_UNKNOWN = 4
        const val ID_GOAT_BUCK = 5
        const val ID_GOAT_DOE = 6
        const val ID_GOAT_WETHER = 7
        const val ID_GOAT_UNKNOWN = 8
        const val ID_CATTLE_BULL = 9
        const val ID_CATTLE_COW = 10
        const val ID_CATTLE_STEER = 11
        const val ID_CATTLE_UNKNOWN = 12
        const val ID_HORSE_STALLION = 13
        const val ID_HORSE_MARE = 14
        const val ID_HORSE_GELDING = 15
        const val ID_HORSE_UNKNOWN = 16
        const val ID_DONKEY_JACK = 17
        const val ID_DONKEY_JENNY = 18
        const val ID_DONKEY_GELDING = 19
        const val ID_DONKEY_UNKNOWN = 20
        const val ID_PIG_BOAR = 21
        const val ID_PIG_SOW = 22
        const val ID_PIG_BARROW = 23
        const val ID_PIG_UNKNOWN = 24

        fun speciesIdFromSexId(sexId: Int): Int = when (sexId) {
            ID_SHEEP_RAM,
            ID_SHEEP_EWE,
            ID_SHEEP_WETHER,
            ID_SHEEP_UNKNOWN -> Species.ID_SHEEP
            ID_GOAT_BUCK,
            ID_GOAT_DOE,
            ID_GOAT_WETHER,
            ID_GOAT_UNKNOWN -> Species.ID_GOAT
            ID_CATTLE_BULL,
            ID_CATTLE_COW,
            ID_CATTLE_STEER,
            ID_CATTLE_UNKNOWN -> Species.ID_CATTLE
            ID_HORSE_STALLION,
            ID_HORSE_MARE,
            ID_HORSE_GELDING,
            ID_HORSE_UNKNOWN -> Species.ID_HORSE
            ID_DONKEY_JACK,
            ID_DONKEY_JENNY,
            ID_DONKEY_GELDING,
            ID_DONKEY_UNKNOWN -> Species.ID_DONKEY
            ID_PIG_BOAR,
            ID_PIG_SOW,
            ID_PIG_BARROW,
            ID_PIG_UNKNOWN -> Species.ID_PIG
            else -> 0
        }

        fun isFemale(sexId: Int): Boolean = when (sexId) {
            ID_SHEEP_EWE,
            ID_GOAT_DOE,
            ID_CATTLE_COW,
            ID_HORSE_MARE,
            ID_DONKEY_JENNY,
            ID_PIG_SOW -> true
            else -> false
        }

        fun isMale(sexId: Int): Boolean = when (sexId) {
            ID_SHEEP_RAM,
            ID_GOAT_BUCK,
            ID_CATTLE_BULL,
            ID_HORSE_STALLION,
            ID_DONKEY_JACK,
            ID_PIG_BOAR -> true
            else -> false
        }

        fun isCastrate(sexId: Int): Boolean = when (sexId) {
            ID_SHEEP_WETHER,
            ID_GOAT_WETHER,
            ID_CATTLE_STEER,
            ID_HORSE_GELDING,
            ID_DONKEY_GELDING,
            ID_PIG_BARROW -> true
            else -> false
        }
    }
}
