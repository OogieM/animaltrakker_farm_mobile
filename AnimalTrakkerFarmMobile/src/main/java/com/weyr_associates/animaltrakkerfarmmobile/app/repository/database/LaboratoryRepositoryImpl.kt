package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.LaboratoryRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.CompanyLaboratoryTable
import com.weyr_associates.animaltrakkerfarmmobile.database.CompanyTable
import com.weyr_associates.animaltrakkerfarmmobile.database.getInt
import com.weyr_associates.animaltrakkerfarmmobile.database.getString
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory

class LaboratoryRepositoryImpl(private val databaseHandler: DatabaseHandler) : LaboratoryRepository {

    companion object {
        private const val SQL_BASE_QUERY_LABORATORY_TABLE =
            """SELECT ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.ID}, 
                    ${CompanyTable.NAME}.${CompanyTable.Columns.ID}, 
                    ${CompanyTable.NAME}.${CompanyTable.Columns.NAME}, 
                    ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.LICENSE_NUMBER},
                    ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.ORDER}
                FROM ${CompanyLaboratoryTable.NAME}
                INNER JOIN ${CompanyTable.NAME}
                    ON ${CompanyTable.NAME}.${CompanyTable.Columns.ID} = ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.COMPANY_ID}"""

        const val SQL_QUERY_LABORATORIES =
            """$SQL_BASE_QUERY_LABORATORY_TABLE
                ORDER BY ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.ORDER}"""

        const val SQL_QUERY_LABORATORY_BY_ID =
            """$SQL_BASE_QUERY_LABORATORY_TABLE
                WHERE ${CompanyLaboratoryTable.NAME}.${CompanyLaboratoryTable.Columns.ID} = ?"""

        const val SQL_QUERY_LABORATORY_BY_COMPANY_ID =
            """$SQL_BASE_QUERY_LABORATORY_TABLE
                WHERE ${CompanyTable.NAME}.${CompanyTable.Columns.ID} = ?"""

        fun laboratoryFrom(cursor: Cursor): Laboratory {
            return Laboratory(
                id = cursor.getInt(CompanyLaboratoryTable.Columns.ID),
                companyId = cursor.getInt(CompanyLaboratoryTable.Columns.COMPANY_ID),
                name = cursor.getString(CompanyTable.Columns.NAME),
                licenseNumber = cursor.getString(CompanyLaboratoryTable.Columns.LICENSE_NUMBER),
                order = cursor.getInt(CompanyLaboratoryTable.Columns.ORDER)
            )
        }
    }

    override fun queryLaboratories(): List<Laboratory> {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_LABORATORIES,
            emptyArray()
        )?.use { cursor ->
            return cursor.readAllItems(::laboratoryFrom)
        } ?: emptyList()
    }

    override fun queryLaboratoryById(id: Int): Laboratory? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_LABORATORY_BY_ID,
            arrayOf(id.toString())
        )?.use { cursor ->
            cursor.readFirstItem(::laboratoryFrom)
        }
    }

    override fun queryLaboratoryByCompanyId(companyId: Int): Laboratory? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_LABORATORY_BY_COMPANY_ID,
            arrayOf(companyId.toString())
        )?.use { cursor ->
            cursor.readFirstItem(::laboratoryFrom)
        }
    }
}
