package com.weyr_associates.animaltrakkerfarmmobile.app.core.select

object SelectItem {
    const val EXTRA_REQUEST_KEY = "EXTRA_REQUEST_KEY"
    const val EXTRA_IS_OPTIONAL = "EXTRA_IS_OPTIONAL"
    const val EXTRA_ASSOCIATED_DATA = "EXTRA_ASSOCIATED_DATE"

    const val EXTRA_RESULT = "EXTRA_RESULT"
    const val EXTRA_SELECTED_ITEM = "EXTRA_SELECTED_ITEM"

    const val RESULT_ITEM_SELECTED = 1
    const val RESULT_ITEM_CLEARED = 0
}
