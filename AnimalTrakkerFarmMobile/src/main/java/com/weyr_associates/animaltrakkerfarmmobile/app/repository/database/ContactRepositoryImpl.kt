package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.repository.ContactRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.ContactTable
import com.weyr_associates.animaltrakkerfarmmobile.database.ContactVeterinarianTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Contact

class ContactRepositoryImpl(private val databaseHandler: DatabaseHandler) : ContactRepository {

    companion object {
        private const val SQL_QUERY_CONTACTS =
            """SELECT * FROM ${ContactTable.NAME}
                ORDER BY ${ContactTable.Columns.LAST_NAME} ASC"""

        private const val SQL_QUERY_CONTACT_BY_ID =
            """SELECT * FROM ${ContactTable.NAME}
                WHERE ${ContactTable.Columns.ID} = ?"""

        private const val SQL_QUERY_VETERINARIANS =
            """SELECT * FROM ${ContactTable.NAME}
                JOIN ${ContactVeterinarianTable.NAME}
                ON ${ContactTable.NAME}.${ContactTable.Columns.ID} = ${ContactVeterinarianTable.NAME}.${ContactVeterinarianTable.Columns.CONTACT_ID}
                ORDER BY ${ContactTable.Columns.LAST_NAME} ASC"""
    }

    override fun queryContacts(): List<Contact> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_CONTACTS, null)
            ?.use { it.readAllItems(ContactTable::contactFromCursor) } ?: emptyList()
    }

    override fun queryVeterinarians(): List<Contact> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_VETERINARIANS, null)
            ?.use { it.readAllItems(ContactTable::contactFromCursor) } ?: emptyList()
    }

    override fun queryContact(id: Int): Contact? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_CONTACT_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(ContactTable::contactFromCursor) }
    }
}
