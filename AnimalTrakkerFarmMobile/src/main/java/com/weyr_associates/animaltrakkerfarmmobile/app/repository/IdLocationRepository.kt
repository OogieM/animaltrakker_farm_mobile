package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation

interface IdLocationRepository {
    fun queryIdLocations(): List<IdLocation>

    fun queryIdLocation(id: Int): IdLocation?
}
