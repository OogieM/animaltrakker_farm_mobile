package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.EvaluationConfiguration
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry
import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.Trait
import com.weyr_associates.animaltrakkerfarmmobile.model.UserType
import java.time.LocalDateTime

interface EvaluationRepository {
    suspend fun queryTraitsByType(typeId: Int): List<Trait>
    suspend fun querySavedEvaluationsForUser(userId: Int, userType: UserType): List<ItemEntry>
    suspend fun querySavedEvaluationById(id: Int): SavedEvaluation?

    suspend fun saveEvaluationConfigurationForUser(
        userId: Int,
        userType: UserType,
        configuration: EvaluationConfiguration,
        timeStamp: LocalDateTime
    )
}
