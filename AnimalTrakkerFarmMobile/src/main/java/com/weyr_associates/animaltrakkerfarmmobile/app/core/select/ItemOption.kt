package com.weyr_associates.animaltrakkerfarmmobile.app.core.select

data class ItemOption<T>(
    val displayText: String,
    val data: T
)
