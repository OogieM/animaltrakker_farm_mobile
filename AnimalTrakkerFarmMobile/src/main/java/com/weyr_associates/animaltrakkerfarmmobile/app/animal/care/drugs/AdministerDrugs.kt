package com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs

object AdministerDrugs {
    const val ACTION_ADMINISTER_DRUGS = "ACTION_ADMINISTER_DRUGS"
    const val ACTION_VACCINATE_AND_DEWORM = "ACTION_VACCINATE_AND_DEWORM"
}
