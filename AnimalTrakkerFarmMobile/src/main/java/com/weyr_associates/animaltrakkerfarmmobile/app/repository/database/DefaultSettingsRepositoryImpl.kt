package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import android.content.ContentValues
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable
import com.weyr_associates.animaltrakkerfarmmobile.database.Sql
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.DefaultSettingsRepository
import java.time.LocalDateTime

class DefaultSettingsRepositoryImpl(private val databaseHandler: DatabaseHandler) :
    com.weyr_associates.animaltrakkerfarmmobile.app.repository.DefaultSettingsRepository {

    companion object {
        const val SQL_QUERY_DEFAULT_SETTINGS_ENTRIES =
            """SELECT ${DefaultSettingsTable.Columns.ID},
                    ${DefaultSettingsTable.Columns.NAME}
               FROM ${DefaultSettingsTable.NAME}
               ORDER BY ${DefaultSettingsTable.Columns.NAME}"""

        const val SQL_QUERY_DEFAULT_SETTINGS_ENTRY_BY_ID =
            """SELECT ${DefaultSettingsTable.Columns.ID},
                    ${DefaultSettingsTable.Columns.NAME}
               FROM ${DefaultSettingsTable.NAME}
               WHERE ${DefaultSettingsTable.Columns.ID} = ?"""

        const val SQL_QUERY_DEFAULT_SETTINGS_BY_ID =
            """SELECT * FROM ${DefaultSettingsTable.NAME}
                WHERE ${DefaultSettingsTable.Columns.ID} = ?"""
    }

    override fun queryDefaultSettingsEntries(): List<ItemEntry> {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_DEFAULT_SETTINGS_ENTRIES, null
        )?.use { cursor ->
            cursor.readAllItems(DefaultSettingsTable::defaultSettingsEntryFromCursor)
        } ?: emptyList()
    }

    override fun queryDefaultSettingsEntryById(id: Int): ItemEntry? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_DEFAULT_SETTINGS_ENTRY_BY_ID, arrayOf(id.toString())
        )?.use { cursor ->
            cursor.readFirstItem(DefaultSettingsTable::defaultSettingsEntryFromCursor)
        }
    }

    override fun queryStandardDefaultSettings(): DefaultSettings {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_DEFAULT_SETTINGS_BY_ID,
            arrayOf(DefaultSettings.SETTINGS_ID_DEFAULT.toString())
        )?.use { cursor ->
            cursor.readFirstItem(DefaultSettingsTable::defaultSettingsFromCursor)
                ?: throw IllegalStateException("Standard default settings records found.")
        } ?: throw IllegalStateException("No cursor result for standard default settings query.")
    }

    override fun queryDefaultSettingsById(id: Int): DefaultSettings? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_DEFAULT_SETTINGS_BY_ID, arrayOf(id.toString())
        )?.use { cursor ->
            cursor.readFirstItem(DefaultSettingsTable::defaultSettingsFromCursor)
        }
    }

    override fun updateNextTrichIdNumber(id: Int, nextTrichId: Int): Int {
        return databaseHandler.writableDatabase.update(
            DefaultSettingsTable.NAME,
            ContentValues().apply {
                put(DefaultSettingsTable.Columns.TRICH_TAG_NEXT_TAG_NUMBER, nextTrichId)
                put(DefaultSettingsTable.Columns.MODIFIED, Sql.formatDateTime(LocalDateTime.now()))
            },
            "${DefaultSettingsTable.Columns.ID} = ?",
            arrayOf(id.toString())
        )
    }
}
