package com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.channel.EventEmitter
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugLocation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

class DrugActionConfigurationViewModel(
    private val drugTypeId: Int,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    sealed interface Event

    data class DrugActionConfigured(
        val drugActionConfiguration: DrugAction.Configuration
    ) : Event

    private val _drugSelection = MutableStateFlow<Drug?>(null)
    val drugSelection = _drugSelection.asStateFlow()

    private val _drugLocationSelection = MutableStateFlow<DrugLocation?>(null)
    val drugLocationSelection = _drugLocationSelection.asStateFlow()

    val canConfigure = combine(drugSelection, drugLocationSelection) { drug, drugLocation ->
        drug != null && drug.typeId == drugTypeId && drugLocation != null
    }.stateIn(viewModelScope, SharingStarted.Eagerly, false)

    private val eventEmitter = EventEmitter<Event>(viewModelScope)
    val events = eventEmitter.events

    init {
        savedStateHandle.get<DrugAction.Configuration>(
            ConfigureDrugAction.EXTRA_DRUG_ACTION_CONFIGURATION
        )?.let { configuration ->
            _drugSelection.update { configuration.drug }
            _drugLocationSelection.update { configuration.location }
        }
    }

    fun updateDrugSelection(drug: Drug) {
        if (drug.typeId == drugTypeId) {
            _drugSelection.update { drug }
        }
    }

    fun updateDrugLocationSelection(drugLocation: DrugLocation) {
        _drugLocationSelection.update { drugLocation }
    }

    fun configure() {
        if (canConfigure.value) {
            val drug = drugSelection.value ?: return
            val drugLocation = drugLocationSelection.value ?: return
            eventEmitter.emit(
                DrugActionConfigured(
                    DrugAction.Configuration(
                        drug,
                        drugLocation
                    )
                )
            )
        }
    }
}
