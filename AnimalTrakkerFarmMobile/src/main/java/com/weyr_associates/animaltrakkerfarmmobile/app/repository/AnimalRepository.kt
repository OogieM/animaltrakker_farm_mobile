package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.ActionSet
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBreeders
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalDetails
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalDrugEvent
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalLifetime
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalNote
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalParentage
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalRearing
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalWeight
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.SexStandard
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleEvent
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTestEvent
import java.time.LocalDate
import java.time.LocalDateTime

interface AnimalRepository {
    suspend fun searchAnimalsByIdType(
        partialId: String,
        idTypeId: Int,
        speciesId: Int,
        sexStandard: SexStandard? = null
    ): List<AnimalBasicInfo>

    suspend fun searchAnimalsByName(
        partialName: String,
        speciesId: Int,
        sexStandard: SexStandard? = null
    ): List<AnimalBasicInfo>

    suspend fun queryAnimalBasicInfoByAnimalId(animalId: Int): AnimalBasicInfo?

    suspend fun queryAnimalBasicInfoByEID(eidNumber: String): AnimalBasicInfo?

    suspend fun queryAnimalDetailsByAnimalId(animalId: Int): AnimalDetails?

    suspend fun queryEIDExistence(eidNumber: String): Boolean

    suspend fun queryEIDExistenceForUpdate(idToUpdate: Int, eidNumber: String): Boolean

    suspend fun queryAnimalCurrentPremise(animalId: Int): Int

    suspend fun queryAnimalLifetime(animalId: Int): AnimalLifetime?

    suspend fun queryAnimalRearing(animalId: Int): AnimalRearing?

    suspend fun queryAnimalParentage(animalId: Int): AnimalParentage?

    suspend fun queryAnimalBreeders(animalId: Int): AnimalBreeders?

    suspend fun queryAnimalLastEvaluationWeight(animalId: Int): AnimalWeight?

    suspend fun queryAnimalNoteHistory(animalId: Int): List<AnimalNote>

    suspend fun queryAnimalDrugHistory(animalId: Int): List<AnimalDrugEvent>

    suspend fun queryAnimalTissueSampleHistory(animalId: Int): List<TissueSampleEvent>

    suspend fun queryAnimalTissueTestHistory(animalId: Int): List<TissueTestEvent>

    suspend fun queryAnimalEvaluationHistory(animalId: Int): List<AnimalEvaluation>

    suspend fun addAnimal(
        animalName: String,
        breedId: Int,
        sexId: Int,
        birthDate: LocalDate,
        birthTypeId: Int,
        rearTypeId: Int,
        ownerId: Int,
        ownerType: Owner.Type,
        ownerPremiseId: Int,
        breederId: Int,
        breederTypeId: Int,
        timeStampOn: LocalDateTime
    ): Long

    suspend fun addNotesToAnimal(
        animalId: Int,
        customNote: String?,
        predefinedNoteIds: List<Int>,
        timeStamp: LocalDateTime
    )

    suspend fun addAlertForAnimal(
        animalId: Int,
        alert: String,
        timeStamp: LocalDateTime
    )

    suspend fun addIdToAnimal(
        animalId: Long,
        idTypeId: Int,
        idColorId: Int,
        idLocationId: Int,
        idNumber: String,
        isOfficial: Boolean,
        timeStampOn: LocalDateTime
    ): Long

    suspend fun updateIdOnAnimal(
        id: Int,
        typeId: Int,
        colorId: Int,
        locationId: Int,
        number: String,
        timeStamp: LocalDateTime
    ): Boolean

    suspend fun removeIdFromAnimal(
        id: Int,
        removeReasonId: Int,
        timeStamp: LocalDateTime
    ): Boolean

    suspend fun addEvaluationForAnimal(
        animalId: Int,
        ageInDays: Long,
        timeStamp: LocalDateTime,
        trait01Id: Int,
        trait01Score: Int,
        trait02Id: Int,
        trait02Score: Int,
        trait03Id: Int,
        trait03Score: Int,
        trait04Id: Int,
        trait04Score: Int,
        trait05Id: Int,
        trait05Score: Int,
        trait06Id: Int,
        trait06Score: Int,
        trait07Id: Int,
        trait07Score: Int,
        trait08Id: Int,
        trait08Score: Int,
        trait09Id: Int,
        trait09Score: Int,
        trait10Id: Int,
        trait10Score: Int,
        trait11Id: Int,
        trait11Score: Float,
        trait11UnitsId: Int,
        trait12Id: Int,
        trait12Score: Float,
        trait12UnitsId: Int,
        trait13Id: Int,
        trait13Score: Float,
        trait13UnitsId: Int,
        trait14Id: Int,
        trait14Score: Float,
        trait14UnitsId: Int,
        trait15Id: Int,
        trait15Score: Float,
        trait15UnitsId: Int,
        trait16Id: Int,
        trait16OptionId: Int,
        trait17Id: Int,
        trait17OptionId: Int,
        trait18Id: Int,
        trait18OptionId: Int,
        trait19Id: Int,
        trait19OptionId: Int,
        trait20Id: Int,
        trait20OptionId: Int
    ): Long

    suspend fun addTissueTestForAnimal(
        animalId: Int,
        tissueSampleTypeId: Int,
        tissueSampleContainerTypeId: Int,
        tissueSampleContainerId: String,
        tissueSampleContainerExpDate: String,
        tissueTestId: Int,
        laboratoryId: Int,
        timeStampOn: LocalDateTime
    ): Long

    suspend fun markAnimalDeceased(
        animalId: Int,
        deathReasonId: Int,
        deathDate: LocalDate,
        timeStamp: LocalDateTime
    )

    suspend fun trackActionsForAnimal(
        animalId: Int,
        ageInDays: Long,
        actions: ActionSet,
        timeStamp: LocalDateTime
    )
}
