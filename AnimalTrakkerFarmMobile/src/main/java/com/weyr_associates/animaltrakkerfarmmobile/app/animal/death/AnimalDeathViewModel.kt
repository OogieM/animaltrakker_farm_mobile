package com.weyr_associates.animaltrakkerfarmmobile.app.animal.death

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.AnimalInfoLookup
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.requireAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.model.DeathReason
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

class AnimalDeathViewModel(
    private val animalRepo: AnimalRepository
) : ViewModel(), LookupAnimalInfo {

    sealed interface Event

    data class AnimalAlert(
        val alertText: String
    ) : Event

    data object UpdateDatabaseSuccess : Event
    data object UpdateDatabaseFailure : Event

    private val animalInfoLookup = AnimalInfoLookup(viewModelScope, animalRepo)

    override val animalInfoState = animalInfoLookup.animalInfoState

    private val _animalDeathReason = MutableStateFlow<DeathReason?>(null)
    val animalDeathReason = _animalDeathReason.asStateFlow()

    private val _animalDeathDate = MutableStateFlow<LocalDate?>(LocalDate.now())
    val animalDeathDate = _animalDeathDate.asStateFlow()

    private val _isUpdatingDatabase = MutableStateFlow(false)

    override fun lookupAnimalInfoById(animalId: Int) {
        animalInfoLookup.lookupAnimalInfoById(animalId)
    }

    override fun lookupAnimalInfoByEIDNumber(eidNumber: String) {
        animalInfoLookup.lookupAnimalInfoByEIDNumber(eidNumber)
    }

    override fun resetAnimalInfo() {
        animalInfoLookup.resetAnimalInfo()
    }

    val canClearData = combine(
        animalInfoState,
        _isUpdatingDatabase,
        _animalDeathReason,
        _animalDeathDate
    ) { animalInfoState, isUpdatingDatabase, deathReason, deathDate ->
        animalInfoState is LookupAnimalInfo.AnimalInfoState.Loaded &&
                !animalInfoState.animalBasicInfo.isDead &&
                !isUpdatingDatabase &&
                (deathReason != null || deathDate != null)
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canSaveToDatabase = combine(
        animalInfoState,
        _isUpdatingDatabase,
        _animalDeathReason,
        _animalDeathDate
    ) { animalInfoState, isUpdatingDatabase, deathReason, deathDate ->
        animalInfoState is LookupAnimalInfo.AnimalInfoState.Loaded &&
                !animalInfoState.animalBasicInfo.isDead &&
                !isUpdatingDatabase &&
                deathReason != null && deathDate != null
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val eventChannel = Channel<Event>()
    val events = eventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            animalInfoState.filter { it is LookupAnimalInfo.AnimalInfoState.Loaded }
                .map { it.requireAs<LookupAnimalInfo.AnimalInfoState.Loaded>().animalBasicInfo }
                .collectLatest {
                    it.alert?.let { alertText ->
                        if (alertText.isNotBlank()) {
                            viewModelScope.launch {
                                eventChannel.send(AnimalAlert(alertText))
                            }
                        }
                    }
                }
        }
    }

    fun updateDeathReason(deathReason: DeathReason) {
        _animalDeathReason.update { deathReason }
    }

    fun updateDeathDate(deathDate: LocalDate) {
        _animalDeathDate.update { deathDate }
    }

    fun clearData() {
        if (canClearData.value) {
            executeClearData()
        }
    }

    fun saveToDatabase() {
        if (canSaveToDatabase.value) {
            viewModelScope.launch {
                executeUpdateDatabase()
            }
        }
    }

    private fun executeClearData() {
        _animalDeathReason.update { null }
        _animalDeathDate.update { null }
    }

    private suspend fun executeUpdateDatabase() {

        val animalBasicInfo = animalInfoState.value.takeAs<LookupAnimalInfo.AnimalInfoState.Loaded>()
            ?.animalBasicInfo ?: return
        val deathReason = _animalDeathReason.value ?: return
        val deathDate = _animalDeathDate.value ?: return

        withContext(Dispatchers.IO) {
            _isUpdatingDatabase.update { true }
            try {
                animalRepo.markAnimalDeceased(
                    animalBasicInfo.id,
                    deathReason.id,
                    deathDate,
                    LocalDateTime.now()
                )
                executeClearData()
                animalInfoLookup.lookupAnimalInfoById(animalBasicInfo.id)
                eventChannel.send(UpdateDatabaseSuccess)
            } catch (ex: Exception) {
                eventChannel.send(UpdateDatabaseFailure)
            } finally {
                _isUpdatingDatabase.update { false }
            }
        }
    }
}
