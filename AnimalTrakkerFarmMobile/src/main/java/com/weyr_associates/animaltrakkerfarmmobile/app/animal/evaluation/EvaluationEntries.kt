package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

data class EvaluationEntries(
    val trait01Id: Int,
    val trait01Score: Int,
    val trait02Id: Int,
    val trait02Score: Int,
    val trait03Id: Int,
    val trait03Score: Int,
    val trait04Id: Int,
    val trait04Score: Int,
    val trait05Id: Int,
    val trait05Score: Int,
    val trait06Id: Int,
    val trait06Score: Int,
    val trait07Id: Int,
    val trait07Score: Int,
    val trait08Id: Int,
    val trait08Score: Int,
    val trait09Id: Int,
    val trait09Score: Int,
    val trait10Id: Int,
    val trait10Score: Int,
    val trait11Id: Int,
    val trait11Score: Float,
    val trait11UnitsId: Int,
    val trait12Id: Int,
    val trait12Score: Float,
    val trait12UnitsId: Int,
    val trait13Id: Int,
    val trait13Score: Float,
    val trait13UnitsId: Int,
    val trait14Id: Int,
    val trait14Score: Float,
    val trait14UnitsId: Int,
    val trait15Id: Int,
    val trait15Score: Float,
    val trait15UnitsId: Int,
    val trait16Id: Int,
    val trait16OptionId: Int,
    val trait17Id: Int,
    val trait17OptionId: Int,
    val trait18Id: Int,
    val trait18OptionId: Int,
    val trait19Id: Int,
    val trait19OptionId: Int,
    val trait20Id: Int,
    val trait20OptionId: Int
) {
    fun extractScoreForTraitId(traitId: Int): Int = when (traitId) {
        trait01Id -> trait01Score
        trait02Id -> trait02Score
        trait03Id -> trait03Score
        trait04Id -> trait04Score
        trait05Id -> trait05Score
        trait06Id -> trait06Score
        trait07Id -> trait07Score
        trait08Id -> trait08Score
        trait09Id -> trait09Score
        trait10Id -> trait10Score
        else -> 0
    }

    fun extractScoreForUnitTraitId(unitsTraitId: Int): Float = when (unitsTraitId) {
        trait11Id -> trait11Score
        trait12Id -> trait12Score
        trait13Id -> trait13Score
        trait14Id -> trait14Score
        trait15Id -> trait15Score
        else -> 0f
    }

    fun extractOptionIdForOptionTraitId(optionTraitId: Int): Int = when (optionTraitId) {
        trait16Id -> trait16OptionId
        trait17Id -> trait17OptionId
        trait18Id -> trait18OptionId
        trait19Id -> trait19OptionId
        trait20Id -> trait20OptionId
        else -> -1
    }
}
