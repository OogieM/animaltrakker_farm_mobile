package com.weyr_associates.animaltrakkerfarmmobile.app.settings

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings

class ActiveDefaultSettings(private val preferences: SharedPreferences) {
    companion object {
        @JvmStatic
        fun from(context: Context): ActiveDefaultSettings {
            return ActiveDefaultSettings(PreferenceManager.getDefaultSharedPreferences(context))
        }

        const val PREFS_KEY_ACTIVE_DEFAULT_SETTINGS_ID = "ACTIVE_DEFAULT_SETTINGS_ID"
    }

    fun loadActiveDefaultSettingsId(): Int {
        return preferences.getInt(
            PREFS_KEY_ACTIVE_DEFAULT_SETTINGS_ID,
            DefaultSettings.SETTINGS_ID_DEFAULT
        )
    }

    fun saveActiveDefaultSettingsId(id: Int) {
        preferences.edit()
            .putInt(PREFS_KEY_ACTIVE_DEFAULT_SETTINGS_ID, id)
            .apply()
    }
}
