package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalDialogs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.AddAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.AddAnimalAlert
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.ShowAlertButtonPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.AnimalSpeciesMismatch
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.IncompleteDataEntry
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.InputEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.PrintLabelRequestedError
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.PrintLabelRequestedEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleViewModel.UpdateDatabaseEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo.AnimalInfoState
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfoPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.note.TakeNotesButtonPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.collectLatestOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.observeOneTimeEventsOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.device.DeviceConnectionState
import com.weyr_associates.animaltrakkerfarmmobile.app.device.DeviceConnectionStatePresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.device.baacode.BaacodeReaderService
import com.weyr_associates.animaltrakkerfarmmobile.app.device.eid.EIDReaderConnection
import com.weyr_associates.animaltrakkerfarmmobile.app.label.ExtractPrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabel
import com.weyr_associates.animaltrakkerfarmmobile.app.label.PrintLabelData
import com.weyr_associates.animaltrakkerfarmmobile.app.permissions.RequiredPermissionsWatcher
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.LaboratoryRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.SpeciesRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.TissueSampleContainerTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.TissueSampleTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.TissueTestRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.laboratorySelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueSampleContainerTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueSampleTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.tissueTestSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadDefaultIdTypeIds
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityTissueSampleBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.Laboratory
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleType
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTest

class TissueSampleActivity : AppCompatActivity() {

    companion object {

        fun newIntent(
            context: Context,
            animalBasicInfo: AnimalBasicInfo,
            titleAddition: String? = null,
            defaultLabCompanyId: Int? = null,
            printLabelData: PrintLabelData? = null
        ) = Intent(context, TissueSampleActivity::class.java).apply {
                action = TissueSample.ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL
                putExtra(TissueSample.EXTRA_ANIMAL_BASIC_INFO, animalBasicInfo)
                titleAddition?.let { putExtra(TissueSample.EXTRA_TITLE_ADDITION, it) }
                defaultLabCompanyId?.let { putExtra(TissueSample.EXTRA_DEFAULT_LAB_COMPANY_ID, it) }
                printLabelData?.let { putExtra(TissueSample.EXTRA_PRINT_LABEL_DATA, it) }
            }
    }

    private var autoPrint = false

    private var mIsBAABound: Boolean = false
    private var mBaaCodeService: Messenger? = null

    private val mMessenger: Messenger = Messenger(IncomingHandler())

    private val viewModel by viewModels<TissueSampleViewModel> { ViewModelFactory(this) }

    private val binding: ActivityTissueSampleBinding by lazy {
        ActivityTissueSampleBinding.inflate(layoutInflater)
    }

    private val takeNotesButtonPresenter by lazy {
        TakeNotesButtonPresenter(this, binding.buttonPanelTop.takeNoteButton)
    }

    private val showAlertButtonPresenter by lazy {
        ShowAlertButtonPresenter(this, binding.buttonPanelTop.showAlertButton)
    }

    private val selectAnimalLauncher = registerForActivityResult(SelectAnimal.Contract()) { animalId ->
        animalId?.let { viewModel.lookupAnimalInfoById(animalId) }
    }

    private val addAnimalAlertLauncher = registerForActivityResult(AddAnimalAlert.Contract()) { result ->
        if (result.success) { viewModel.lookupAnimalInfoById(result.animalId) }
    }

    private val addAndSelectAnimalLauncher = registerForActivityResult(AddAnimal.Contract()) { result ->
        result?.let { viewModel.lookupAnimalInfoById(result.animalId) }
    }

    private val printLabelLauncher = registerForActivityResult(PrintLabel.Contract()) { success ->
        if (success) {
            startActivity(PrintLabel.newIntentToPrint(this, autoPrint))
        }
    }

    private val lookupAnimalInfoPresenter by lazy {
        LookupAnimalInfoPresenter(binding.lookupAnimalInfo).apply {
            addAnimalAlertLauncher = this@TissueSampleActivity.addAnimalAlertLauncher
            onAddAnimalWithEIDClicked = { eidNumber ->
                AnimalDialogs.promptToAddAnimalWithEID(
                    this@TissueSampleActivity, eidNumber, addAndSelectAnimalLauncher
                )
            }
        }
    }

    private lateinit var labNameSpinnerPresenter: ItemSelectionPresenter<Laboratory>

    private lateinit var sampleContainerTypeSpinnerPresenter: ItemSelectionPresenter<TissueSampleContainerType>

    private lateinit var sampleTypeSpinnerPresenter: ItemSelectionPresenter<TissueSampleType>

    private lateinit var sampleTestSpinnerPresenter: ItemSelectionPresenter<TissueTest>

    private val requiredPermissionsWatcher = RequiredPermissionsWatcher(this)

    private lateinit var eidReaderConnection: EIDReaderConnection
    private lateinit var baaCodeReaderStatePresenter: DeviceConnectionStatePresenter

    //  baacode service connection & disconnection
    private val mbaacodeConnection: ServiceConnection = BaacodeServiceConnection()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.hasExtra(TissueSample.EXTRA_TITLE_ADDITION)) {
            val titleString = getString(R.string.title_activity_tissue_sample)
            val titleAddition = intent.getStringExtra(TissueSample.EXTRA_TITLE_ADDITION)
            title = "$titleString - $titleAddition"
        }
        setContentView(binding.root)
        loadPreferences()

        with(binding.buttonPanelTop) {

            if (intent.action == TissueSample.ACTION_TAKE_TISSUE_SAMPLE_FOR_ANIMAL) {
                show(
                    TopButtonBar.UI_SHOW_ALERT or
                            TopButtonBar.UI_TAKE_NOTE or
                            TopButtonBar.UI_CLEAR_DATA or
                            TopButtonBar.UI_ACTION_UPDATE_DATABASE
                )
            } else {
                show(TopButtonBar.UI_ALL or TopButtonBar.UI_ACTION_UPDATE_DATABASE)
            }

            scanEIDButton.setOnClickListener { onScanEID() }
            lookupAnimalButton.setOnClickListener { onLookupAnimal() }
            clearDataButton.setOnClickListener { viewModel.clearData() }
            mainActionButton.setOnClickListener { viewModel.saveToDatabase() }
        }

        baaCodeReaderStatePresenter = DeviceConnectionStatePresenter(
            this@TissueSampleActivity, binding.imageBaacodeReaderStatus
        )

        labNameSpinnerPresenter = laboratorySelectionPresenter(
            binding.labNameSpinner
        ) {
            viewModel.selectLaboratory(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedLaboratory)
        }

        sampleTypeSpinnerPresenter = tissueSampleTypeSelectionPresenter(
            binding.sampleTypeSpinner
        ) {
            viewModel.selectTissueSampleType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedTissueSampleType)
        }

        sampleTestSpinnerPresenter = tissueTestSelectionPresenter(
            binding.sampleTestSpinner
        ) {
            viewModel.selectTissueTestType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedTissueTestType)
        }

        sampleContainerTypeSpinnerPresenter = tissueSampleContainerTypeSelectionPresenter(
            binding.sampleContainerTypeSpinner
        ) {
            viewModel.selectTissueSampleContainerType(it)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectTissueContainerType)
        }

        collectLatestOnStart(viewModel.canClearData) { canClear ->
            binding.buttonPanelTop.clearDataButton.isEnabled = canClear
        }

        collectLatestOnStart(viewModel.canSaveToDatabase) { canSave ->
            binding.buttonPanelTop.mainActionButton.isEnabled = canSave
        }

        collectLatestOnStart(viewModel.canScanTSU) { canScan ->
            binding.btnScanSampleContainer.isEnabled = canScan
        }

        collectLatestOnStart(viewModel.canPrintLabel) { canPrint ->
            binding.printLabelBtn.isEnabled = canPrint
        }

        observeOneTimeEventsOnStart(viewModel.events, ::handleEvent)

        collectLatestOnStart(viewModel.animalInfoState) { animalInfoState ->
            updateTopBarAnimalActions(animalInfoState)
            updateAnimalInfoDisplay(animalInfoState)
        }

        binding.sampleContainerId.addTextChangedListener { viewModel.containerId = it.toString() }
        binding.sampleContainerId.setText(viewModel.containerId)
        binding.sampleExpDate.addTextChangedListener { viewModel.containerExpDate = it.toString() }
        binding.sampleExpDate.setText(viewModel.containerExpDate)

        binding.btnScanSampleContainer.setOnClickListener { scanTSU() }
        binding.printLabelBtn.setOnClickListener { viewModel.printLabel() }

        eidReaderConnection = EIDReaderConnection(this, lifecycle)
            .also { lifecycle.addObserver(it) }

        collectLatestOnStart(eidReaderConnection.deviceConnectionState) { connectionState ->
            binding.buttonPanelTop.updateEIDReaderConnectionState(connectionState)
        }
        collectLatestOnStart(eidReaderConnection.isScanningForEID) { isScanning ->
            binding.buttonPanelTop.showScanningEID = isScanning
        }
        observeOneTimeEventsOnStart(eidReaderConnection.onEIDScanned, ::onEIDScanned)

        lifecycle.addObserver(requiredPermissionsWatcher)
    }

    override fun onResume() {
        super.onResume()
        checkIfServiceIsRunning()
    }

    public override fun onPause() {
        super.onPause()
        Log.i("Breeding", " OnPause")
        doUnbindbaacodeService()
    }

    private fun onScanEID() {
        eidReaderConnection.toggleScanningEID()
    }

    private fun onLookupAnimal() {
        selectAnimalLauncher.launch(null)
    }

    //  Really should be check if services are running...
    //  Start the service if it is not already running and bind to it
    private fun checkIfServiceIsRunning() {
        if (BaacodeReaderService.isBaaRunning()) {
            doBindbaacodeService()
        } else {
            startService(Intent(this, BaacodeReaderService::class.java))
            doBindbaacodeService()
        }
    }

    private fun updateTopBarAnimalActions(animalInfoState: AnimalInfoState) {
        takeNotesButtonPresenter.animalInfoState = animalInfoState
        showAlertButtonPresenter.animalInfoState = animalInfoState
    }

    private fun updateAnimalInfoDisplay(animalInfoState: AnimalInfoState) {
        lookupAnimalInfoPresenter.animalInfoState = animalInfoState
    }

    private fun handlePrintLabelRequestedEvent(event: PrintLabelRequestedEvent) {
        printLabelLauncher.launch(PrintLabel.Request(event.printLabelData, autoPrint))
    }

    private fun loadPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        autoPrint = preferences.getBoolean("autop", false)
    }

    private fun handleEvent(event: TissueSampleViewModel.Event) {
        when (event) {
            InputEvent.ContainerIdChanged -> {
                binding.sampleContainerId.setText(viewModel.containerId)
            }
            InputEvent.ContainerExpDateChanged -> {
                binding.sampleExpDate.setText(viewModel.containerExpDate)
            }
            IncompleteDataEntry -> {
                showIncompleteDataEntryError()
            }
            is AnimalSpeciesMismatch -> {
                showAnimalSpeciesMismatchPrompt(
                    event.defaultSpeciesName,
                    event.animalSpeciesName
                )
            }
            is PrintLabelRequestedEvent -> {
                handlePrintLabelRequestedEvent(event)
            }
            is PrintLabelRequestedError -> {
                showEIDRequiredToPrintError()
            }
            UpdateDatabaseEvent.Success -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_tissue_test_success,
                    Toast.LENGTH_SHORT
                ).show()
            }
            UpdateDatabaseEvent.Error -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_tissue_test_error,
                    Toast.LENGTH_SHORT
                ).show()
            }
            is TissueSampleViewModel.AnimalAlert -> {
                AnimalDialogs.showAnimalAlert(this, event.alertText)
            }
        }
    }

    private fun showIncompleteDataEntryError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_incomplete_tissue_sample_entry)
            .setMessage(R.string.dialog_message_incomplete_tissue_sample_entry)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun showAnimalSpeciesMismatchPrompt(defaultSpecies: String, currentSpecies: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_animal_species_mismatch_prompt)
            .setMessage(
                getString(
                    R.string.dialog_message_animal_species_mismatch_prompt,
                    defaultSpecies,
                    currentSpecies
                )
            )
            .setPositiveButton(R.string.yes_label) { _, _ -> /*NO-OP*/ }
            .setNegativeButton(R.string.no_label) { _, _ -> viewModel.resetAnimalInfo() }
            .create()
            .show()
    }

    private fun showEIDRequiredToPrintError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_eid_required_to_print_label)
            .setMessage(R.string.dialog_message_eid_required_to_print_label)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }

    private fun scanTSU() {
        mBaaCodeService?.let { service ->
            try {
                //Start baaCodeService sending tags
                val msg = Message.obtain(null, BaacodeReaderService.MSG_SEND_ME_TAGS)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
    }

    private fun onEIDScanned(eidNumber: String) {
        viewModel.lookupAnimalInfoByEIDNumber(eidNumber)
    }

    // use BaaCode reader
    private fun onBAAScanned(baaCode: String) {
        viewModel.onBaaCodeScanned(baaCode)
    }

    //  Finally the baacode bind
    private fun doBindbaacodeService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.i("Breeding", "At baacode bind1.")
        bindService(Intent(this, BaacodeReaderService::class.java), mbaacodeConnection, BIND_AUTO_CREATE)
        mIsBAABound = true
        val service = mBaaCodeService
        if (service != null) {
            try {
                //Request status update
                var msg = Message.obtain(null, BaacodeReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
                //			Log.i("Breeding", "At baacode bind2.");
                //Request full log from service.
                msg = Message.obtain(null, BaacodeReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                service.send(msg)
            } catch (e: RemoteException) {
//			Log.i("Breeding", "message exception bind");
            }
        }
    }

    /// baacode unbind
    fun doUnbindbaacodeService() {
        Log.i("Breeding", "At baacodeUnbindservice")
        val service = mBaaCodeService
        if (service != null) {
            try {
                //Stop baacodeService from sending tags
                val msg = Message.obtain(null, BaacodeReaderService.MSG_NO_TAGS_PLEASE)
                msg.replyTo = mMessenger
                Log.i("Breeding", "At baacode unbind.")
                service.send(msg)
            } catch (e: RemoteException) {
                // In this case the service has crashed before we could even do anything with it
            }
        }
        if (mIsBAABound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (service != null) {
                try {
                    val msg = Message.obtain(null, BaacodeReaderService.MSG_UNREGISTER_CLIENT)
                    msg.replyTo = mMessenger
                    service.send(msg)
                } catch (e: RemoteException) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mbaacodeConnection)
            mIsBAABound = false
        }
    }

    private inner class BaacodeServiceConnection : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val service = Messenger(binder).also { mBaaCodeService = it }
            Log.i("Breeding", "At baacodeService.")
            try {
                //Register client with service
                val msg = Message.obtain(null, BaacodeReaderService.MSG_REGISTER_CLIENT)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService exception.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Update status
            try {
                val msg = Message.obtain(null, BaacodeReaderService.MSG_UPDATE_STATUS, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService update status crash.")
                // In this case the service has crashed before we could even do anything with it
            }
            //Request full log from service.
            try {
                //Update status
                val msg = Message.obtain(null, BaacodeReaderService.MSG_UPDATE_LOG_FULL, 0, 0)
                msg.replyTo = mMessenger
                service.send(msg)
            } catch (e: RemoteException) {
                Log.i("Breeding", "At baacodeService update log full crash.")
                // In this case the service has crashed before we could even do anything with it
            }
        }

        //    ***    And last the baacode service disconnection
        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mBaaCodeService = null
        }
    }

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                BaacodeReaderService.MSG_UPDATE_STATUS -> {
                    val data = msg.data
                    val state = data.getString("stat1")
                    state?.let { stateString ->
                        baaCodeReaderStatePresenter.connectionState =
                            DeviceConnectionState.fromStateString(stateString)
                    }
                }

                BaacodeReaderService.MSG_NEW_Baacode_FOUND -> {
                    val b4 = msg.data
                    val baaCode = b4.getString("info1")
                    if (baaCode != null) {
                        onBAAScanned(baaCode)
                    }
                }

                BaacodeReaderService.MSG_THREAD_SUICIDE -> {
                    doUnbindbaacodeService()
                    stopService(Intent(this@TissueSampleActivity, BaacodeReaderService::class.java))
                }

                else -> super.handleMessage(msg)
            }
        }
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            TissueSampleViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
                val activeDefSettings = ActiveDefaultSettings(sharedPreferences)
                val defSettingsRepo = DefaultSettingsRepositoryImpl(databaseHandler)
                val loadActiveDefaults = LoadActiveDefaultSettings(
                    activeDefaultSettings = activeDefSettings,
                    defSettingsRepo
                )
                val loadDefaultIdTypeIds = LoadDefaultIdTypeIds(loadActiveDefaults)
                val animalRepo = AnimalRepositoryImpl(databaseHandler)
                val speciesRepo = SpeciesRepositoryImpl(databaseHandler)
                @Suppress("UNCHECKED_CAST")
                TissueSampleViewModel(
                    extras.createSavedStateHandle(),
                    databaseHandler,
                    loadActiveDefaultSettings = loadActiveDefaults,
                    animalRepo = animalRepo,
                    speciesRepo = speciesRepo,
                    laboratoryRepository = LaboratoryRepositoryImpl(databaseHandler),
                    tissueSampleTypeRepository = TissueSampleTypeRepositoryImpl(databaseHandler),
                    tissueTestRepository = TissueTestRepositoryImpl(databaseHandler),
                    tissueSampleContainerTypeRepository = TissueSampleContainerTypeRepositoryImpl(databaseHandler),
                    extractPrintLabelData = ExtractPrintLabelData(sharedPreferences, loadDefaultIdTypeIds)
                ) as T
            }
            else -> throw IllegalStateException("${modelClass.simpleName} is not supported.")
        }
    }
}
