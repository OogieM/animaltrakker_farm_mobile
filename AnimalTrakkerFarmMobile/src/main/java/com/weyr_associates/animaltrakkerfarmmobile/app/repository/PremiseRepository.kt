package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Premise

interface PremiseRepository {

    suspend fun queryPhysicalPremiseForOwner(
        ownerId: Int,
        ownerType: Owner.Type
    ): Premise?
}
