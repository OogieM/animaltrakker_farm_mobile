package com.weyr_associates.animaltrakkerfarmmobile.app.animal.info

import com.weyr_associates.animaltrakkerfarmmobile.databinding.ViewAnimalInfoSectionParentageBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalParentage

class AnimalParentagePresenter(binding: ViewAnimalInfoSectionParentageBinding? = null) {

    var binding: ViewAnimalInfoSectionParentageBinding? = binding
        set(value) {
            field = value
            bindViews()
        }

    var animalParentage: AnimalParentage? = null
        set(value) {
            field = value
            bindViews()
        }

    private fun bindViews() {
        val binding = binding ?: return
        val parentage = animalParentage
        if (parentage != null) {
            with(binding) {
                textSireFlockName.text = parentage.sireFlockPrefix
                textSireAnimalName.text = parentage.sireName
                textDamFlockName.text = parentage.damFlockPrefix
                textDamAnimalName.text = parentage.damName
            }
        } else {
            with(binding) {
                textSireFlockName.text = ""
                textSireAnimalName.text = ""
                textDamFlockName.text = ""
                textDamAnimalName.text = ""
            }
        }
    }
}
