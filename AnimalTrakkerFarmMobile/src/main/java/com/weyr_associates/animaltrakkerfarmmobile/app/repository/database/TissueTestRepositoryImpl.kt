package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.repository.TissueTestRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueTestTable
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueTestTable.Sql.SQL_QUERY_TISSUE_TESTS
import com.weyr_associates.animaltrakkerfarmmobile.database.TissueTestTable.Sql.SQL_QUERY_TISSUE_TEST_BY_ID
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueTest

class TissueTestRepositoryImpl(private val databaseHandler: DatabaseHandler) : TissueTestRepository {

    override fun queryTissueTests(): List<TissueTest> {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_TISSUE_TESTS,
            emptyArray()
        )?.use { cursor ->
            cursor.readAllItems(TissueTestTable::tissueTestFrom)
        } ?: emptyList()
    }

    override fun queryTissueTestById(id: Int): TissueTest? {
        return databaseHandler.readableDatabase.rawQuery(
            SQL_QUERY_TISSUE_TEST_BY_ID,
            arrayOf(id.toString())
        )?.use { cursor ->
            cursor.readFirstItem(TissueTestTable::tissueTestFrom)
        }
    }
}
