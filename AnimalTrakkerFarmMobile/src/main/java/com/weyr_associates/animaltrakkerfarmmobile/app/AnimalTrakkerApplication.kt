package com.weyr_associates.animaltrakkerfarmmobile.app

import android.app.Application
import android.util.Log
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.files.AppDirectories
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AnimalTrakkerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (!AppDirectories.ensureAppDirectoriesExist(this)) {
            Log.w(TAG, "Failed to ensure the existence of the application directory structure.")
        }
        with(DatabaseManager.getInstance(this)) {
            if(!isDatabaseFilePresent()) {
                @Suppress("OPT_IN_USAGE")
                GlobalScope.launch { loadSeedDatabase() }
            }
        }
    }

    companion object {
        private val TAG = AnimalTrakkerApplication::class.java.simpleName
    }
}
