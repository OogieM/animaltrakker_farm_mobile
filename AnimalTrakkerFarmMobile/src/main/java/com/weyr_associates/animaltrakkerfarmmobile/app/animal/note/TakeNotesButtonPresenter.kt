package com.weyr_associates.animaltrakkerfarmmobile.app.animal.note

import android.content.Context
import android.widget.Button
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo

class TakeNotesButtonPresenter(private val context: Context, button: Button? = null) {

    var button: Button? = button
        set(value) {
            field = value
            updateButtonDisplay()
        }

    var animalInfoState: LookupAnimalInfo.AnimalInfoState? = null
        set(value) {
            field = value
            updateButtonDisplay()
        }

    var onTakeNotesClicked: ((AnimalBasicInfo) -> Unit)? = null

    private fun updateButtonDisplay() {
        val button = button ?: return
        when (val animalInfoState = animalInfoState) {
            is LookupAnimalInfo.AnimalInfoState.Loaded -> {
                button.isEnabled = true
                button.setOnClickListener {
                    onTakeNotesClicked?.invoke(animalInfoState.animalBasicInfo)
                        ?: startTakeNotesActivity(animalInfoState.animalBasicInfo.id)
                }
            }
            else -> {
                with(button) {
                    isEnabled = false
                    setOnClickListener(null)
                }
            }
        }
    }

    private fun startTakeNotesActivity(animalId: Int) {
        context.startActivity(TakeNotesActivity.newIntent(context, animalId))
    }
}
