package com.weyr_associates.animaltrakkerfarmmobile.app.settings

class SaveActiveDefaultSettings(
    private val activeDefaultSettings: ActiveDefaultSettings
) {
    operator fun invoke(defaultSettingsId: Int) {
        activeDefaultSettings.saveActiveDefaultSettingsId(
            defaultSettingsId
        )
    }
}
