package com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.AnimalDialogs
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.ActionSet
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.AnimalAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug.ConfigureDrugAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug.DrugAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.menu.AnimalActionMenuBottomSheetDialog
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.menu.MenuOption
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.weight.WeightAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.AddAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.AddAnimalAlert
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.ShowAlertButtonPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfoPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.note.TakeNotesButtonPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.collectLatestOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.observeOneTimeEventsOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.recyclerview.GridSpacerItemDecoration
import com.weyr_associates.animaltrakkerfarmmobile.app.device.eid.EIDReaderConnection
import com.weyr_associates.animaltrakkerfarmmobile.app.device.scale.ScaleDeviceConnection
import com.weyr_associates.animaltrakkerfarmmobile.app.permissions.RequiredPermissionsWatcher
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.UnitOfMeasureRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectAnimal
import com.weyr_associates.animaltrakkerfarmmobile.app.select.drugTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityAdministerDrugsBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ItemAnimalActionDrugBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ItemAnimalActionWeightBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import kotlinx.coroutines.flow.combine

class AdministerDrugsActivity : AppCompatActivity() {

    companion object {
        fun newIntentToAdministerDrugs(context: Context) =
            Intent(context, AdministerDrugsActivity::class.java).apply {
                action = AdministerDrugs.ACTION_ADMINISTER_DRUGS
            }

        fun newIntentToVaccinateAndDeworm(context: Context) =
            Intent(context, AdministerDrugsActivity::class.java).apply {
                action = AdministerDrugs.ACTION_VACCINATE_AND_DEWORM
            }

        private const val REASON_SCAN_WEIGHT_FOR_ACTION_SET = "REASON_SCAN_WEIGHT_FOR_ACTION_SET"
    }

    private val isVaccineAndDeworm by lazy {
        intent?.action == AdministerDrugs.ACTION_VACCINATE_AND_DEWORM
    }

    private val binding by lazy {
        ActivityAdministerDrugsBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<AdministerDrugsViewModel> {
        ViewModelFactory(this)
    }

    private val backConfirmationHandler = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            showExitConfirmationDialog()
        }
    }

    private val lookupAnimalInfoPresenter by lazy {
        LookupAnimalInfoPresenter(binding.lookupAnimalInfo).apply {
            displayAnimalWeight = true
            displayFlockAndBreed = false
            addAnimalAlertLauncher = this@AdministerDrugsActivity.addAnimalAlertLauncher
            onAddAnimalWithEIDClicked = { eidNumber ->
                addAndSelectAnimalLauncher.launch(AddAnimal.Request(IdType.ID_TYPE_ID_EID, eidNumber))
            }
        }
    }

    private val takeNotesButtonPresenter by lazy {
        TakeNotesButtonPresenter(this, binding.buttonPanelTop.takeNoteButton)
    }

    private val showAlertButtonPresenter by lazy {
        ShowAlertButtonPresenter(this, binding.buttonPanelTop.showAlertButton)
    }

    private val selectAnimalLauncher = registerForActivityResult(SelectAnimal.Contract()) { animalId ->
        animalId?.let { viewModel.lookupAnimalInfoById(animalId) }
    }

    private val addAnimalAlertLauncher = registerForActivityResult(AddAnimalAlert.Contract()) { result ->
        if (result.success) { viewModel.lookupAnimalInfoById(result.animalId) }
    }

    private val addAndSelectAnimalLauncher = registerForActivityResult(AddAnimal.Contract()) { result ->
        result?.let { viewModel.lookupAnimalInfoById(result.animalId) }
    }

    private val configureDrugActionLauncher = registerForActivityResult(ConfigureDrugAction.ConfigureContract()) { result ->
        result?.let { viewModel.onDrugActionConfigured(result) }
    }

    private val editDrugActionLauncher = registerForActivityResult(ConfigureDrugAction.EditContract()) { result ->
        result?.let { viewModel.onDrugActionConfigurationEdited(it.actionId, it.configuration) }
    }

    private lateinit var eidReaderConnection: EIDReaderConnection
    private lateinit var scaleDeviceConnection: ScaleDeviceConnection
    private lateinit var requiredPermissionsWatcher: RequiredPermissionsWatcher

    private lateinit var actionsAdapter: AnimalActionAdapter

    private lateinit var drugTypeSelectionPresenter: ItemSelectionPresenter<DrugType>
    private var selectedDrugType: DrugType? = null

    private data class ActionsUpdate(val isConfigured: Boolean, val actions: ActionSet?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        eidReaderConnection = EIDReaderConnection(this, lifecycle)
            .also { lifecycle.addObserver(it) }
        scaleDeviceConnection = ScaleDeviceConnection(this, lifecycle)
            .also { lifecycle.addObserver(it) }
        requiredPermissionsWatcher = RequiredPermissionsWatcher(this)
            .also { lifecycle.addObserver(it) }
        with(binding) {
            with(buttonPanelTop) {
                scanEIDButton.setOnClickListener {
                    eidReaderConnection.toggleScanningEID()
                }
                lookupAnimalButton.setOnClickListener {
                    selectAnimalLauncher.launch(null)
                }
                clearDataButton.setOnClickListener {
                    viewModel.clearData()
                }
                mainActionButton.setOnClickListener {
                    viewModel.saveToDatabase()
                }
            }
            buttonAddVaccine.setOnClickListener {
                viewModel.onConfigureDrugAction(DrugType.ID_VACCINE)
            }
            buttonAddDewormer.setOnClickListener {
                viewModel.onConfigureDrugAction(DrugType.ID_DEWORMER)
            }
        }
        actionsAdapter = AnimalActionAdapter(
            onActionActivated = ::onAnimalActionActivated,
            onActionMenuActivated = ::onAnimalActionMenuActivated
        )
        with(binding.recyclerActionItems) {
            layoutManager = GridLayoutManager(
                this@AdministerDrugsActivity, 2
            )
            addItemDecoration(
                GridSpacerItemDecoration(
                    this@AdministerDrugsActivity, 2
                )
            )
            adapter = actionsAdapter
        }
        collectLatestOnStart(viewModel.animalInfoState) {
            lookupAnimalInfoPresenter.animalInfoState = it
            takeNotesButtonPresenter.animalInfoState = it
            showAlertButtonPresenter.animalInfoState = it
        }
        collectLatestOnStart(viewModel.animalWeight) { animalWeight ->
            lookupAnimalInfoPresenter.animalWeight = animalWeight
        }
        collectLatestOnStart(
            combine(viewModel.isConfigured, viewModel.actions) { isConfigured, actions ->
                ActionsUpdate(isConfigured, actions)
            },
            ::updateActionsDisplay
        )
        collectLatestOnStart(eidReaderConnection.deviceConnectionState) { connectionState ->
            binding.buttonPanelTop.updateEIDReaderConnectionState(connectionState)
        }
        collectLatestOnStart(eidReaderConnection.isScanningForEID) { isScanning ->
            binding.buttonPanelTop.showScanningEID = isScanning
        }
        collectLatestOnStart(viewModel.canClearData) { canClear ->
            binding.buttonPanelTop.clearDataButton.isEnabled = canClear
        }
        collectLatestOnStart(viewModel.canSaveToDatabase) { canSave ->
            binding.buttonPanelTop.mainActionButton.isEnabled = canSave
        }
        collectLatestOnStart(viewModel.canAddDewormer) { canAddDewormer ->
            binding.buttonAddDewormer.isEnabled = canAddDewormer
        }
        collectLatestOnStart(viewModel.canAddVaccine) { canAddVaccine ->
            binding.buttonAddVaccine.isEnabled = canAddVaccine
        }
        observeOneTimeEventsOnStart(eidReaderConnection.onEIDScanned) { eidNumber ->
            viewModel.lookupAnimalInfoByEIDNumber(eidNumber)
        }
        observeOneTimeEventsOnStart(scaleDeviceConnection.onWeightScanned) { scanResult ->
            viewModel.onWeightValueEntered(scanResult.weight)
        }
        observeOneTimeEventsOnStart(viewModel.animalAlertsEvent) { event ->
            AnimalDialogs.showAnimalAlert(this, event.alertText)
        }
        observeOneTimeEventsOnStart(viewModel.events, ::handleEvent)

        drugTypeSelectionPresenter = drugTypeSelectionPresenter(
            button = binding.spinnerDrugTypeSelection,
            excludedDrugTypeIds = setOf(DrugType.ID_VACCINE, DrugType.ID_DEWORMER)
        ) { drugType ->
            selectedDrugType = drugType
            drugTypeSelectionPresenter.displaySelectedItem(drugType)
        }

        binding.buttonAddOtherDrug.setOnClickListener {
            selectedDrugType?.let { drugType ->
                viewModel.onConfigureDrugAction(drugType.id)
            }
        }

        onBackPressedDispatcher.addCallback(this, backConfirmationHandler)

        configureDisplayForAction()
    }

    private fun configureDisplayForAction() {
        title = getString(
            if (isVaccineAndDeworm) R.string.title_activity_vaccine_deworm
            else R.string.title_activity_give_drugs
        )
        binding.textConfigurationRequired.text = getString(
            if (isVaccineAndDeworm) R.string.text_vaccine_dewormer_configuration_required
            else R.string.text_administer_drugs_configuration_required
        )
        binding.containerAddOtherDrugs.isGone = isVaccineAndDeworm
        binding.containerAddVaccinesDewormers.isVisible = isVaccineAndDeworm
    }

    private fun onAnimalActionActivated(action: AnimalAction) {
        when (action) {
            is DrugAction -> {
                viewModel.onAnimalActionActivated(action)
            }
            is WeightAction -> {
                scaleDeviceConnection.scanWeight(REASON_SCAN_WEIGHT_FOR_ACTION_SET)
            }
        }
    }

    private fun onAnimalActionMenuActivated(action: AnimalAction) {
        when (action) {
            is DrugAction -> showDrugActionMenuBottomSheet(action)
            is WeightAction -> showManualWeightEntryDialog(action)
        }
    }

    private fun showDrugActionMenuBottomSheet(drugAction: DrugAction) {
        AnimalActionMenuBottomSheetDialog(
            this,
            title = drugAction.configuration.drug.nameAndLot,
            subtitle = drugAction.configuration.location.name
        ) { menuOption ->
            when (menuOption) {
                MenuOption.EDIT -> {
                    viewModel.onEditDrugAction(drugAction)
                }
                MenuOption.DELETE -> {
                    viewModel.onRemoveDrugAction(drugAction)
                }
            }
        }.show()
    }

    private fun showManualWeightEntryDialog(weightAction: WeightAction) {
        AnimalDialogs.manuallyEnterAnimalWeight(this, weightAction.weight) { weight ->
            viewModel.onWeightValueEntered(weight)
        }
    }

    private fun updateActionsDisplay(actionsUpdate: ActionsUpdate) {
        binding.containerConfigurationRequired.isVisible = !actionsUpdate.isConfigured
        binding.recyclerActionItems.isVisible = actionsUpdate.isConfigured
        actionsAdapter.updateActionSet(actionsUpdate.actions) {
            binding.recyclerActionItems.invalidateItemDecorations()
        }
    }

    private fun handleEvent(event: AdministerDrugsViewModel.Event) {
        when (event) {
            is AdministerDrugsViewModel.AddDrugConfigurationEvent -> {
                configureDrugActionLauncher.launch(
                    ConfigureDrugAction.ConfigureRequest(
                        event.drugTypeId,
                        event.excludedDrugIds
                    )
                )
            }
            is AdministerDrugsViewModel.EditDrugConfigurationEvent -> {
                editDrugActionLauncher.launch(
                    ConfigureDrugAction.EditRequest(
                        event.actionId,
                        event.configuration,
                        event.excludedDrugIds
                    )
                )
            }
            AdministerDrugsViewModel.UpdateDatabaseEvent.Success -> {
                Toast.makeText(
                    this,
                    if (isVaccineAndDeworm) R.string.toast_administer_vaccine_dewormer_success
                    else R.string.toast_administer_drugs_success,
                    Toast.LENGTH_SHORT
                ).show()
            }
            AdministerDrugsViewModel.UpdateDatabaseEvent.Error -> {
                Toast.makeText(
                    this,
                    if (isVaccineAndDeworm) R.string.toast_administer_vaccine_dewormer_error
                    else R.string.toast_administer_drugs_error,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun showExitConfirmationDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_confirm_exit)
            .setMessage(R.string.dialog_message_confirm_exit)
            .setPositiveButton(R.string.yes_label) { _, _ ->
                backConfirmationHandler.isEnabled = false
                super.onBackPressed()
            }
            .setNegativeButton(R.string.no_label, null)
            .create()
            .show()
    }

    private abstract class AnimalActionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private class DrugActionViewHolder(
        private val binding: ItemAnimalActionDrugBinding,
        private val onActionActivated: (DrugAction) -> Unit,
        private val onActionMenuActivated: (DrugAction) -> Unit
    ) : AnimalActionViewHolder(binding.root) {

        private val checkBoxDrawableComplete: Drawable
        private val checkBoxDrawableIncomplete: Drawable
        private val backgroundDrawableComplete: Drawable
        private val backgroundDrawableIncomplete: Drawable

        init {
            with(binding.root.context) {
                checkBoxDrawableComplete = requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.ic_check_box_checked)
                )
                checkBoxDrawableIncomplete = requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.ic_check_box_outline_blank)
                )
                backgroundDrawableComplete = requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.background_action_item_complete)
                )
                backgroundDrawableIncomplete= requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.background_action_item_incomplete)
                )
            }
        }

        fun bind(drugAction: DrugAction) {
            binding.root.background = if (drugAction.isComplete)
                backgroundDrawableComplete else backgroundDrawableIncomplete
            binding.imageCompleteness.setImageDrawable(
                if (drugAction.isComplete) checkBoxDrawableComplete
                else checkBoxDrawableIncomplete
            )
            binding.textDrugName.text = drugAction.configuration.drug.genericName
            binding.textDrugLocation.text = drugAction.configuration.location.abbreviation
            binding.textLot.text = drugAction.configuration.drug.lot
            binding.root.setOnClickListener {
                onActionActivated.invoke(drugAction)
            }
            binding.imageMoreOptions.setOnClickListener {
                onActionMenuActivated.invoke(drugAction)
            }
        }
    }

    private class WeightActionViewHolder(
        private val binding: ItemAnimalActionWeightBinding,
        private val onActionActivated: (WeightAction) -> Unit,
        private val onActionMenuActivated: (WeightAction) -> Unit
    ) : AnimalActionViewHolder(binding.root) {

        private val backgroundDrawableComplete: Drawable
        private val backgroundDrawableIncomplete: Drawable

        init {
            with(binding.root.context) {
                backgroundDrawableComplete = requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.background_action_item_complete)
                )
                backgroundDrawableIncomplete = requireNotNull(
                    ContextCompat.getDrawable(this, R.drawable.background_action_item_incomplete)
                )
            }
        }

        @SuppressLint("DefaultLocale")
        fun bind(weightAction: WeightAction) {
            binding.root.background = if (weightAction.isComplete)
                backgroundDrawableComplete else backgroundDrawableIncomplete
            binding.root.setOnClickListener {
                onActionActivated.invoke(weightAction)
            }
            binding.buttonEdit.setOnClickListener {
                onActionMenuActivated.invoke(weightAction)
            }
            binding.textTapToScanWeight.isInvisible = weightAction.weight != null
            binding.textWeight.isGone = weightAction.weight == null
            binding.textWeight.text = weightAction.weight?.let {
                "${String.format("%.2f", it)} ${weightAction.units.abbreviation}"
            }
        }
    }

    private class AnimalActionAdapter(
        private val onActionActivated: (AnimalAction) -> Unit,
        private val onActionMenuActivated: (AnimalAction) -> Unit,
    ) : RecyclerView.Adapter<AnimalActionViewHolder>() {

        companion object {

            private const val VIEW_TYPE_DRUG_ACTION = 0
            private const val VIEW_TYPE_WEIGHT_ACTION = 1

            private val ITEM_CALLBACK = object : DiffUtil.ItemCallback<AnimalAction>() {
                override fun areItemsTheSame(oldItem: AnimalAction, newItem: AnimalAction): Boolean {
                    return oldItem.actionId == newItem.actionId
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldItem: AnimalAction, newItem: AnimalAction): Boolean {
                    return oldItem == newItem
                }
            }
        }

        private val asyncListDiffer = AsyncListDiffer(this, ITEM_CALLBACK)

        fun updateActionSet(actionSet: ActionSet?, callback: () -> Unit = {}) {
            val listToSubmit = actionSet?.let {
                it.vaccines + it.dewormers + it.otherDrugs + it.weight
            } ?: emptyList()
            asyncListDiffer.submitList(listToSubmit, callback)
        }

        override fun getItemCount(): Int {
            return asyncListDiffer.currentList.size
        }

        override fun getItemViewType(position: Int): Int {
            return when (position) {
                asyncListDiffer.currentList.size - 1 -> VIEW_TYPE_WEIGHT_ACTION
                else -> VIEW_TYPE_DRUG_ACTION
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalActionViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            return when (viewType) {
                VIEW_TYPE_DRUG_ACTION -> DrugActionViewHolder(
                    binding = ItemAnimalActionDrugBinding.inflate(layoutInflater),
                    onActionActivated = onActionActivated,
                    onActionMenuActivated = onActionMenuActivated
                )
                VIEW_TYPE_WEIGHT_ACTION -> WeightActionViewHolder(
                    binding = ItemAnimalActionWeightBinding.inflate(layoutInflater),
                    onActionActivated = onActionActivated,
                    onActionMenuActivated = onActionMenuActivated
                )
                else -> throw IllegalStateException("Unknown View Type")
            }
        }

        override fun onBindViewHolder(holder: AnimalActionViewHolder, position: Int) {
            when (getItemViewType(position)) {
                VIEW_TYPE_DRUG_ACTION -> holder.takeAs<DrugActionViewHolder>()?.let { drugActionVH ->
                    asyncListDiffer.currentList[position].takeAs<DrugAction>()?.let { drugAction ->
                        drugActionVH.bind(drugAction)
                    }
                }
                VIEW_TYPE_WEIGHT_ACTION -> holder.takeAs<WeightActionViewHolder>()?.let { weightActionVH ->
                    asyncListDiffer.currentList[position]?.takeAs<WeightAction>()?.let { weightAction ->
                        weightActionVH.bind(weightAction)
                    }
                }
            }
        }
    }

    private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

        private val appContext = context.applicationContext

        override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
            return when (modelClass) {
                AdministerDrugsViewModel::class.java -> {
                    val databaseHandler = DatabaseHandler.create(appContext)
                    val animalRepo = AnimalRepositoryImpl(databaseHandler)
                    val defaultSettingsRepo = DefaultSettingsRepositoryImpl(databaseHandler)
                    val unitsRepository = UnitOfMeasureRepositoryImpl(databaseHandler)
                    val activeDefaultSettings = ActiveDefaultSettings(
                        PreferenceManager.getDefaultSharedPreferences(appContext)
                    )
                    val loadDefaultSettings = LoadActiveDefaultSettings(
                        activeDefaultSettings = activeDefaultSettings,
                        defaultSettingsRepo = defaultSettingsRepo
                    )
                    @Suppress("UNCHECKED_CAST")
                    AdministerDrugsViewModel(
                        animalRepo = animalRepo,
                        loadDefaultSettings = loadDefaultSettings,
                        unitsRepository = unitsRepository
                    ) as T
                }
                else -> throw IllegalStateException("${modelClass.simpleName} is not supported.")
            }
        }
    }
}
