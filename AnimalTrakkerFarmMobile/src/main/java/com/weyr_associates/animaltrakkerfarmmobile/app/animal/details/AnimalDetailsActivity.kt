package com.weyr_associates.animaltrakkerfarmmobile.app.animal.details

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalDrugHistoryFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalDrugHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalEvaluationHistoryFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalEvaluationsHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalHistoryFragmentFactory
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalHistoryTabsAdapter
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalNotesFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalNotesViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueSampleHistoryFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueSampleHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueTestHistoryFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.history.AnimalTissueTestHistoryViewModelContract
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.info.AnimalDetailedInfoPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.collectLatestOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.requireAs
import com.weyr_associates.animaltrakkerfarmmobile.app.core.viewBinding
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityAnimalDetailsBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentAnimalDetailsHistoryBinding
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentAnimalDetailsInfoBinding

private const val EXTRA_ANIMAL_ID = "EXTRA_ANIMAL_ID"

class AnimalDetailsActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context, animalId: Int) =
            Intent(context, AnimalDetailsActivity::class.java).apply {
                putExtra(EXTRA_ANIMAL_ID, animalId)
            }
    }

    private val binding by lazy {
        ActivityAnimalDetailsBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<AnimalDetailsViewModel> {
        ViewModelFactory(this, extractAnimalIdFrom(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        with(binding.viewPagerAnimalDetails) {
            adapter = AnimalDetailsTabsAdapter(this@AnimalDetailsActivity)
            isUserInputEnabled = false
        }
        TabLayoutMediator(
            binding.tabsAnimalDetails,
            binding.viewPagerAnimalDetails,
            true,
            false
        ) { tab, position ->
            tab.setText(
                when(position) {
                    0 -> R.string.text_info
                    1 -> R.string.text_history
                    else -> throw IllegalStateException("Invalid position: $position")
                }
            )
        }.attach()
        collectLatestOnStart(viewModel.animalDetails) { animalDetailsState ->
            when(animalDetailsState) {
                is AnimalDetailsViewModel.AnimalDetailsLoaded -> {
                    title = getString(
                        R.string.title_activity_animal_details_format,
                        animalDetailsState.animalDetails.basicInfo.name
                    )
                }
                else -> setTitle(R.string.title_activity_animal_details)
            }
        }
    }

    private class AnimalDetailsTabsAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            return when(position) {
                0 -> AnimalDetailsInfoFragment()
                1 -> AnimalDetailsHistoryFragment()
                else -> throw IllegalStateException("Invalid position: $position")
            }
        }
    }
}

//Slightly hacky way to get animal id into VM factory
//for child fragments.
private fun extractAnimalIdFrom(activity: Activity): Int {
    return activity.requireAs<AnimalDetailsActivity>().intent
        .getIntExtra(EXTRA_ANIMAL_ID, -1)
}

class AnimalDetailsInfoFragment : Fragment(R.layout.fragment_animal_details_info) {

    private val binding by viewBinding<FragmentAnimalDetailsInfoBinding>()

    private val viewModel by activityViewModels<AnimalDetailsViewModel> {
        ViewModelFactory(requireContext(), extractAnimalIdFrom(requireActivity()))
    }

    private val animalDetailsInfoPresenter = AnimalDetailedInfoPresenter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animalDetailsInfoPresenter.binding = binding.animalDetailedInfo
        collectLatestOnStart(viewModel.animalDetails) {
            when (it) {
                AnimalDetailsViewModel.AnimalDetailsLoading -> {
                    binding.containerAnimalDetailedInfo.isGone = true
                    binding.containerNoAnimalDetailedInfo.isGone = true
                    animalDetailsInfoPresenter.animalDetails = null
                }
                AnimalDetailsViewModel.AnimalDetailsNotFound -> {
                    binding.containerAnimalDetailedInfo.isGone = true
                    binding.containerNoAnimalDetailedInfo.isGone = false
                    animalDetailsInfoPresenter.animalDetails = null
                }
                is AnimalDetailsViewModel.AnimalDetailsLoaded -> {
                    binding.containerAnimalDetailedInfo.isGone = false
                    binding.containerNoAnimalDetailedInfo.isGone = true
                    animalDetailsInfoPresenter.animalDetails = it.animalDetails
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        animalDetailsInfoPresenter.binding = null
    }
}

class AnimalDetailsHistoryFragment : Fragment(R.layout.fragment_animal_details_history) {

    private val binding by viewBinding<FragmentAnimalDetailsHistoryBinding>()

    private val animalHistoryFragmentFactory = object : AnimalHistoryFragmentFactory {
        override fun createAnimalNotesFragment(): AnimalNotesFragment {
            return AnimalDetailsNotesFragment()
        }
        override fun createAnimalDrugHistoryFragment(): AnimalDrugHistoryFragment {
            return AnimalDetailsDrugsFragment()
        }
        override fun createAnimalTissueSampleHistoryFragment(): AnimalTissueSampleHistoryFragment {
            return AnimalDetailsTissueSamplesFragment()
        }
        override fun createAnimalTissueTestSampleFragment(): AnimalTissueTestHistoryFragment {
            return AnimalDetailsTissueTestsFragment()
        }
        override fun createAnimalEvaluationHistoryFragment(): AnimalEvaluationHistoryFragment {
            return AnimalDetailsEvaluationsFragment()
        }
    }

    private lateinit var animalHistoriesAdapter: AnimalHistoryTabsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        animalHistoriesAdapter = AnimalHistoryTabsAdapter(this, animalHistoryFragmentFactory)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.viewPagerAnimalHistory){
            adapter = animalHistoriesAdapter
            isUserInputEnabled = false
        }
        TabLayoutMediator(
            binding.tabsAnimalHistory,
            binding.viewPagerAnimalHistory,
            true,
            false,
            animalHistoriesAdapter,
        ).attach()
    }
}

class AnimalDetailsNotesFragment : AnimalNotesFragment() {
    override val viewModel: AnimalNotesViewModelContract
        by activityViewModels<AnimalDetailsViewModel> {
            ViewModelFactory(
                requireContext(),
                extractAnimalIdFrom(requireActivity())
            )
        }
}

class AnimalDetailsDrugsFragment : AnimalDrugHistoryFragment() {
    override val viewModel: AnimalDrugHistoryViewModelContract
        by activityViewModels<AnimalDetailsViewModel> {
            ViewModelFactory(
                requireContext(),
                extractAnimalIdFrom(requireActivity())
            )
        }
}

class AnimalDetailsTissueSamplesFragment : AnimalTissueSampleHistoryFragment() {
    override val viewModel: AnimalTissueSampleHistoryViewModelContract
        by activityViewModels<AnimalDetailsViewModel> {
            ViewModelFactory(
                requireContext(),
                extractAnimalIdFrom(requireActivity())
            )
        }
}

class AnimalDetailsTissueTestsFragment : AnimalTissueTestHistoryFragment() {
    override val viewModel: AnimalTissueTestHistoryViewModelContract
        by activityViewModels<AnimalDetailsViewModel> {
            ViewModelFactory(
                requireContext(),
                extractAnimalIdFrom(requireActivity())
            )
        }
}

class AnimalDetailsEvaluationsFragment : AnimalEvaluationHistoryFragment() {
    override val viewModel: AnimalEvaluationsHistoryViewModelContract
        by activityViewModels<AnimalDetailsViewModel> {
            ViewModelFactory(
                requireContext(),
                extractAnimalIdFrom(requireActivity())
            )
        }
}

private class ViewModelFactory(context: Context, private val animalId: Int) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        return when (modelClass) {
            AnimalDetailsViewModel::class.java -> {
                val databaseHandler = DatabaseHandler.create(appContext)
                val animalRepo = AnimalRepositoryImpl(databaseHandler)
                @Suppress("UNCHECKED_CAST")
                AnimalDetailsViewModel(animalId, animalRepo) as T
            }
            else -> throw IllegalStateException("${modelClass.simpleName} is not supported.")
        }
    }
}
