package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Sex

interface SexRepository {
    fun querySexes(speciesId: Int): List<Sex>
    fun querySex(id: Int): Sex?
}
