package com.weyr_associates.animaltrakkerfarmmobile.app.core

import android.app.Activity
import android.content.Intent
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseManagementActivity

fun Activity.hideKeyboard() {
    getSystemService(InputMethodManager::class.java).hideSoftInputFromWindow(
        window.decorView.rootView.windowToken, 0
    )
}

fun Activity.checkDatabaseIsPresentThen(onPresent: () -> Unit) {
    if (DatabaseHandler.isDatabasePresent(this)) {
        onPresent.invoke()
    } else {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_install_database)
            .setMessage(R.string.dialog_message_install_database)
            .setPositiveButton(R.string.yes_label) { _, _ ->
                this.startActivity(Intent(this, DatabaseManagementActivity::class.java))
            }
            .setNegativeButton(R.string.no_label) { _, _ -> }
                .create().show()
    }
}
