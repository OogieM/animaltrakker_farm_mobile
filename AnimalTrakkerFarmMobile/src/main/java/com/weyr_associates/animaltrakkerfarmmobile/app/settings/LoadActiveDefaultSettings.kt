package com.weyr_associates.animaltrakkerfarmmobile.app.settings

import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.DefaultSettingsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LoadActiveDefaultSettings(
    private val activeDefaultSettings: ActiveDefaultSettings,
    private val defaultSettingsRepo: DefaultSettingsRepository
) {
    suspend operator fun invoke(): DefaultSettings {
        val defaultSettingsId = activeDefaultSettings.loadActiveDefaultSettingsId()
        return withContext(Dispatchers.IO) {
            requireNotNull(
                defaultSettingsRepo.queryDefaultSettingsById(defaultSettingsId)
            )
        }
    }
}
