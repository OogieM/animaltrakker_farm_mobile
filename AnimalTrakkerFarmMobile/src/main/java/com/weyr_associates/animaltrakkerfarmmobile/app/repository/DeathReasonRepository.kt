package com.weyr_associates.animaltrakkerfarmmobile.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.DeathReason
import com.weyr_associates.animaltrakkerfarmmobile.model.UserType

interface DeathReasonRepository {
    suspend fun queryDefaultDeathReasons(): List<DeathReason>
    suspend fun queryDeathReasonsByUser(userId: Int, userType: UserType): List<DeathReason>
}
