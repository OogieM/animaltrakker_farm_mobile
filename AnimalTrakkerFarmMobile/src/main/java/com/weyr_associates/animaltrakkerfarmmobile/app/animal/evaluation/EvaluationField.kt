package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

data class EvaluationField(
    val id: EvaluationFieldId,
    val traitId: Int = -1,
    val traitName: String = "",
    val traitEntry: Entry = Entry.UNCOLLECTED,
) {

    enum class Entry {
        REQUIRED,
        OPTIONAL,
        UNCOLLECTED;

        val isNotRequired
            get() = this != REQUIRED

        val isUncollected
            get() = this == UNCOLLECTED
    }
}
