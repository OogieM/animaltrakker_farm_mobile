package com.weyr_associates.animaltrakkerfarmmobile.app.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.weyr_associates.animaltrakkerfarmmobile.R;
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar;
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler;
import com.weyr_associates.animaltrakkerfarmmobile.database.BreedTable;
import com.weyr_associates.animaltrakkerfarmmobile.database.Cursors;
import com.weyr_associates.animaltrakkerfarmmobile.database.DefaultSettingsTable;
import com.weyr_associates.animaltrakkerfarmmobile.databinding.SetDefaultsBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SetDefaults extends AppCompatActivity {
	private DatabaseHandler dbh;
	public Cursor cursor, breedCursor, ownerCursor, labCursor, evalCursor, evalCursor2;
	public Object crsr, breedcrsr, ownercrsr, labcrsr, evalcrsr, evalcrsr2;
	public Spinner eid_tag_type_spinner, eid_tag_location_spinner, eid_tag_male_color_spinner, eid_tag_female_color_spinner;
	public Spinner federal_tag_type_spinner, federal_tag_location_spinner, federal_tag_male_color_spinner, federal_tag_female_color_spinner;
	public Spinner farm_tag_type_spinner, farm_tag_location_spinner, farm_tag_male_color_spinner, farm_tag_female_color_spinner;
	public Spinner species_spinner, animal_breed_spinner, registry_spinner;
	public Spinner owner_name_spinner, lab_name_spinner, custom_evaluation_spinner;
	public List<String> possible_custom_evaluation, possible_custom_evaluation_id_order, possible_custom_evaluation_display_order;
	public List<String> possible_labs_display_order, possible_labs_id_order, possible_owners_display_order, possible_owners_id_order;
	public List<String> sample_container_type, sample_container_type_id_order, sample_container_type_display_order;
	public Spinner sample_type_spinner, sample_test_spinner, sample_container_type_spinner;
	public List<String> sample_type, sample_type_id_order, sample_type_display_order;
	public List<String> sample_test, sample_test_id_order, sample_test_display_order;
	public List<Integer> possible_owners_id, possible_custom_evaluation_id;
	public List<String> possible_owners, possible_labs;
	public String default_species, default_breed, default_sample_type, default_sample_test, default_container_type, default_lab, default_custom_evaluation;
	public int owner_id_contactid;
	public int current_breed_id, current_tag_type_id, current_lab_name_id, last_location, default_owner, lab_location;
	public int sample_type_location, sample_test_location, sample_container_type_location, custom_evaluation_location;
	public List<String> animal_breed, animal_breed_id_order, animal_breed_display_order;
	public int which_breed, which_species, which_lab, which_owner, which_test, which_tissue, which_container, which_custom_test;


	public List<String> tag_types, tag_locations, tag_colors;
	ArrayAdapter<String> dataAdapter;
	public String cmd;
	public Button btn;
	public List<String> species_name_list, flock_name_list, registry_name_list, breed_name_list;
	public List<String> species_id_list, flock_id_list, registry_id_list, breed_id_list;
	public Map<String,Integer> animaltrakker_defaults;
	public String default_sex;

	public int breed_location, species_location;

	public SimpleCursorAdapter myadapter;
	public RadioGroup radioGroup, radioGroup2;

	private SetDefaultsBinding binding;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = SetDefaultsBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		Log.i("SetDefaults", " after set content view");
		String dbname = getString(R.string.real_database_file);
		dbh = new DatabaseHandler(this, dbname);
		Log.i("database is ", dbname);
// todo this really should have a where clause that picks the current user,
//  either by contactid or companyid so that we can store multiple default settings
//  It will need to handle a user of either type and be something it gets on startup

		// todo also need to refactor all these to use multiple classes and reference by column names
		//  not integer positions. Will require massive code changes in all sections but make system more easily expanded later.
		//  I'd like to see about making that a library or a utility so I don't have to repeat the code in all the activities.
		//  Considering a Java hash table

		animaltrakker_defaults = DefaultSettingsTable.readAsMapFrom(this, dbh);

//		Make the update database button green
		binding.buttonPanelTop.show(TopButtonBar.UI_ACTION_UPDATE_DATABASE);
		btn = binding.buttonPanelTop.getMainActionButton();
		btn.setEnabled(true);
		btn.setOnClickListener(view -> updateDatabase());

// 		Fill the Species Spinner
			Log.i("in create ", " ready to get species list");
			// todo set order properly
			cmd = "select id_speciesid, species_common_name from species_table ";
			Log.i("fill species spinner ", "command is " + cmd);
			crsr = dbh.exec(cmd);
			Log.i("in create ", "after execute comand");
			cursor = (Cursor) crsr;
			dbh.moveToFirstRecord();
			species_spinner = binding.speciesSpinner;
			species_name_list = new ArrayList<String>();
			species_id_list = new ArrayList<String>();
			species_name_list.add("Select Species");
			species_id_list.add("species_id");
			Log.i("fill species spinner ", "added first option ");
			// looping through all rows and adding to list
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				species_id_list.add(cursor.getString(0));
				Log.i("setDefaults", "species id order " + cursor.getString(0));
				species_name_list.add(cursor.getString(1));
				Log.i("setDefaults", "species " + cursor.getString(1));
			}

			// Creating adapter for spinner
			Log.i("before array adapters ", "first one ");
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, species_name_list);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			species_spinner.setAdapter(dataAdapter);
			Log.i("in create ", " after species ");
			//	set initial to be current default
			default_species = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.SPECIES_ID));
			species_location = species_id_list.indexOf(default_species);
			which_species = species_location;
			species_spinner.setSelection(species_location);
			species_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					which_species = species_spinner.getSelectedItemPosition();
					//  Select the fields we need from the list of animal breeds
					cmd = String.format("SELECT id_breedid, breed_name, breed_display_order FROM breed_table WHERE id_speciesid = '%s' ORDER BY breed_display_order", which_species);
					Log.i("fill breed spinner", "command is " + cmd);
					breedcrsr = dbh.exec(cmd);
					breedCursor = (Cursor) breedcrsr;
					dbh.moveToFirstRecord();
					animal_breed = new ArrayList<String>();
					animal_breed_id_order = new ArrayList<String>();
					animal_breed_display_order = new ArrayList<String>();
					animal_breed.add("Select a Breed");
					animal_breed_id_order.add("animal_breed_id");
					animal_breed_display_order.add("animal_breed_display_order");
					for (breedCursor.moveToFirst(); !breedCursor.isAfterLast(); breedCursor.moveToNext()) {
						animal_breed_id_order.add(Cursors.getString(breedCursor, BreedTable.Columns.ID));
						Log.i("BreedingApp", "animal_breed_id_order " + animal_breed_id_order);
						String breedName = Cursors.getString(breedCursor, BreedTable.Columns.NAME);
						animal_breed.add(breedName);
						Log.i("BreedingApp", "animal_breed" + breedName);
						String breedDisplayOrder = Cursors.getString(breedCursor, BreedTable.Columns.ORDER);
						animal_breed_display_order.add(breedDisplayOrder);
						Log.i("BreedingApp", "animal_breed_display_order " + breedDisplayOrder);
					}
					// Creating adapter for spinner
					dataAdapter = new ArrayAdapter<String>(SetDefaults.this, R.layout.large_spinner_item, animal_breed);
					animal_breed_spinner.setAdapter(dataAdapter);
					dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
//				animal_breed_spinner.setSelection(0);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) {
	// your code here


				}
			});

			//  Select the fields we need from the list of animal breeds
			cmd = String.format("SELECT id_breedid, breed_name, breed_display_order FROM breed_table WHERE id_speciesid = '%s' ORDER BY breed_display_order", default_species);
			Log.i("fill breed spinner", "command is " + cmd);
			breedcrsr = dbh.exec(cmd);
			breedCursor = (Cursor) breedcrsr;
			dbh.moveToFirstRecord();
			animal_breed = new ArrayList<String>();
			animal_breed_id_order = new ArrayList<String>();
			animal_breed_display_order = new ArrayList<String>();
			animal_breed.add("Select a Breed");
			animal_breed_id_order.add("animal_breed_id");
			animal_breed_display_order.add("animal_breed_display_order");
			for (breedCursor.moveToFirst(); !breedCursor.isAfterLast(); breedCursor.moveToNext()) {
				String breedId = Cursors.getString(breedCursor, BreedTable.Columns.ID);
				animal_breed_id_order.add(breedId);
				Log.i("SetDefaults", "animal_breed_id_order " + breedId);
				String breedName = Cursors.getString(breedCursor, BreedTable.Columns.NAME);
				animal_breed.add(breedName);
				Log.i("SetDefaults", "animal_breed" + breedName);
				String breedDisplayOrder = Cursors.getString(breedCursor, BreedTable.Columns.ORDER);
				animal_breed_display_order.add(breedDisplayOrder);
				Log.i("SetDefaults", "animal_breed_display_order " + breedDisplayOrder);
			}
			// Creating adapter for spinner
			animal_breed_spinner = binding.animalBreedSpinner;
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, animal_breed);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			animal_breed_spinner.setAdapter(dataAdapter);
			//  Need to first get the default which is the actual primary key in the breed_table
			//  Then need to find the spinner number that corresponds to that breed
			default_breed = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.BREED_ID));
			Log.i("breed default is  ", " is " + default_breed);
			breed_location = animal_breed_id_order.indexOf(default_breed);
			Log.i("breed location ", " is " + breed_location);
			//  Then set the spinner to that number
			animal_breed_spinner.setSelection(breed_location);

			//  Fill the lab Spinner
			lab_name_spinner = binding.labNameSpinner;
			possible_labs = new ArrayList<String>();
			possible_labs_id_order = new ArrayList<String>();
			possible_labs_display_order = new ArrayList<String>();
			//  Select the fields we need from the list of owners that are laboratories
			cmd = "SELECT company_table.id_companyid\n" +
					"\t, company_table.company\n" +
					"\t, company_laboratory_table.laboratory_display_order \n" +
					"FROM company_table\n" +
					"INNER JOIN company_laboratory_table on company_table.id_companyid = company_laboratory_table.id_companyid \n" +
					"ORDER BY company_laboratory_table.laboratory_display_order ";
			Log.i("fill lab spinner", "command is " + cmd);
			labcrsr = dbh.exec(cmd);
			labCursor = (Cursor) labcrsr;
			dbh.moveToFirstRecord();
			possible_labs.add("Select a Laboratory");
			possible_labs_id_order.add("possible_labs_id_order");
			possible_labs_display_order.add("possible_labs_display_order");
			for (labCursor.moveToFirst(); !labCursor.isAfterLast(); labCursor.moveToNext()) {
				possible_labs_id_order.add(labCursor.getString(0));
				possible_labs.add(labCursor.getString(1));
				possible_labs_display_order.add(labCursor.getString(2));
			}
			// Creating adapter for spinner
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, possible_labs);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			lab_name_spinner.setAdapter(dataAdapter);
			default_lab = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.LAB_COMPANY_ID));
			lab_location = possible_labs_id_order.indexOf(default_lab);
			lab_name_spinner.setSelection(lab_location);

			//	Fill the sample Type Spinner
			sample_type_spinner = binding.sampleTypeSpinner;
			Log.i("SetDefaults", " after get the place to fill the spinner ");
			sample_type = new ArrayList<String>();
			sample_type_id_order = new ArrayList<String>();
			sample_type_display_order = new ArrayList<String>();
			cmd = "SELECT id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_display_order  " +
					" FROM tissue_sample_type_table ORDER BY tissue_sample_type_display_order ASC ";
			Log.i("tissue types", "command is " + cmd);
			crsr = dbh.exec(cmd);
			cursor = (Cursor) crsr;
			dbh.moveToFirstRecord();
			sample_type.add("Select a Sample Type");
			sample_type_id_order.add("sample_type_id");
			sample_type_display_order.add("sample_type_display_order");
			Log.i("tissue types", "after add sample type order  ");
			// looping through all rows and adding to list
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				Log.i("TissueSample", " adding tissue id " + cursor.getInt(0));
				sample_type_id_order.add(cursor.getString(0));
				Log.i("TissueSample", " adding tissue type " + cursor.getString(1));
				sample_type.add(cursor.getString(1));
				Log.i("TissueSample", " in loop fill spinner after get sample type ");
				sample_type_display_order.add(cursor.getString(2));
				Log.i("TissueSample", " in loop fill spinner after get sample type display order ");
			}
			Log.i("TissueSample", " after filling tissue sample type spinner");
			// Creating adapter for spinner
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, sample_type);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			sample_type_spinner.setAdapter(dataAdapter);
			// todo if I add things into the defaults I may have to fix which default location (field) I am looking at.
			default_sample_type = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TISSUE_SAMPLE_TYPE_ID));
			Log.i("sample type default ", " is " + default_sample_type);
			sample_type_location = sample_type_id_order.indexOf(default_sample_type);
			Log.i("sample type location ", " is " + sample_type_location);
			//  Then set the spinner to that number
			sample_type_spinner.setSelection(sample_type_location);
			Log.i("TissueSample", " after display tissue type spinner");

			//	Fill the tissue test selection spinner
			sample_test_spinner = binding.sampleTestSpinner;
			sample_test = new ArrayList<String>();
			sample_test_id_order = new ArrayList<String>();
			sample_test_display_order = new ArrayList<String>();
			// Select All fields from test options to build the spinner
			cmd = "select id_tissuetestid, tissue_test_abbrev, tissue_test_display_order from " +
					"tissue_test_table order by tissue_test_display_order";
			Log.i("fill sample spinner", "command is " + cmd);
			crsr = dbh.exec(cmd);
			cursor = (Cursor) crsr;
			dbh.moveToFirstRecord();
			sample_test.add("Select a Test");
			sample_test_id_order.add("sample_test_id_order");
			sample_test_display_order.add("sample_test_display_order");
			// looping through all rows and adding to list
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				sample_test_id_order.add(cursor.getString(0));
				sample_test.add(cursor.getString(1));
				Log.i("TissueSample", " tissue test " + cursor.getString(1));
				sample_test_display_order.add(cursor.getString(2));
				Log.i("TissueSample", " after add to tissue test display spinner " + cursor.getString(2));
			}
			// Creating adapter for spinner
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, sample_test);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			sample_test_spinner.setAdapter(dataAdapter);
			default_sample_test = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TISSUE_TEST_ID));
			Log.i("sampletest default is ", " is " + default_sample_test);
			sample_test_location = sample_test_id_order.indexOf(default_sample_test);
			sample_test_spinner.setSelection(sample_test_location);

//	    Fill the container type selection spinner
			sample_container_type_spinner = binding.sampleContainerTypeSpinner;
			sample_container_type_id_order = new ArrayList<String>();
			sample_container_type = new ArrayList<String>();
			sample_container_type_display_order = new ArrayList<String>();
			// Select All fields from test options to build the spinner
			cmd = "select id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_display_order " +
					"from tissue_sample_container_type_table order by tissue_sample_container_display_order";
			Log.i("fill container spinner", "command is " + cmd);
			crsr = dbh.exec(cmd);
			cursor = (Cursor) crsr;
			dbh.moveToFirstRecord();
			sample_container_type_id_order.add("sample_container_type_id_location");
			sample_container_type.add("Select a Container");
			sample_container_type_display_order.add("sample_container_type_display_order");
			// looping through all rows and adding to list
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
				sample_container_type_id_order.add(cursor.getString(0));
				Log.i("TissueSample", " tissue test id spinner " + cursor.getString(0));
				sample_container_type.add(cursor.getString(1));
				Log.i("TissueSample", " container " + cursor.getString(1));
				sample_container_type_display_order.add(cursor.getString(2));
				Log.i("TissueSample", " tissue test display spinner " + cursor.getString(2));
			}
			// Creating adapter for spinner
			Log.i("TissueSample", " before get container spinner ");
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, sample_container_type);
			Log.i("TissueSample", " after get container spinner ");
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			sample_container_type_spinner.setAdapter(dataAdapter);
			Log.i("TissueSample", " after set container spinner adapter");
			default_container_type = String.valueOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.TISSUE_CONTAINER_TYPE_ID));
			sample_container_type_location = sample_container_type_id_order.indexOf(default_container_type);
			sample_container_type_spinner.setSelection(sample_container_type_location);
			Log.i("TissueSample", " after set container selection ");

			// Fill the owner Spinner
			owner_name_spinner = binding.ownerNameSpinner;
			possible_owners = new ArrayList<String>();
			possible_owners_display_order = new ArrayList<String>();
			possible_owners_id = new ArrayList<Integer>();
			//  Select the fields we need from the list of owners
			cmd = "SELECT id_contactid, contact_first_name, contact_last_name from contact_table";
			Log.i("fill owner spinner", "command is " + cmd);
			ownercrsr = dbh.exec(cmd);
			ownerCursor = (Cursor) ownercrsr;
			dbh.moveToFirstRecord();
			possible_owners_display_order.add("owner display order");
			possible_owners.add("Select a Ranch or Owner");
			possible_owners_id.add (0);
			for (ownerCursor.moveToFirst(); !ownerCursor.isAfterLast(); ownerCursor.moveToNext()) {
				possible_owners_id.add(ownerCursor.getInt(0));
				possible_owners.add(ownerCursor.getString(1) + " " + ownerCursor.getString(2));
				//possible_owners_display_order.add();
			}
			// Creating adapter for spinner
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, possible_owners);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			owner_name_spinner.setAdapter(dataAdapter);
			//TODO: what about the default breeder?
			default_owner = possible_owners_id.indexOf(animaltrakker_defaults.get(DefaultSettingsTable.Columns.OWNER_CONTACT_ID));
			owner_name_spinner.setSelection(default_owner);

			//  Fill the Custom Evaluation Spinner
			custom_evaluation_spinner = binding.customEvaluationSpinner;
			possible_custom_evaluation = new ArrayList<String>();
			possible_custom_evaluation_id_order = new ArrayList<String>();
			possible_custom_evaluation_display_order = new ArrayList<String>();
			cmd = "SELECT id_evaluationtraitid, trait_name, evaluation_trait_display_order FROM evaluation_trait_table WHERE id_evaluationtraittypeid = 3 ORDER BY evaluation_trait_display_order ";
			Log.i("fill evaluation", " spinner command is " + cmd);
			evalcrsr = dbh.exec(cmd);
			evalCursor = (Cursor) evalcrsr;
			dbh.moveToFirstRecord();
			possible_custom_evaluation.add("Select Custom Evaluation");
			possible_custom_evaluation_id_order.add("possible_custom_evaluation_id_order");
			possible_custom_evaluation_display_order.add("possible_custom_evaluation_display_order");
			for (evalCursor.moveToFirst(); !evalCursor.isAfterLast(); evalCursor.moveToNext()) {
				possible_custom_evaluation_id_order.add(evalCursor.getString(0));
				Log.i("loop ", " possibleevalorder " + evalCursor.getString(0));
				possible_custom_evaluation.add(evalCursor.getString(1));
				Log.i("loop ", " customeval " + evalCursor.getString(1));
				possible_custom_evaluation_display_order.add(evalCursor.getString(2));
				Log.i("loop ", " evaldisplayorder " + evalCursor.getString(2));
			}
			// Creating adapter for spinner
			dataAdapter = new ArrayAdapter<String>(this, R.layout.large_spinner_item, possible_custom_evaluation);
			dataAdapter.setDropDownViewResource(R.layout.large_spinner_item);
			custom_evaluation_spinner.setAdapter(dataAdapter);
			cmd = "SELECT id_evaluationtraitid from last_evaluation_table WHERE id_lastevaluationid = 16";
			evalcrsr2 = dbh.exec(cmd);
			evalCursor2 = (Cursor) evalcrsr2;
			dbh.moveToFirstRecord();
			custom_evaluation_location = 0;
			default_custom_evaluation = String.valueOf(evalCursor2.getInt(0));
			Log.i("afterloop ", " which location " +custom_evaluation_location);
			Log.i("afterloop ", " which eval " + evalCursor2.getString(0));
			custom_evaluation_location = possible_custom_evaluation_id_order.indexOf(default_custom_evaluation);
			Log.i("afterloop ", " eval loc " + custom_evaluation_location);
			custom_evaluation_spinner.setSelection(custom_evaluation_location);
			Log.i("afterloop ", " after the spinner selection is set.  " );
		}

		private void updateDatabase() {
			TextView TV;
			String temp_string;
			Log.i("in save ", " defaults stub ");
			// Disable Update Database button and make it red to prevent getting 2 records at one time
			btn = binding.buttonPanelTop.getMainActionButton();
			btn.setEnabled(false);

			species_spinner = binding.speciesSpinner;
			which_species = species_spinner.getSelectedItemPosition();
			Log.i("in save ", " defaults new species is " + which_species);
			which_species = Integer.parseInt(species_id_list.get(which_species));
			Log.i("update ", "command is " + cmd);
			cmd = String.format("update animaltrakker_default_settings_table set id_speciesid = %s ", which_species);
//		Log.i("update ", "command is " + cmd);
			crsr = dbh.exec(cmd);
//		Log.i("in create ", "after execute comand");
			animal_breed_spinner = binding.animalBreedSpinner;
			which_breed = animal_breed_spinner.getSelectedItemPosition();
			if (which_breed < 1) {
				// Missing data so  display an alert
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(R.string.fill_breed)
						.setTitle(R.string.fill_breed);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int idx) {
						Button btn1;
						// User clicked OK button
						//  Set update database button green
						btn1 = binding.buttonPanelTop.getMainActionButton();
						btn1.setEnabled(true);
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			} else {
				which_breed = Integer.parseInt(animal_breed_id_order.get(which_breed));
				Log.i("update ", "command is " + cmd);
				cmd = String.format("update animaltrakker_default_settings_table set id_breedid = %s ", which_breed);
				Log.i("update ", "command is " + cmd);
				crsr = dbh.exec(cmd);
			}
			//todo instead of setting males by species this should be a selection in the top from the sexes for each species
			switch (which_species) {
				case 1: // Sheep
					default_sex = "1";
					break;
				case 2: // Goat
					default_sex = "5";
					break;
				case 3:// Cattle
					default_sex = "9";
					break;
				case 4:// Horse
					default_sex = "13";
					break;
				case 5:// Donkey
					default_sex = "17";
					break;
				case 6:// Pig
					default_sex = "21";
					break;
			}
			cmd = String.format("update animaltrakker_default_settings_table set id_sexid = %s ", default_sex);
			Log.i("set defaults ", "cmd is " + cmd);
			dbh.exec(cmd);

			lab_name_spinner = binding.labNameSpinner;
			which_lab = lab_name_spinner.getSelectedItemPosition();
			which_lab = Integer.parseInt(possible_labs_id_order.get(which_lab));
			cmd = String.format("update animaltrakker_default_settings_table set lab_id_companyid = %s ", which_lab);
			Log.i("update ", "command is " + cmd);
			dbh.exec(cmd);

			sample_test_spinner = binding.sampleTestSpinner;
			which_test = sample_test_spinner.getSelectedItemPosition();
			which_test = Integer.parseInt(sample_test_id_order.get(which_test));
			cmd = String.format("update animaltrakker_default_settings_table set id_tissuetestid = %s ", which_test);
			Log.i("update ", "command is " + cmd);
			dbh.exec(cmd);

			sample_type_spinner = binding.sampleTypeSpinner;
			which_tissue = sample_type_spinner.getSelectedItemPosition();
			which_tissue = Integer.parseInt(sample_type_id_order.get(which_tissue));
			cmd = String.format("update animaltrakker_default_settings_table set id_tissuesampletypeid = %s ", which_tissue);
			Log.i("update ", "command is " + cmd);
			dbh.exec(cmd);

			sample_container_type_spinner = binding.sampleContainerTypeSpinner;
			which_container = sample_container_type_spinner.getSelectedItemPosition();
			which_container = Integer.parseInt(sample_container_type_id_order.get(which_container));
			cmd = String.format("update animaltrakker_default_settings_table set id_tissuesamplecontainertypeid = %s ", which_container);
			Log.i("update ", "command is " + cmd);
			dbh.exec(cmd);

			owner_name_spinner = binding.ownerNameSpinner;
			which_owner = owner_name_spinner.getSelectedItemPosition();
			which_owner = possible_owners_id.get(which_owner);
			cmd = String.format("update animaltrakker_default_settings_table set owner_id_contactid = %s ", which_owner);
			Log.i("update ", "command is " + cmd);
			dbh.exec(cmd);

			custom_evaluation_spinner = binding.customEvaluationSpinner;
			which_custom_test = custom_evaluation_spinner.getSelectedItemPosition();
			//which_custom_test = Integer.parseInt(possible_custom_evaluation_id_order.get(which_custom_test));
			if (which_custom_test == 0) {
				Log.i("no test ", "no custom eval so don't update DB ");
			} else {
				cmd = String.format("UPDATE last_evaluation_table SET id_evaluationtraitid = %s WHERE id_lastevaluationid = 16 ", which_custom_test);
				Log.i("update ", "command is " + cmd);
				dbh.exec(cmd);
			}
		}
		@Override
		public void onDestroy () {
			super.onDestroy();
			dbh.close();
		}

		public void backBtn (View v )
		{
			// Added this to close the database if we go back to the main activity
			try {
				cursor.close();
			} catch (Exception r) {
				Log.i("back btn", "cursor RunTimeException: " + r);
			}
			dbh.closeDB();
			//Go back to main
			this.finish();
		}
	}

