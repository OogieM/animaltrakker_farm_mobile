package com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.ActionSet
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.AnimalAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug.DrugAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.weight.WeightAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.AnimalAlertEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.EmitsAnimalAlerts
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.alert.extractAnimalAlertEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.AnimalInfoLookup
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.channel.eventEmitter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.takeAs
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.UnitOfMeasureRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.util.UUID

class AdministerDrugsViewModel(
    private val animalRepo: AnimalRepository,
    private val loadDefaultSettings: LoadActiveDefaultSettings,
    private val unitsRepository: UnitOfMeasureRepository
) : ViewModel(), LookupAnimalInfo, EmitsAnimalAlerts {

    sealed interface Event

    data class AddDrugConfigurationEvent(
        val drugTypeId: Int,
        val excludedDrugIds: Set<Int>
    ) : Event

    data class EditDrugConfigurationEvent(
        val actionId: UUID,
        val configuration: DrugAction.Configuration,
        val excludedDrugIds: Set<Int>
    ) : Event

    sealed interface UpdateDatabaseEvent : Event {
        data object Success : UpdateDatabaseEvent
        data object Error : UpdateDatabaseEvent
    }

    private val animalInfoLookup = AnimalInfoLookup(viewModelScope, animalRepo)
    private val animalAlertEventEmitter = eventEmitter<AnimalAlertEvent>()
    private val internalEventsFlow = MutableSharedFlow<Event>()
    private val isUpdatingDatabase = MutableStateFlow(false)

    private val eventsChannel = Channel<Event>()
    val events = eventsChannel.receiveAsFlow()

    override val animalInfoState = animalInfoLookup.animalInfoState

    override val animalAlertsEvent: Flow<AnimalAlertEvent> =
        animalAlertEventEmitter.events

    val animalWeight = animalInfoState.flatMapConcat {
        flow {
            emit(null)
            if (it is LookupAnimalInfo.AnimalInfoState.Loaded) {
                emit(animalRepo.queryAnimalLastEvaluationWeight(it.animalBasicInfo.id))
            }
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, null)

    private val _actions = MutableStateFlow<ActionSet?>(null)
    val actions = _actions.asStateFlow()

    val isConfigured = actions.map { it?.isConfigured ?: false }
        .stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canAddDewormer = actions.map { it?.canAddDewormers ?: false }
        .stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canAddVaccine = actions.map { it?.canAddVaccines ?: false }
        .stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canClearData = actions.map { actionSet ->
        val actionSetIsConfigured = actionSet?.isConfigured ?: false
        val canClearVaccines = actionSet?.vaccines
            ?.any { it.isComplete != it.configuration.autoComplete } ?: false
        val canClearDewormers = actionSet?.dewormers
            ?.any { it.isComplete != it.configuration.autoComplete } ?: false
        val canClearOtherDrugs = actionSet?.otherDrugs
            ?.any { it.isComplete != it.configuration.autoComplete } ?: false
        val canClearAnyDrugs = canClearVaccines || canClearDewormers || canClearOtherDrugs
        val canClearWeight = actionSet?.weight?.isComplete ?: false
        val canClearAnyData = canClearAnyDrugs || canClearWeight
        actionSetIsConfigured && canClearAnyData
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    private val dataSaved = merge(
        internalEventsFlow.filterIsInstance<UpdateDatabaseEvent.Success>(),
        animalInfoState.filterIsInstance<LookupAnimalInfo.AnimalInfoState.Loaded>()
            .map { it.animalBasicInfo.id }.distinctUntilChanged(),
        actions
    ).map {
        when (it) {
            is UpdateDatabaseEvent.Success -> true
            else -> false
        }
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    val canSaveToDatabase = combine(
        animalInfoState,
        isUpdatingDatabase,
        dataSaved,
        actions
    ) { animalInfoState, isUpdatingDatabase, dataSaved, actionSet ->
        val actionSetIsConfigured = actionSet?.isConfigured ?: false
        val animalInfoLoaded = animalInfoState is LookupAnimalInfo.AnimalInfoState.Loaded
        val hasCompletedVaccines = actionSet?.vaccines?.any { it.isComplete } ?: false
        val hasCompletedDewormers = actionSet?.dewormers?.any { it.isComplete } ?: false
        val hasCompletedOtherDrugs = actionSet?.otherDrugs?.any { it.isComplete } ?: false
        !dataSaved && !isUpdatingDatabase && animalInfoLoaded && actionSetIsConfigured &&
                (hasCompletedVaccines || hasCompletedDewormers || hasCompletedOtherDrugs)
    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    init {
        viewModelScope.launch {
            _actions.update { initializeActionSet() }
        }
        animalAlertEventEmitter.forwardFrom(
            animalInfoState.extractAnimalAlertEvents()
        )
    }

    override fun lookupAnimalInfoById(animalId: Int) {
        animalInfoLookup.lookupAnimalInfoById(animalId)
    }

    override fun lookupAnimalInfoByEIDNumber(eidNumber: String) {
        animalInfoLookup.lookupAnimalInfoByEIDNumber(eidNumber)
    }

    override fun resetAnimalInfo() {
        animalInfoLookup.resetAnimalInfo()
    }

    fun onConfigureDrugAction(drugTypeId: Int) {
        val excludedDrugIds = collectExcludedDrugIdsForDrugType(drugTypeId)
        viewModelScope.launch {
            eventsChannel.send(
                AddDrugConfigurationEvent(
                    drugTypeId = drugTypeId,
                    excludedDrugIds = excludedDrugIds
                )
            )
        }
    }

    fun onDrugActionConfigured(configuration: DrugAction.Configuration) {
        _actions.update { it?.addDrugAction(configuration) }
    }

    fun onEditDrugAction(drugAction: DrugAction) {
        _actions.value?.let { actionSet ->
            if (actionSet.containsDrugAction(drugAction)) {
                val excludedDrugIds = collectExcludedDrugIdsForDrugType(
                    drugAction.configuration.drug.typeId
                ).filter { it != drugAction.configuration.drug.id }.toSet()
                viewModelScope.launch {
                    eventsChannel.send(
                        EditDrugConfigurationEvent(
                            actionId = drugAction.actionId,
                            configuration = drugAction.configuration,
                            excludedDrugIds = excludedDrugIds
                        )
                    )
                }
            }
        }
    }

    fun onDrugActionConfigurationEdited(actionId: UUID, configuration: DrugAction.Configuration) {
        _actions.update { it?.updateDrugAction(actionId, configuration) }
    }

    fun onRemoveDrugAction(drugAction: DrugAction) {
        _actions.update { it?.removeDrugAction(drugAction) }
    }

    fun onAnimalActionActivated(action: AnimalAction) {
        when (action) {
            is DrugAction -> {
                _actions.update { actionSet ->
                    actionSet?.markDrugActionComplete(action, !action.isComplete)
                }
            }
        }
    }

    fun onWeightValueEntered(weight: Float?) {
        _actions.update {
            it?.copy(weight = it.weight.copy(weight = weight))
        }
    }

    fun clearData() {
        if (canClearData.value) {
            executeClearData()
        }
    }

    fun saveToDatabase() {
        if (canSaveToDatabase.value) {
            viewModelScope.launch {
                updateDatabase()
            }
        }
    }

    private suspend fun initializeActionSet(): ActionSet {
        return ActionSet(
            weight = WeightAction(
                units = requireNotNull(
                    unitsRepository.queryUnitOfMeasureById(
                        loadDefaultSettings().weightUnitsId
                    )
                )
            )
        )
    }

    private fun collectExcludedDrugIdsForDrugType(drugTypeId: Int): Set<Int> {
        return when (drugTypeId) {
            DrugType.ID_VACCINE -> actions.value?.vaccines
            DrugType.ID_DEWORMER -> actions.value?.dewormers
            else -> actions.value?.otherDrugs
        }?.map {
            it.configuration.drug.id
        }?.toSet() ?: emptySet()
    }

    private fun executeClearData() {
        _actions.update {
            it?.resetAllActions()
        }
    }

    private suspend fun updateDatabase() {
        try {

            isUpdatingDatabase.value = true

            val animalInfo = animalInfoLookup.animalInfoState.value
                .takeAs<LookupAnimalInfo.AnimalInfoState.Loaded>()
                ?.animalBasicInfo ?: return

            val actionSet = actions.value ?: return

            withContext(Dispatchers.IO) {
                animalRepo.trackActionsForAnimal(
                    animalId = animalInfo.id,
                    animalInfo.ageInDays(),
                    actions = actionSet,
                    timeStamp = LocalDateTime.now()
                )
            }

            animalInfoLookup.lookupAnimalInfoById(animalInfo.id)
            executeClearData()
            postEvent(UpdateDatabaseEvent.Success)
        } catch(ex: Exception) {
            eventsChannel.send(UpdateDatabaseEvent.Error)
        } finally {
            isUpdatingDatabase.value = false
        }
    }

    private fun postEvent(event: Event) {
        viewModelScope.launch {
            internalEventsFlow.emit(event)
            eventsChannel.send(event)
        }
    }
}
