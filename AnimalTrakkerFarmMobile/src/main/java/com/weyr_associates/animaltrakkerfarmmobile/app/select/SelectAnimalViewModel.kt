package com.weyr_associates.animaltrakkerfarmmobile.app.select

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.awaitAll
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.IdTypeRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.SexStandard
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class SelectAnimalViewModel(
    private val databaseHandler: DatabaseHandler,
    private val sexStandard: SexStandard?,
    private val animalRepository: AnimalRepository,
    private val idTypesRepository: IdTypeRepository,
    private val loadDefaultSettings: LoadActiveDefaultSettings
) : ViewModel() {

    companion object {

        private val ID_TYPE_DEFAULT = IdType.ID_TYPE_ID_NAME

        private val ID_TYPE_PLACEHOLDER = IdType(
            id = ID_TYPE_DEFAULT,
            name = "",
            abbreviation = "",
            order = 0
        )
        //TODO: Discuss this value. Is it appropriate to make it larger?
        private const val MINIMUM_SEARCH_TERM_LENGTH = 1
    }

    data class ViewState(
        val isLoading: Boolean = false,
        val isScanning: Boolean = false,
        val searchTerm: String = "",
        val idType: IdType = ID_TYPE_PLACEHOLDER,
        val animalInfo: List<AnimalBasicInfo>? = null
    )

    private data class Defaults(
        val speciesId: Int,
        val eidIdType: IdType
    )

    private data class Search(
        val searchTerm: String,
        val idType: IdType,
        val speciesId: Int,
    )

    private data class SearchResults(
        val search: Search,
        val animalInfo: List<AnimalBasicInfo>?
    )

    private val defaultsFlow: Flow<Defaults> = flow { emit(loadDefaults()) }
        .shareIn(viewModelScope, SharingStarted.Lazily, replay = 1)

    private val searchTermFlow = MutableStateFlow("")
    private val searchIdTypeFlow = MutableStateFlow(ID_TYPE_PLACEHOLDER)

    private val searchFlow = combine(
        defaultsFlow,
        searchTermFlow.debounce(500L),
        searchIdTypeFlow
    ) { defaults, term, idType ->
        Search(term, idType, defaults.speciesId)
    }

    private val searchResultsFlow = searchFlow.mapLatest { search ->
        performSearch(search)
    }

    init {
        viewModelScope.launch {
            searchResultsFlow.collectLatest { searchResults ->
                updateViewStateWithSearchResults(searchResults)
            }
        }
    }

    private val viewStateMutableFlow = MutableStateFlow(ViewState(true))
    val viewStateFlow: StateFlow<ViewState> = viewStateMutableFlow

    fun updateSearchTerm(term: String) {
        searchTermFlow.value = term.trim()
    }

    fun updateSearchIdType(idType: IdType) {
        searchIdTypeFlow.value = idType
        viewStateMutableFlow.update { it.copy(idType = idType) }
    }

    override fun onCleared() {
        super.onCleared()
        databaseHandler.close()
    }

    private suspend fun loadDefaults(): Defaults {
        viewStateMutableFlow.update {
            it.copy(isLoading = true)
        }
        //TODO: Remove explicit dispatcher here once underlying repositories use suspend and are made main safe.
        val defaultSettingsAsync = viewModelScope.async(Dispatchers.IO) {
            loadDefaultSettings()
        }
        //TODO: Remove explicit dispatcher here once underlying repositories use suspend and are made main safe.
        val defaultIdTypeAsync = viewModelScope.async(Dispatchers.IO) {
            idTypesRepository.queryForIdType(ID_TYPE_DEFAULT)
        }
        val defaults = awaitAll(defaultSettingsAsync, defaultIdTypeAsync) { defaultSettings, defaultIdType ->
            Defaults(defaultSettings.speciesId, requireNotNull(defaultIdType))
        }
        viewStateMutableFlow.update {
            it.copy(isLoading = false, idType = defaults.eidIdType)
        }
        searchIdTypeFlow.update { defaults.eidIdType }

        return defaults
    }

    private suspend fun performSearch(search: Search): SearchResults {
        if (search.searchTerm.length < MINIMUM_SEARCH_TERM_LENGTH) {
            return SearchResults(search, null)
        }
        val results = when (search.idType.id) {
            IdType.ID_TYPE_ID_NAME -> animalRepository.searchAnimalsByName(
                partialName = search.searchTerm,
                speciesId = search.speciesId,
                sexStandard = sexStandard
            )
            else -> animalRepository.searchAnimalsByIdType(
                partialId = search.searchTerm,
                idTypeId = search.idType.id,
                speciesId = search.speciesId,
                sexStandard = sexStandard
            )
        }
        return SearchResults(search, results)
    }

    private fun updateViewStateWithSearchResults(searchResults: SearchResults) {
        viewStateMutableFlow.update {
            it.copy(animalInfo = searchResults.animalInfo)
        }
    }
}
