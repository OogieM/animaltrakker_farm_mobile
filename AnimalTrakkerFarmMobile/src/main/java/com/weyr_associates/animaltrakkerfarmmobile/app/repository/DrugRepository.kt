package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.Contact
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import java.time.LocalDate
import java.time.LocalDateTime

interface DrugRepository {
    suspend fun queryDrugTypes(): List<DrugType>
    suspend fun queryDrugsByType(drugTypeId: Int): List<Drug>

    suspend fun addDrug(
        drugTypeId: Int,
        officialDrugName: String,
        genericDrugName: String,
        drugLot: String,
        officialDrugDose: String,
        userDrugDose: String,
        expirationDate: LocalDate,
        isRemovable: Boolean,
        isOffLabel: Boolean,
        offLabelVetContactId: Int?,
        meatWithdrawal: Int?,
        userMeatWithdrawal: Int?,
        meatWithdrawalUnitsId: Int?,
        milkWithdrawal: Int?,
        userMilkWithdrawal: Int?,
        milkWithdrawalUnitsId: Int?,
        cost: Float?,
        currencyUnitsId: Int?,
        amountPurchased: String,
        purchaseDate: LocalDate?,
        timeStamp: LocalDateTime
    ): Long
}
