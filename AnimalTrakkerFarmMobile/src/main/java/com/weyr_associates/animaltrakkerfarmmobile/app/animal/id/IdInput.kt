package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

data class IdInput(
    val number: String?,
    val type: IdType?,
    val color: IdColor?,
    val location: IdLocation?
) {
    fun toEntry(id: Int = -1): IdEntry {
        return IdEntry(
            id = id,
            number = requireNotNull(
                number?.takeIf { it.isNotBlank() }?.trim()
            ),
            type = requireNotNull(type),
            color = requireNotNull(color),
            location = requireNotNull(location)
        )
    }

    fun toOptEntry(): IdEntry? = when {
            !number.isNullOrBlank() &&
                    type != null &&
                    color != null &&
                    location != null -> toEntry()
            else -> null
        }
}
