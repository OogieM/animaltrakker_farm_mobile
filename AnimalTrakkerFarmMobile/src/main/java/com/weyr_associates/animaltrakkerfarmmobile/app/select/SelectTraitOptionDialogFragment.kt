package com.weyr_associates.animaltrakkerfarmmobile.app.select

import android.content.Context
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.core.FragmentResultListenerRegistrar
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemDataSource
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemDelegateFactory
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemDisplayTextProvider
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.SelectItem
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.SelectItemDialogFragment
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.itemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.optionalItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.model.itemCallbackUsingOnlyIdentity
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTraitOption

class SelectTraitOptionDialogFragment : SelectItemDialogFragment<EvalTraitOption>(
    EvalTraitOption::class.java,
    R.string.select_title_trait_option,
    provideFilter = false
) {

    companion object {
        @JvmStatic
        fun newInstance(options: List<EvalTraitOption>, requestKey: String = REQUEST_KEY_SELECT_TRAIT_OPTION) =
            SelectTraitOptionDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(SelectItem.EXTRA_REQUEST_KEY, requestKey)
                    putParcelableArrayList(EXTRA_TRAIT_OPTIONS, ArrayList(options))
                }
            }

        const val REQUEST_KEY_SELECT_TRAIT_OPTION = "REQUEST_KEY_SELECT_TRAIT_OPTION"
        private const val EXTRA_TRAIT_OPTIONS = "EXTRA_TRAIT_OPTIONS"
    }

    private val traitOptions: List<EvalTraitOption> by lazy {
        requireNotNull(requireArguments().getParcelableArrayList<EvalTraitOption>(EXTRA_TRAIT_OPTIONS)).toList()
    }

    override fun createItemDelegateFactory(context: Context): ItemDelegateFactory<EvalTraitOption> {
        return Factory()
    }

    private inner class Factory : ItemDelegateFactory<EvalTraitOption> {
        override fun createDataSource(): ItemDataSource<EvalTraitOption> {
            return object : ItemDataSource<EvalTraitOption> {
                override suspend fun queryItems(filterText: String): List<EvalTraitOption> {
                   return traitOptions
                }
            }
        }

        override fun createItemDiffCallback(): DiffUtil.ItemCallback<EvalTraitOption> {
           return itemCallbackUsingOnlyIdentity()
        }

        override fun createDisplayTextProvider(): ItemDisplayTextProvider<EvalTraitOption> {
            return NameDisplayTextProvider
        }
    }
}

// region Launch Helpers

fun evalTraitOptionSelectionPresenter(
    registrar: FragmentResultListenerRegistrar,
    optionsProvider: () -> List<EvalTraitOption>,
    button: Button? = null,
    requestKey: String? = null,
    hintText: String? = null,
    itemDisplayTextProvider: ItemDisplayTextProvider<EvalTraitOption>? = null,
    onItemSelected: (EvalTraitOption) -> Unit
): ItemSelectionPresenter<EvalTraitOption> {
    val requestKeyActual = requestKey ?: SelectTraitOptionDialogFragment.REQUEST_KEY_SELECT_TRAIT_OPTION
    return itemSelectionPresenter(
        registrar,
        requestKeyActual,
        button,
        hintText,
        itemDisplayTextProvider.orNameAsDefault(),
        onItemSelected
    ) { SelectTraitOptionDialogFragment.newInstance(optionsProvider.invoke(), requestKeyActual) }
}

fun optionalEvalTraitOptionSelectionPresenter(
    registrar: FragmentResultListenerRegistrar,
    optionsProvider: () -> List<EvalTraitOption>,
    button: Button? = null,
    requestKey: String? = null,
    hintText: String? = null,
    itemDisplayTextProvider: ItemDisplayTextProvider<EvalTraitOption>? = null,
    onItemSelected: (EvalTraitOption?) -> Unit
): ItemSelectionPresenter<EvalTraitOption> {
    val requestKeyActual = requestKey ?: SelectTraitOptionDialogFragment.REQUEST_KEY_SELECT_TRAIT_OPTION
    return optionalItemSelectionPresenter(
        registrar,
        requestKeyActual,
        button,
        hintText,
        itemDisplayTextProvider.orNameAsDefault(),
        onItemSelected
    ) { SelectTraitOptionDialogFragment.newInstance(optionsProvider.invoke(), requestKeyActual) }
}

// endregion
