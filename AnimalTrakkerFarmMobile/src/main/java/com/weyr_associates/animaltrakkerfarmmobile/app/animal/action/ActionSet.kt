package com.weyr_associates.animaltrakkerfarmmobile.app.animal.action

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug.DrugAction
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.weight.WeightAction
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import java.util.UUID

data class ActionSet(
    val dewormers: List<DrugAction> = emptyList(),
    val vaccines: List<DrugAction> = emptyList(),
    val otherDrugs: List<DrugAction> = emptyList(),
    val weight: WeightAction
) {
    companion object {
        const val MAX_NUM_DEWORMERS = 4
        const val MAX_NUM_VACCINES = 10
    }

    val isConfigured: Boolean
        get() = dewormers.isNotEmpty() ||
                vaccines.isNotEmpty() ||
                otherDrugs.isNotEmpty()

    val canAddDewormers: Boolean
        get() = dewormers.size < MAX_NUM_DEWORMERS

    val canAddVaccines: Boolean
        get() = vaccines.size < MAX_NUM_VACCINES

    fun containsDrugAction(drugAction: DrugAction): Boolean {
        return drugActionsForDrug(drugAction.configuration.drug.typeId)
            .any { it.actionId == drugAction.actionId }
    }

    fun addDrugAction(configuration: DrugAction.Configuration): ActionSet {
        if (isDrugConfigured(configuration.drug)) {
            return this
        }
        return when {
            configuration.drug.typeId == DrugType.ID_DEWORMER && canAddDewormers -> {
                copy(dewormers = addDrugActionTo(configuration, dewormers))
            }
            configuration.drug.typeId == DrugType.ID_VACCINE && canAddVaccines -> {
                copy(vaccines = addDrugActionTo(configuration, vaccines))
            }
            else -> copy(otherDrugs = addDrugActionTo(configuration, otherDrugs))
        }
    }

    fun updateDrugAction(actionId: UUID, configuration: DrugAction.Configuration): ActionSet {
        return when {
            configuration.drug.typeId == DrugType.ID_VACCINE &&
                    vaccines.any { it.actionId == actionId } -> {
                copy(vaccines = updateDrugAction(vaccines, actionId, configuration))
            }
            configuration.drug.typeId == DrugType.ID_DEWORMER &&
                    dewormers.any { it.actionId == actionId } -> {
                copy(dewormers = updateDrugAction(dewormers, actionId, configuration))
            }
            otherDrugs.any { it.actionId == actionId } -> {
                copy(otherDrugs = updateDrugAction(otherDrugs, actionId, configuration))
            }
            else -> this
        }
    }

    fun removeDrugAction(drugAction: DrugAction): ActionSet {
        return when (drugAction.configuration.drug.typeId) {
            DrugType.ID_VACCINE -> copy(vaccines = removeDrugActionFrom(drugAction, vaccines))
            DrugType.ID_DEWORMER -> copy(dewormers = removeDrugActionFrom(drugAction, dewormers))
            else -> copy(otherDrugs = removeDrugActionFrom(drugAction, otherDrugs))
        }
    }

    fun markDrugActionComplete(drugAction: DrugAction, complete: Boolean): ActionSet {
        return when (drugAction.configuration.drug.typeId) {
            DrugType.ID_VACCINE -> copy(vaccines = markDrugActionCompleteIn(drugAction, complete, vaccines))
            DrugType.ID_DEWORMER -> copy(dewormers = markDrugActionCompleteIn(drugAction, complete, dewormers))
            else -> copy(otherDrugs = markDrugActionCompleteIn(drugAction, complete, otherDrugs))
        }
    }

    fun resetAllActions(): ActionSet {
        return copy(
            vaccines = vaccines.map { it.reset() },
            dewormers = dewormers.map { it.reset() },
            otherDrugs = otherDrugs.map { it.reset() },
            weight = weight.copy(weight = null)
        )
    }

    private fun isDrugConfigured(drug: Drug): Boolean {
        return drugActionsForDrug(drug.typeId)
            .any { it.configuration.drug.id == drug.id }
    }

    private fun addDrugActionTo(configuration: DrugAction.Configuration, drugActions: List<DrugAction>): List<DrugAction> {
        return drugActions.toMutableList().apply {
            add(DrugAction(configuration))
        }
    }

    private fun updateDrugAction(
        list: List<DrugAction>,
        actionId: UUID,
        configuration: DrugAction.Configuration
    ): List<DrugAction> {
        return list.toMutableList().also {
            it.replaceAll { drugAction ->
                if (drugAction.actionId != actionId) {
                    drugAction
                } else {
                    DrugAction(
                        actionId = actionId,
                        configuration = configuration,
                    )
                }
            }
        }
    }

    private fun removeDrugActionFrom(drugAction: DrugAction, drugActions: List<DrugAction>): List<DrugAction> {
        return drugActions.toMutableList().apply {
            removeIf { it.actionId == drugAction.actionId }
        }
    }

    private fun markDrugActionCompleteIn(drugAction: DrugAction, complete: Boolean, drugActions: List<DrugAction>): List<DrugAction> {
        val index = drugActions.indexOfFirst { it.actionId == drugAction.actionId }
        return if (-1 < index && drugActions[index].isComplete != complete) {
            drugActions.toMutableList().apply {
                this[index] = this[index].copy(isComplete = complete)
            }
        } else drugActions
    }

    private fun drugActionsForDrug(drugTypeId: Int): List<DrugAction> = when (drugTypeId) {
        DrugType.ID_VACCINE -> vaccines
        DrugType.ID_DEWORMER -> dewormers
        else -> otherDrugs
    }
}
