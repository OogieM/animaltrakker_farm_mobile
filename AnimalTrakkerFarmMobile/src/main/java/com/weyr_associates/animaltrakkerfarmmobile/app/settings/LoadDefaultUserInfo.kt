package com.weyr_associates.animaltrakkerfarmmobile.app.settings

import com.weyr_associates.animaltrakkerfarmmobile.model.UserType

data class UserInfo(
    val userId: Int,
    val userType: UserType
)

class LoadDefaultUserInfo(
    private val loadActiveDefaults: LoadActiveDefaultSettings
) {
    suspend operator fun invoke(): UserInfo {
        return loadActiveDefaults.invoke().let {
            val userId = it.userId
            val userType = it.userType
            if (userId != null && userType != null) {
                UserInfo(userId, userType)
            } else {
                UserInfo(0, UserType.CONTACT)
            }
        }
    }
}
