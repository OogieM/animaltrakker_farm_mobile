package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.simple.SimpleEvaluationActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuBirthingBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation

class BirthingMenuFragment : Fragment(R.layout.fragment_menu_birthing) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(FragmentMenuBirthingBinding.bind(view)) {
            btnSimpleLambing.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(
                        SimpleEvaluationActivity.newIntent(
                            requireContext(),
                            SavedEvaluation.ID_SIMPLE_LAMBING
                        )
                    )
                }
            }
            btnSimpleBirths.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(
                        SimpleEvaluationActivity.newIntent(
                            requireContext(),
                            SavedEvaluation.ID_SIMPLE_BIRTHS
                        )
                    )
                }
            }
            listOf(
                btnDetailedLambing,
                btnAddFemaleBirthingHistory
            ).forEach { it.deactivate() }
        }
    }
}