package com.weyr_associates.animaltrakkerfarmmobile.app.core.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.weyr_associates.animaltrakkerfarmmobile.R

class Divider @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.atrkkrDividerStyleHorizontal
) : View(context, attrs, defStyleAttr)
