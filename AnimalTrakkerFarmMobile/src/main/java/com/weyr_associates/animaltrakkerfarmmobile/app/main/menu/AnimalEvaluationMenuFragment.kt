package com.weyr_associates.animaltrakkerfarmmobile.app.main.menu

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.configuration.ConfigureEvaluationActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.optimalag.OptAgRamBreedingSoundnessActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.simple.SimpleEvaluationActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation.tissue.TissueSampleActivity
import com.weyr_associates.animaltrakkerfarmmobile.app.core.checkDatabaseIsPresentThen
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.selectedItem
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectEvaluationDialogFragment
import com.weyr_associates.animaltrakkerfarmmobile.databinding.FragmentMenuAnimalEvaluationBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry

class AnimalEvaluationMenuFragment : Fragment(R.layout.fragment_menu_animal_evaluation) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childFragmentManager.setFragmentResultListener(
            SelectEvaluationDialogFragment.REQUEST_KEY_SELECT_ANIMAL_EVALUTION, this
        ) { requestKey, result ->
            if (requestKey == SelectEvaluationDialogFragment.REQUEST_KEY_SELECT_ANIMAL_EVALUTION) {
                val selectEvalItem = result.selectedItem<ItemEntry>()
                startActivity(
                    SimpleEvaluationActivity.newIntent(
                        requireContext(),
                        selectEvalItem.id,
                        allowLoadEvaluation = true
                    )
                )
            }
        }
        with(FragmentMenuAnimalEvaluationBinding.bind(view)) {
            btnTakeTissueSample.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), TissueSampleActivity::class.java))
                }
            }
            btnOptimalAgRamBse.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireActivity(), OptAgRamBreedingSoundnessActivity::class.java))
                }
            }
            btnEvaluateAnimal.setOnClickListener {
                checkDatabaseIsPresentThen {
                    SelectEvaluationDialogFragment.newInstance()
                        .show(childFragmentManager, "TAG_FRAGMENT_SELECT_EVALUATION")
                }
            }
            btnCreateEvaluation.setOnClickListener {
                checkDatabaseIsPresentThen {
                    startActivity(Intent(requireContext(), ConfigureEvaluationActivity::class.java))
                }
            }
            listOf(
                btnMaleBreedingSoundness,
                btnOptimalAgEweUltrasound
            ).forEach { it.deactivate() }
        }
    }
}
