package com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs.AddDrugViewModel.Field
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.collectLatestOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.observeOneTimeEventsOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.DrugRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.select.SelectContactType
import com.weyr_associates.animaltrakkerfarmmobile.app.select.contactSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.dateSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.drugTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalUnitsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.unitsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivityAddDrugBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.Contact
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import kotlinx.coroutines.flow.map
import java.time.LocalDate

class AddDrugActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_KEY_CURRENCY_SELECTION = "REQUEST_KEY_CURRENCY_SELECTION"
        private const val REQUEST_KEY_PURCHASE_DATE_SELECTION = "REQUEST_KEY_PURCHASE_DATE_SELECTION"
        private const val REQUEST_KEY_EXPIRATION_DATE_SELECTION = "REQUEST_KEY_EXPIRATION_DATE_SELECTION"
        private const val REQUEST_KEY_MEAT_WITHDRAWAL_UNITS_SELECTION = "REQUEST_KEY_MEAT_WITHDRAWAL_UNITS_SELECTION"
        private const val REQUEST_KEY_MILK_WITHDRAWAL_UNITS_SELECTION = "REQUEST_KEY_MILK_WITHDRAWAL_UNITS_SELECTION"
    }

    private val binding by lazy {
        ActivityAddDrugBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<AddDrugViewModel> {
        ViewModelFactory(this)
    }

    private lateinit var drugTypeSelectionPresenter: ItemSelectionPresenter<DrugType>
    private lateinit var currencySelectionPresenter: ItemSelectionPresenter<UnitOfMeasure>
    private lateinit var purchaseDateSelectionPresenter: ItemSelectionPresenter<LocalDate>
    private lateinit var expirationDateSelectionPresenter: ItemSelectionPresenter<LocalDate>
    private lateinit var meatWithdrawalUnitsPresenter: ItemSelectionPresenter<UnitOfMeasure>
    private lateinit var milkWithdrawalUnitsPresenter: ItemSelectionPresenter<UnitOfMeasure>
    private lateinit var vetContactSelectionPresenter: ItemSelectionPresenter<Contact>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.buttonAdd.setOnClickListener {
            viewModel.save()
        }
        binding.inputOfficialName.addTextChangedListener {
            viewModel.officialDrugName = it.toString()
        }
        binding.inputGenericName.addTextChangedListener {
            viewModel.genericDrugName = it.toString()
        }
        binding.inputDrugLot.addTextChangedListener {
            viewModel.drugLot = it.toString()
        }
        binding.inputAmountPurchased.addTextChangedListener {
            viewModel.amountPurchased = it.toString()
        }
        binding.inputCost.addTextChangedListener {
            viewModel.drugCost = it.toString().toFloatOrNull()
        }
        binding.inputOfficialDose.addTextChangedListener {
            viewModel.officialDrugDose = it.toString()
        }
        binding.inputUserDose.addTextChangedListener {
            viewModel.userDrugDose = it.toString()
        }
        binding.inputMeatWithdrawal.addTextChangedListener {
            viewModel.meatWithdrawal = it.toString().toIntOrNull()
        }
        binding.inputUserMeatWithdrawal.addTextChangedListener {
            viewModel.userMeatWithdrawal = it.toString().toIntOrNull()
        }
        binding.inputMilkWithdrawal.addTextChangedListener {
            viewModel.milkWithdrawal = it.toString().toIntOrNull()
        }
        binding.inputUserMilkWithdrawal.addTextChangedListener {
            viewModel.userMilkWithdrawal = it.toString().toIntOrNull()
        }
        binding.switchIsRemovable.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setIsRemovable(isChecked)
        }
        binding.switchIsOffLabel.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setIsOffLabel(isChecked)
        }
        drugTypeSelectionPresenter = drugTypeSelectionPresenter(
            button = binding.spinnerDrugTypeSelection
        ) { drugType ->
            viewModel.selectDrugType(drugType)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedDrugType)
        }
        currencySelectionPresenter = unitsSelectionPresenter(
            button = binding.spinnerCurrencySelection,
            requestKey = REQUEST_KEY_CURRENCY_SELECTION,
            unitsTypeId = UnitOfMeasure.Type.ID_CURRENCY
        ) { currency ->
            viewModel.selectCurrency(currency)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedCurrency)
        }
        purchaseDateSelectionPresenter = dateSelectionPresenter(
            button = binding.spinnerPurchaseDate,
            requestKey = REQUEST_KEY_PURCHASE_DATE_SELECTION
        ) { purchaseDate ->
            viewModel.selectPurchaseDate(purchaseDate)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedPurchaseDate)
        }
        expirationDateSelectionPresenter = dateSelectionPresenter(
            button = binding.spinnerExpirationDate,
            requestKey = REQUEST_KEY_EXPIRATION_DATE_SELECTION
        ) { expirationDate ->
            viewModel.selectExpirationDate(expirationDate)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedExpirationDate)
        }
        meatWithdrawalUnitsPresenter = optionalUnitsSelectionPresenter(
            button = binding.spinnerMeatWithdrawalUnitsSelection,
            requestKey = REQUEST_KEY_MEAT_WITHDRAWAL_UNITS_SELECTION,
            unitsTypeId = UnitOfMeasure.Type.ID_TIME
        ) { units ->
            viewModel.selectMeatWithdrawalUnits(units)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedMeatWithdrawalUnits)
        }
        milkWithdrawalUnitsPresenter = optionalUnitsSelectionPresenter(
            button = binding.spinnerMilkWithdrawalUnitsSelection,
            requestKey = REQUEST_KEY_MILK_WITHDRAWAL_UNITS_SELECTION,
            unitsTypeId = UnitOfMeasure.Type.ID_TIME
        ) { units ->
            viewModel.selectMilkWithdrawalUnits(units)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedMilkWithdrawalUnits)
        }
        vetContactSelectionPresenter = contactSelectionPresenter(
            contactType = SelectContactType.VETERINARIANS,
            button = binding.spinnerVetContactSelection
        ) { contact ->
            viewModel.selectVetContact(contact)
        }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedOffLabelVetContact)
        }
        collectLatestOnStart(viewModel.canSave) { canSave ->
            binding.buttonAdd.isEnabled = canSave
        }
        collectLatestOnStart(viewModel.isDrugRemovable) { isRemovable ->
            binding.switchIsRemovable.isChecked = isRemovable
        }
        collectLatestOnStart(viewModel.isDrugOffLabel) { isOffLabel ->
            binding.switchIsOffLabel.isChecked = isOffLabel
            binding.spinnerVetContactSelection.isEnabled = isOffLabel
        }
        collectLatestOnStart(viewModel.selectedMeatWithdrawalUnits.map { it != null }) { expectsWithdrawal ->
            binding.inputMeatWithdrawal.isEnabled = expectsWithdrawal
            binding.inputUserMeatWithdrawal.isEnabled = expectsWithdrawal
        }
        collectLatestOnStart(viewModel.selectedMilkWithdrawalUnits.map { it != null }) { expectsWithdrawal ->
            binding.inputMilkWithdrawal.isEnabled = expectsWithdrawal
            binding.inputUserMilkWithdrawal.isEnabled = expectsWithdrawal
        }
        observeOneTimeEventsOnStart(viewModel.events, ::handleEvent)
    }

    private fun handleEvent(event: AddDrugViewModel.Event) {
        when (event) {
            is AddDrugViewModel.FieldValueChanged -> handleFieldValueChanged(event)
            AddDrugViewModel.SaveSucceededEvent -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_drug_succeeded,
                    Toast.LENGTH_SHORT
                ).show()
                binding.containerContentScroll.fullScroll(View.FOCUS_UP)
            }
            AddDrugViewModel.SaveFailedEvent -> {
                Toast.makeText(
                    this,
                    R.string.toast_add_drug_failed,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun handleFieldValueChanged(event: AddDrugViewModel.FieldValueChanged) {
        if (event.isAffected(Field.OFFICIAL_NAME)) {
            binding.inputOfficialName.setText(viewModel.officialDrugName)
        }
        if (event.isAffected(Field.GENERIC_NAME)) {
            binding.inputGenericName.setText(viewModel.genericDrugName)
        }
        if (event.isAffected(Field.LOT)) {
            binding.inputDrugLot.setText(viewModel.drugLot)
        }
        if (event.isAffected(Field.COST)) {
            binding.inputCost.setText(viewModel.drugCost?.toString() ?: "")
        }
        if (event.isAffected(Field.AMOUNT_PURCHASED)) {
            binding.inputAmountPurchased.setText(viewModel.amountPurchased)
        }
        if (event.isAffected(Field.OFFICIAL_DOSE)) {
            binding.inputOfficialDose.setText(viewModel.officialDrugDose)
        }
        if (event.isAffected(Field.USER_DOSE)) {
            binding.inputUserDose.setText(viewModel.userDrugDose)
        }
        if (event.isAffected(Field.MEAT_WITHDRAWAL)) {
            binding.inputMeatWithdrawal.setText(
                viewModel.meatWithdrawal?.toString() ?: ""
            )
        }
        if (event.isAffected(Field.USER_MEAT_WITHDRAWAL)) {
            binding.inputUserMeatWithdrawal.setText(
                viewModel.userMeatWithdrawal?.toString() ?: ""
            )
        }
        if (event.isAffected(Field.MILK_WITHDRAWAL)) {
            binding.inputMilkWithdrawal.setText(
                viewModel.milkWithdrawal?.toString() ?: ""
            )
        }
        if (event.isAffected(Field.USER_MILK_WITHDRAWAL)) {
            binding.inputUserMilkWithdrawal.setText(
                viewModel.userMilkWithdrawal?.toString() ?: ""
            )
        }
    }

    private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

        private val appContext = context.applicationContext

        override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
            val drugRepository = DrugRepositoryImpl(DatabaseHandler.create(appContext))
            @Suppress("UNCHECKED_CAST")
            return AddDrugViewModel(drugRepository) as T
        }
    }
}
