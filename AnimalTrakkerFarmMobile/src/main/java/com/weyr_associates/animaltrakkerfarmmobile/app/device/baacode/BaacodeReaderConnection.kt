package com.weyr_associates.animaltrakkerfarmmobile.app.device.baacode

import android.content.Context
import android.os.Message
import androidx.lifecycle.Lifecycle
import com.weyr_associates.animaltrakkerfarmmobile.app.device.DeviceServiceConnection

class BaacodeReaderConnection(
    context: Context, lifecycle: Lifecycle
) : DeviceServiceConnection(context, lifecycle, BaacodeReaderService::class.java) {
    override val isDeviceServiceRunning: Boolean
        get() = BaacodeReaderService.isBaaRunning

    override fun onMessageReceived(message: Message) {

    }
}
