package com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup

import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo.AnimalInfoState
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.lookup.LookupAnimalInfo.Lookup
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.AnimalRepository
import com.weyr_associates.animaltrakkerfarmmobile.model.AnimalBasicInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger

class AnimalInfoLookup(
    private val coroutineScope: CoroutineScope,
    private val animalRepo: AnimalRepository
) : LookupAnimalInfo {

    private val loadOccurrence: AtomicInteger = AtomicInteger(0)

    private data object ResetAnimalInfo

    private val onLoadAnimalInfoChannel = Channel<AnimalBasicInfo>()
    private val onLookupAnimalChannel = Channel<Lookup>()
    private val onResetAnimalChannel = Channel<ResetAnimalInfo>()

    override val animalInfoState: StateFlow<AnimalInfoState> = merge(
        onLoadAnimalInfoChannel.receiveAsFlow(),
        onLookupAnimalChannel.receiveAsFlow(),
        onResetAnimalChannel.receiveAsFlow()
    ).mapLatest { event ->
        when (event) {
            is AnimalBasicInfo -> {
                val animalBasicInfo: AnimalBasicInfo = event
                AnimalInfoState.Loaded(
                    animalBasicInfo,
                    null,
                    loadOccurrence.incrementAndGet()
                )
            }
            is Lookup -> {
                val lookup: Lookup = event
                val (animalInfo, scannedEID) = when (lookup) {
                    is Lookup.ByAnimalId -> {
                        Pair(animalRepo.queryAnimalBasicInfoByAnimalId(lookup.animalId), null)
                    }

                    is Lookup.ByScannedEID -> {
                        Pair(animalRepo.queryAnimalBasicInfoByEID(lookup.eidNumber), lookup.eidNumber)
                    }
                }
                animalInfo?.let {
                    AnimalInfoState.Loaded(
                        it,
                        scannedEID,
                        loadOccurrence.incrementAndGet()
                    )
                } ?: AnimalInfoState.NotFound(lookup)
            }
            else -> AnimalInfoState.Initial
        }
    }.stateIn(coroutineScope, SharingStarted.Lazily, AnimalInfoState.Initial)

    fun loadAnimalInfo(animalBasicInfo: AnimalBasicInfo) {
        coroutineScope.launch {
            onLoadAnimalInfoChannel.send(animalBasicInfo)
        }
    }

    override fun lookupAnimalInfoById(animalId: Int) {
        coroutineScope.launch {
            onLookupAnimalChannel.send(Lookup.ByAnimalId(animalId))
        }
    }

    override fun lookupAnimalInfoByEIDNumber(eidNumber: String) {
        coroutineScope.launch {
            onLookupAnimalChannel.send(Lookup.ByScannedEID(eidNumber))
        }
    }

    override fun resetAnimalInfo() {
        coroutineScope.launch {
            onResetAnimalChannel.send(ResetAnimalInfo)
        }
    }
}
