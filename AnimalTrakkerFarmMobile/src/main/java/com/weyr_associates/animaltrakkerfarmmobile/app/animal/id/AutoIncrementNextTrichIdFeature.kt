package com.weyr_associates.animaltrakkerfarmmobile.app.animal.id

import com.weyr_associates.animaltrakkerfarmmobile.app.repository.DefaultSettingsRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.DefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

class AutoIncrementNextTrichIdFeature(
    private val loadActiveDefaultSettings: LoadActiveDefaultSettings,
    private val defaultSettingsRepository: DefaultSettingsRepository
) {

    var shouldAutoIncrementTrichNumber = false
        private set
    var nextTrichNumber = 0
        private set

    private var activeDefaultSettingsId = 0

    suspend fun configureFromSettings() {
        configureFromSettings(loadActiveDefaultSettings())
    }

    fun configureFromSettings(defaults: DefaultSettings) {
        activeDefaultSettingsId = defaults.id
        shouldAutoIncrementTrichNumber = defaults.trichIdAutoIncrement
        nextTrichNumber = defaults.trichNextIdNumber
    }

    fun shouldAutoPopulateTrichNumber(idType: IdType): Boolean {
        return idType.id == IdType.ID_TYPE_ID_TRICH &&
                shouldAutoIncrementTrichNumber
    }

    suspend fun autoIncrementIfRequired(idEntry: IdEntry) {
        if (idEntry.type.id == IdType.ID_TYPE_ID_TRICH &&
            shouldAutoIncrementTrichNumber) {
            val updatedTrichNumber = idEntry.number.toInt() + 1
            defaultSettingsRepository.updateNextTrichIdNumber(
                activeDefaultSettingsId,
                updatedTrichNumber
            )
            nextTrichNumber = updatedTrichNumber
        }
    }
}
