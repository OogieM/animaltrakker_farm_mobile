package com.weyr_associates.animaltrakkerfarmmobile.app.animal.add

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.preference.PreferenceManager
import com.weyr_associates.animaltrakkerfarmmobile.R
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.Event
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.InputEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.UpdateDatabaseEvent
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.add.SimpleAddAnimalViewModel.ValidationError
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.AutoIncrementNextTrichIdFeature
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdInputSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdValidationErrorDialog
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.id.IdValidations
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.collectLatestOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.android.lifecycle.observeOneTimeEventsOnStart
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.observeOneTimeEvents
import com.weyr_associates.animaltrakkerfarmmobile.app.core.select.ItemSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.core.widget.TopButtonBar
import com.weyr_associates.animaltrakkerfarmmobile.app.device.eid.EIDReaderConnection
import com.weyr_associates.animaltrakkerfarmmobile.app.permissions.RequiredPermissionsWatcher
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.AnimalRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.BreedRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.DefaultSettingsRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.IdColorRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.IdLocationRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.IdTypeRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.OwnerRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.PremiseRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.database.SexRepositoryImpl
import com.weyr_associates.animaltrakkerfarmmobile.app.select.AbbreviationDisplayTextProvider
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageMonthsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ageYearsSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.breedSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.idTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdColorSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdLocationSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.optionalIdTypeSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.ownerSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.select.sexSelectionPresenter
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.LoadActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.databinding.ActivitySimpleAddAnimalBinding
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed
import com.weyr_associates.animaltrakkerfarmmobile.model.IdColor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdLocation
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.model.Sex
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class SimpleAddAnimalActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, SimpleAddAnimalActivity::class.java)

        fun newIntentToAddAndSelect(
            context: Context,
            primaryIdType: Int,
            primaryIdNumber: String
        ): Intent {
            return Intent(context, SimpleAddAnimalActivity::class.java).apply {
                action = AddAnimal.ACTION_ADD_AND_SELECT
                putExtra(AddAnimal.EXTRA_PRIMARY_ID_TYPE_ID, primaryIdType)
                putExtra(AddAnimal.EXTRA_PRIMARY_ID_NUMBER, primaryIdNumber)
            }
        }

        fun startToAddAndSelect(
            activity: Activity,
            primaryIdType: Int,
            primaryIdNumber: String,
            requestCode: Int
        ) {
            activity.startActivityForResult(
                newIntentToAddAndSelect(
                    activity,
                    primaryIdType,
                    primaryIdNumber
                ),
                requestCode
            )
        }

        private const val REQUEST_SELECT_ID_TYPE_1 = "REQUEST_SELECT_TAG_TYPE_1"
        private const val REQUEST_SELECT_ID_TYPE_2 = "REQUEST_SELECT_TAG_TYPE_2"
        private const val REQUEST_SELECT_ID_TYPE_3 = "REQUEST_SELECT_TAG_TYPE_3"
        private const val REQUEST_SELECT_ID_LOCATION_1 = "REQUEST_SELECT_ID_LOCATION_1"
        private const val REQUEST_SELECT_ID_LOCATION_2 = "REQUEST_SELECT_ID_LOCATION_2"
        private const val REQUEST_SELECT_ID_LOCATION_3 = "REQUEST_SELECT_ID_LOCATION_3"
        private const val REQUEST_SELECT_ID_COLOR_1 = "REQUEST_SELECT_ID_COLOR_1"
        private const val REQUEST_SELECT_ID_COLOR_2 = "REQUEST_SELECT_ID_COLOR_2"
        private const val REQUEST_SELECT_ID_COLOR_3 = "REQUEST_SELECT_ID_COLOR_3"
    }

    private val viewModel: SimpleAddAnimalViewModel
            by viewModels<SimpleAddAnimalViewModel> {
                ViewModelFactory(this)
            }

    private lateinit var binding: ActivitySimpleAddAnimalBinding

    private lateinit var breedPresenter: ItemSelectionPresenter<Breed>
    private lateinit var ageYearsPresenter: ItemSelectionPresenter<Int>
    private lateinit var ageMonthsPresenter: ItemSelectionPresenter<Int>
    private lateinit var sexPresenter: ItemSelectionPresenter<Sex>
    private lateinit var ownerPresenter: ItemSelectionPresenter<Owner>

    private lateinit var idType1Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor1Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation1Presenter: ItemSelectionPresenter<IdLocation>

    private lateinit var idType2Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor2Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation2Presenter: ItemSelectionPresenter<IdLocation>

    private lateinit var idType3Presenter: ItemSelectionPresenter<IdType>
    private lateinit var idColor3Presenter: ItemSelectionPresenter<IdColor>
    private lateinit var idLocation3Presenter: ItemSelectionPresenter<IdLocation>

    private val requiredPermissionsWatcher = RequiredPermissionsWatcher(this)

    private var breedCollectionJob: Job? = null
    private var sexCollectionJob: Job? = null

    private lateinit var eidReaderConnection: EIDReaderConnection

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimpleAddAnimalBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())
        setupTopButtonBar()
        setupInputFields()
        setupBreedAndSexSpinner()
        setupAgeSpinners()
        setupOwnerSpinner()
        setupIdTypeSpinners()
        setupTagLocationSpinners()
        setupTagColorSpinners()
        bindToEvents()

        eidReaderConnection = EIDReaderConnection(this, lifecycle)
            .also { lifecycle.addObserver(it) }

        collectLatestOnStart(eidReaderConnection.deviceConnectionState) { connectionState ->
            binding.buttonPanelTop.updateEIDReaderConnectionState(connectionState)
        }
        collectLatestOnStart(eidReaderConnection.isScanningForEID) { isScanning ->
            binding.buttonPanelTop.showScanningEID = isScanning
        }
        observeOneTimeEventsOnStart(eidReaderConnection.onEIDScanned) { eidString ->
            onEIDScanned(eidString)
        }

        lifecycle.addObserver(requiredPermissionsWatcher)
    }

    private fun setupTopButtonBar() {
        binding.buttonPanelTop.show(
            TopButtonBar.UI_SCANNER_STATUS or
                    TopButtonBar.UI_SCAN_EID or
                    TopButtonBar.UI_CLEAR_DATA or
                    TopButtonBar.UI_ACTION_UPDATE_DATABASE
        )
        binding.buttonPanelTop.scanEIDButton.setOnClickListener {
            eidReaderConnection.toggleScanningEID()
        }
        binding.buttonPanelTop.clearDataButton.setOnClickListener {
            viewModel.clearData()
        }
        binding.buttonPanelTop.mainActionButton.setOnClickListener {
            viewModel.saveToDatabase()
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canClearData.collectLatest { canClear ->
                    binding.buttonPanelTop.clearDataButton.isEnabled = canClear
                }
            }
        }
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.canSaveToDatabase.collectLatest { canSave ->
                    binding.buttonPanelTop.mainActionButton.isEnabled = canSave
                }
            }
        }
    }

    private fun setupInputFields() {
        with(binding.inputAnimalName) {
            setText(viewModel.animalName)
            addTextChangedListener {
                viewModel.animalName = it.toString()
            }
        }

        with(binding.inputAnimalId1.inputIdNumber) {
            setText(viewModel.idNumber1)
            addTextChangedListener {
                viewModel.idNumber1 = it.toString()
            }
        }

        with(binding.inputAnimalId2.inputIdNumber) {
            setText(viewModel.idNumber2)
            addTextChangedListener {
                viewModel.idNumber2 = it.toString()
            }
        }

        with(binding.inputAnimalId3.inputIdNumber) {
            setText(viewModel.idNumber3)
            addTextChangedListener {
                viewModel.idNumber3 = it.toString()
            }
        }
    }

    private fun setupBreedAndSexSpinner() {
        //Reassign breed and sex presenters
        //whenever the species ID changes
        //so proper breed and sex options are available.
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.speciesId.collect { speciesId ->
                    breedCollectionJob?.cancel()
                    breedPresenter = breedSelectionPresenter(
                        speciesId = speciesId,
                        button = binding.animalBreedSpinner,
                        hintText = getString(R.string.hint_select_breed)
                    ) { breed -> viewModel.selectBreed(breed) }.also {
                        breedCollectionJob = it.bindToFlow(
                            this@SimpleAddAnimalActivity,
                            lifecycleScope,
                            viewModel.selectedBreed
                        )
                    }
                    sexCollectionJob?.cancel()
                    sexPresenter = sexSelectionPresenter(
                        speciesId = speciesId,
                        button = binding.animalSexSpinner,
                        hintText = getString(R.string.hint_select_sex)
                    ) { sex -> viewModel.selectSex(sex) }.also {
                        sexCollectionJob = it.bindToFlow(
                            this@SimpleAddAnimalActivity,
                            lifecycleScope,
                            viewModel.selectedSex
                        )
                    }
                }
            }
        }
    }

    private fun setupAgeSpinners() {
        ageYearsPresenter = ageYearsSelectionPresenter(
            button = binding.ageYearSpinner,
            hintText = getString(R.string.hint_select_age_years)
        ) { ageYears -> viewModel.selectAgeYears(ageYears) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedAgeYears)
        }

        ageMonthsPresenter = ageMonthsSelectionPresenter(
            button = binding.ageMonthSpinner,
            hintText = getString(R.string.hint_select_age_months)
        ) { ageMonths -> viewModel.selectAgeMonths(ageMonths) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedAgeMonths)
        }
    }

    private fun setupOwnerSpinner() {
        ownerPresenter = ownerSelectionPresenter(
            button = binding.ownerNameSpinner,
            hintText = getString(R.string.hint_select_owner)
        ) { owner -> viewModel.selectOwner(owner) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedOwner)
        }
    }

    private fun setupIdTypeSpinners() {
        idType1Presenter = idTypeSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_1
        ) { idType -> viewModel.selectIdType1(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType1)
        }
        collectLatestOnStart(viewModel.selectedIdType1) { idType ->
            with(binding.inputAnimalId1.inputIdNumber) {
                isEnabled = idType != null
                IdInputSettings.applyTo(this, idType?.id)
            }
        }
        idType2Presenter = optionalIdTypeSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_2
        ) { idType -> viewModel.selectIdType2(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType2)
        }
        collectLatestOnStart(viewModel.selectedIdType2) { idType ->
            with(binding.inputAnimalId2.inputIdNumber) {
                isEnabled = idType != null
                IdInputSettings.applyTo(this, idType?.id)
            }
        }
        idType3Presenter = optionalIdTypeSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdType,
            requestKey = REQUEST_SELECT_ID_TYPE_3
        ) { idType -> viewModel.selectIdType3(idType) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdType3)
        }
        collectLatestOnStart(viewModel.selectedIdType3) { idType ->
            with(binding.inputAnimalId3.inputIdNumber) {
                isEnabled = idType != null
                IdInputSettings.applyTo(this, idType?.id)
            }
        }
    }

    private fun setupTagLocationSpinners() {
        idLocation1Presenter = idLocationSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_1,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation1(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation1)
        }
        idLocation2Presenter = optionalIdLocationSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_2,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation2(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation2)
        }
        idLocation3Presenter = optionalIdLocationSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdLocation,
            requestKey = REQUEST_SELECT_ID_LOCATION_3,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idLocation -> viewModel.selectIdLocation3(idLocation) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdLocation3)
        }
    }

    private fun setupTagColorSpinners() {
        idColor1Presenter = idColorSelectionPresenter(
            button = binding.inputAnimalId1.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_1,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor1(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor1)
        }
        idColor2Presenter = optionalIdColorSelectionPresenter(
            button = binding.inputAnimalId2.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_2,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor2(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor2)
        }
        idColor3Presenter = optionalIdColorSelectionPresenter(
            button = binding.inputAnimalId3.spinnerIdColor,
            requestKey = REQUEST_SELECT_ID_COLOR_3,
            itemDisplayTextProvider = AbbreviationDisplayTextProvider
        ) { idColor -> viewModel.selectIdColor3(idColor) }.also {
            it.bindToFlow(this, lifecycleScope, viewModel.selectedIdColor3)
        }
    }

    private fun bindToEvents() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.events.observeOneTimeEvents {
                    handleEvent(it)
                }
            }
        }
    }

    // use EID reader to look up an animal
    private fun onEIDScanned(scannedEID: String) {
        viewModel.onEIDScanned(scannedEID)
        Log.i("in onEIDScanned ", "with EID of $scannedEID")
    }

    private fun handleEvent(event: Event) {
        when (event) {
            is InputEvent -> handleInputEvent(event)
            is ValidationError -> handleValidationError(event)
            is UpdateDatabaseEvent -> handleUpdateDatabaseEvent(event)
        }
    }

    private fun handleInputEvent(inputEvent: InputEvent) {
        when (inputEvent) {
            InputEvent.AnimalNameChanged -> {
                binding.inputAnimalName.setText(viewModel.animalName)
            }

            InputEvent.IdNumber1Changed -> {
                binding.inputAnimalId1.inputIdNumber.setText(viewModel.idNumber1)
            }

            InputEvent.IdNumber2Changed -> {
                binding.inputAnimalId2.inputIdNumber.setText(viewModel.idNumber2)
            }

            InputEvent.IdNumber3Changed -> {
                binding.inputAnimalId3.inputIdNumber.setText(viewModel.idNumber3)
            }
        }
    }

    private fun handleValidationError(validationError: ValidationError) {
        when (validationError) {
            is ValidationError.ScannedEIDAlreadyUsed -> {
                IdValidationErrorDialog.showEIDAlreadyInUseError(
                    this,
                    validationError.error
                )
            }

            ValidationError.IncompleteAnimalEntry -> {
                showIncompleteAnimalEntryError()
            }

            ValidationError.IdEntryRequired -> {
                IdValidationErrorDialog.showIdEntryIsRequiredError(this)
            }

            ValidationError.PartialIdEntry -> {
                IdValidationErrorDialog.showPartialIdEntryError(this)
            }

            is ValidationError.InvalidIdNumberFormat -> {
                IdValidationErrorDialog.showIdNumberFormatError(this, validationError.idEntry)
            }

            is ValidationError.InvalidIdCombination -> {
                IdValidationErrorDialog.showIdCombinationError(this, validationError.error)
            }
        }
    }

    private fun handleUpdateDatabaseEvent(event: UpdateDatabaseEvent) {
        when (event) {
            is UpdateDatabaseEvent.Success -> {
                if (intent?.action == AddAnimal.ACTION_ADD_AND_SELECT) {
                    //TODO: Pass this animal id as a long and not an int once IDs are sorted out as longs.
                    setResult(RESULT_OK, Intent().apply {
                        putExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_ID, event.animalId.toInt())
                        putExtra(AddAnimal.EXTRA_RESULTING_ANIMAL_NAME, event.animalName)
                    })
                    finish()
                } else {
                    //TODO: Discuss what, if anything else, to show here.
                    Toast.makeText(
                        this,
                        getString(R.string.toast_add_animal_success, event.animalName),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            is UpdateDatabaseEvent.Error -> {
                //TODO: Discuss what, if anything else, to show here.
                Toast.makeText(
                    this,
                    getString(R.string.toast_add_animal_failure, event.animalName),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun showIncompleteAnimalEntryError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title_incomplete_animal_entry)
            .setMessage(R.string.dialog_message_incomplete_animal_entry)
            .setPositiveButton(R.string.ok) { _, _ -> /*NO-OP*/ }
            .create()
            .show()
    }
}

private class ViewModelFactory(context: Context) : ViewModelProvider.Factory {

    private val appContext = context.applicationContext

    override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
        when(modelClass) {
            SimpleAddAnimalViewModel::class.java -> {
                val dbh = DatabaseHandler.create(appContext)
                val activeDefSettings = ActiveDefaultSettings(
                    PreferenceManager.getDefaultSharedPreferences(appContext)
                )
                val defSettingsRepo = DefaultSettingsRepositoryImpl(dbh)
                val loadActiveDefaultSettings = LoadActiveDefaultSettings(
                    activeDefaultSettings = activeDefSettings,
                    defSettingsRepo
                )
                val animalRepo = AnimalRepositoryImpl(dbh)
                @Suppress("UNCHECKED_CAST")
                return SimpleAddAnimalViewModel(
                    savedStateHandle = extras.createSavedStateHandle(),
                    databaseHandler = dbh,
                    loadActiveDefaultSettings = loadActiveDefaultSettings,
                    animalRepo = animalRepo,
                    breedRepo = BreedRepositoryImpl(dbh),
                    sexRepo = SexRepositoryImpl(dbh),
                    ownerRepo = OwnerRepositoryImpl(dbh),
                    premiseRepo = PremiseRepositoryImpl(dbh),
                    idTypeRepo = IdTypeRepositoryImpl(dbh),
                    idColorRepo = IdColorRepositoryImpl(dbh),
                    idLocationRepo = IdLocationRepositoryImpl(dbh),
                    idValidations = IdValidations(animalRepo),
                    autoUpdateTrichId = AutoIncrementNextTrichIdFeature(
                        loadActiveDefaultSettings = loadActiveDefaultSettings,
                        defaultSettingsRepository = defSettingsRepo
                    )
                ) as T
            }
            else -> {
                throw IllegalStateException("${modelClass.simpleName} is not supported.")
            }
        }
    }
}
