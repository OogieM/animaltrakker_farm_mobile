package com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.drug

import android.os.Parcelable
import com.weyr_associates.animaltrakkerfarmmobile.app.animal.action.AnimalAction
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugLocation
import kotlinx.parcelize.Parcelize
import java.util.UUID

@Parcelize
data class DrugAction(
    val configuration: Configuration,
    override val actionId: UUID = UUID.randomUUID(),
    override val isComplete: Boolean = configuration.autoComplete
) : AnimalAction, Parcelable {
    @Parcelize
    data class Configuration(
        val drug: Drug,
        val location: DrugLocation,
        val autoComplete: Boolean = true
    ) : Parcelable

    fun reset(): DrugAction {
        return copy(isComplete = configuration.autoComplete)
    }
}
