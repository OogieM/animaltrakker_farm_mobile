package com.weyr_associates.animaltrakkerfarmmobile.app.repository

import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

interface IdTypeRepository {
    fun queryIdTypes(): List<IdType>
    fun queryIdTypesForSearch(): List<IdType>
    fun queryForIdType(id: Int): IdType?
}
