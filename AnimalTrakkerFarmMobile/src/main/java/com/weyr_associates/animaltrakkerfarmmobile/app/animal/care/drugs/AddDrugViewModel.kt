package com.weyr_associates.animaltrakkerfarmmobile.app.animal.care.drugs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weyr_associates.animaltrakkerfarmmobile.app.core.kt.coroutines.flow.combine19
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.DrugRepository
import com.weyr_associates.animaltrakkerfarmmobile.model.Contact
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

class AddDrugViewModel(
    private val drugRepository: DrugRepository
) : ViewModel() {

    enum class Field {
        OFFICIAL_NAME,
        GENERIC_NAME,
        OFFICIAL_DOSE,
        USER_DOSE,
        LOT,
        COST,
        AMOUNT_PURCHASED,
        MEAT_WITHDRAWAL,
        USER_MEAT_WITHDRAWAL,
        MILK_WITHDRAWAL,
        USER_MILK_WITHDRAWAL
    }

    sealed interface Event

    data class FieldValueChanged(
        private val affectedFields: Set<Field>
    ) : Event {
        constructor(vararg affectedFields: Field)
                : this(affectedFields.toSet())
        fun isAffected(field: Field): Boolean {
            return affectedFields.isEmpty() || affectedFields.contains(field)
        }
    }

    data object SaveSucceededEvent : Event
    data object SaveFailedEvent : Event

    private val _selectedDrugType = MutableStateFlow<DrugType?>(null)
    val selectedDrugType = _selectedDrugType.asStateFlow()

    private val _isDrugRemovable = MutableStateFlow(false)
    val isDrugRemovable = _isDrugRemovable.asStateFlow()

    private val _officialDrugName = MutableStateFlow("")
    var officialDrugName: String
        get() = _officialDrugName.value
        set(value) { _officialDrugName.update { value } }

    private val _genericDrugName = MutableStateFlow("")
    var genericDrugName: String
        get() = _genericDrugName.value
        set(value) { _genericDrugName.update { value } }

    private val _drugLot = MutableStateFlow("")
    var drugLot: String
        get() = _drugLot.value
        set(value) { _drugLot.update { value } }

    private val _drugCost = MutableStateFlow<Float?>(null)
    var drugCost: Float?
        get() = _drugCost.value
        set(value) { _drugCost.update { value } }

    private val _selectedCurrency = MutableStateFlow<UnitOfMeasure?>(null)
    val selectedCurrency = _selectedCurrency.asStateFlow()

    private val _amountPurchased = MutableStateFlow("")
    var amountPurchased: String
        get() = _amountPurchased.value
        set(value) { _amountPurchased.update { value } }

    private val _selectedPurchaseDate = MutableStateFlow<LocalDate?>(null)
    val selectedPurchaseDate = _selectedPurchaseDate.asStateFlow()

    private val _selectedExpirationDate = MutableStateFlow<LocalDate?>(null)
    val selectedExpirationDate = _selectedExpirationDate.asStateFlow()

    private val _meatWithdrawal = MutableStateFlow<Int?>(null)
    var meatWithdrawal: Int?
        get() = _meatWithdrawal.value
        set(value) { _meatWithdrawal.update { value } }

    private val _userMeatWithdrawal = MutableStateFlow<Int?>(null)
    var userMeatWithdrawal: Int?
        get() = _userMeatWithdrawal.value
        set(value) { _userMeatWithdrawal.update { value } }

    private val _selectedMeatWithdrawalUnits = MutableStateFlow<UnitOfMeasure?>(null)
    val selectedMeatWithdrawalUnits = _selectedMeatWithdrawalUnits.asStateFlow()

    private val _milkWithdrawal = MutableStateFlow<Int?>(null)
    var milkWithdrawal: Int?
        get() = _milkWithdrawal.value
        set(value) { _milkWithdrawal.update { value } }

    private val _userMilkWithdrawal = MutableStateFlow<Int?>(null)
    var userMilkWithdrawal: Int?
        get() = _userMilkWithdrawal.value
        set(value) { _userMilkWithdrawal.update { value } }

    private val _selectedMilkWithdrawalUnits = MutableStateFlow<UnitOfMeasure?>(null)
    val selectedMilkWithdrawalUnits = _selectedMilkWithdrawalUnits.asStateFlow()

    private val _officialDrugDose = MutableStateFlow("")
    var officialDrugDose: String
        get() = _officialDrugDose.value
        set(value) { _officialDrugDose.update { value} }

    private val _userDrugDose = MutableStateFlow("")
    var userDrugDose: String
        get() = _userDrugDose.value
        set(value) { _userDrugDose.update { value } }

    private val _isDrugOffLabel = MutableStateFlow(false)
    val isDrugOffLabel = _isDrugOffLabel.asStateFlow()

    private val _selectedOffLabelVetContact = MutableStateFlow<Contact?>(null)
    val selectedOffLabelVetContact = _selectedOffLabelVetContact.asStateFlow()

    private val eventsChannel = Channel<Event>()
    val events = eventsChannel.receiveAsFlow()

    val canSave = combine19(
        _selectedDrugType, _officialDrugName, _genericDrugName,
        _drugLot, _drugCost, _selectedCurrency, _amountPurchased,
        _selectedPurchaseDate, _selectedExpirationDate,
        _meatWithdrawal, _userMeatWithdrawal, _selectedMeatWithdrawalUnits,
        _milkWithdrawal, _userMilkWithdrawal, _selectedMilkWithdrawalUnits,
        _officialDrugDose, _userDrugDose, _isDrugOffLabel, _selectedOffLabelVetContact
    ) { drugType, officialName, genericName, lot, cost, currency, amount,
        purchaseDate, expirationDate, meatWithdrawal, userMeatWithdrawal,
        meatWithdrawalUnits, withdrawalMilk, userMilkWithdrawal, milkWithdrawalUnits,
        officialDose, userDose, isOffLabel, offLabelVetContact ->

        val requiredValuesPresent = drugType != null && officialName.isNotBlank() && genericName.isNotBlank() &&
                lot.isNotBlank() && expirationDate != null && officialDose.isNotBlank() && userDose.isNotBlank()

        val offLabelValuesAreValid = (isOffLabel && offLabelVetContact != null) ||
                (!isOffLabel && offLabelVetContact == null)

        val isMeatWithdrawalExpected = meatWithdrawalUnits != null

        val isMeatWithdrawalValid = if (isMeatWithdrawalExpected)
            meatWithdrawal != null && 0 <= meatWithdrawal else meatWithdrawal == null

        val isMeatWithdrawalUserValid = if(isMeatWithdrawalExpected)
            userMeatWithdrawal == null ||
                    (meatWithdrawal != null && meatWithdrawal <= userMeatWithdrawal)
            else userMeatWithdrawal == null

        val isMilkWithdrawalExpected = milkWithdrawalUnits != null

        val isMilkWithdrawalValid = if (isMilkWithdrawalExpected)
            withdrawalMilk != null && 0 <= withdrawalMilk else withdrawalMilk == null

        val isMilkWithdrawalUserValid = if (isMilkWithdrawalExpected)
            userMilkWithdrawal == null ||
                    (withdrawalMilk != null && withdrawalMilk <= userMilkWithdrawal)
        else userMilkWithdrawal == null

        val isPurchaseCostValid = (cost == null && currency == null) ||
                (cost != null && 0.0f < cost && currency != null)

        requiredValuesPresent && offLabelValuesAreValid &&
                isMeatWithdrawalValid && isMeatWithdrawalUserValid &&
                isMilkWithdrawalValid && isMilkWithdrawalUserValid &&
                isPurchaseCostValid

    }.stateIn(viewModelScope, SharingStarted.Lazily, false)

    init {
        viewModelScope.launch {
            selectedMeatWithdrawalUnits.collectLatest {
                if (it == null) {
                    meatWithdrawal = null
                    userMeatWithdrawal = null
                    eventsChannel.send(
                        FieldValueChanged(
                            Field.MEAT_WITHDRAWAL,
                            Field.USER_MEAT_WITHDRAWAL
                        )
                    )
                }
            }
        }
        viewModelScope.launch {
            selectedMilkWithdrawalUnits.collectLatest {
                if (it == null) {
                    milkWithdrawal = null
                    userMilkWithdrawal = null
                    eventsChannel.send(
                        FieldValueChanged(
                            Field.MILK_WITHDRAWAL,
                            Field.USER_MILK_WITHDRAWAL
                        )
                    )
                }
            }
        }
        viewModelScope.launch {
            isDrugOffLabel.collectLatest {
                if (!it) {
                    _selectedOffLabelVetContact.update { null }
                }
            }
        }
    }

    fun selectDrugType(drugType: DrugType) {
        _selectedDrugType.update { drugType }
    }

    fun setIsRemovable(isRemovable: Boolean) {
        _isDrugRemovable.update { isRemovable }
    }

    fun selectCurrency(currency: UnitOfMeasure) {
        if (currency.type.id == UnitOfMeasure.Type.ID_CURRENCY) {
            _selectedCurrency.update { currency }
        }
    }

    fun selectPurchaseDate(purchaseDate: LocalDate) {
        _selectedPurchaseDate.update { purchaseDate }
    }

    fun selectExpirationDate(expirationDate: LocalDate) {
        _selectedExpirationDate.update { expirationDate }
    }

    fun selectMeatWithdrawalUnits(withdrawalUnits: UnitOfMeasure?) {
        if (withdrawalUnits == null || withdrawalUnits.type.id == UnitOfMeasure.Type.ID_TIME) {
            _selectedMeatWithdrawalUnits.update { withdrawalUnits }
        }
    }

    fun selectMilkWithdrawalUnits(withdrawalUnits: UnitOfMeasure?) {
        if (withdrawalUnits == null || withdrawalUnits.type.id == UnitOfMeasure.Type.ID_TIME) {
            _selectedMilkWithdrawalUnits.update { withdrawalUnits }
        }
    }

    fun setIsOffLabel(isOffLabel: Boolean) {
        _isDrugOffLabel.update { isOffLabel }
    }

    fun selectVetContact(contact: Contact) {
        _selectedOffLabelVetContact.update { contact }
    }

    fun save() {
        if (canSave.value) {
            viewModelScope.launch {
                executeSave()
            }
        }
    }

    private suspend fun executeSave() {

        val drugTypeId = selectedDrugType.value?.id ?: return
        val officialName = officialDrugName.trim().takeIf { it.isNotBlank() } ?: return
        val genericName = genericDrugName.trim().takeIf { it.isNotBlank() } ?: return
        val drugLot = drugLot.trim().takeIf { it.isNotBlank() } ?: return
        val officialDose = officialDrugDose.trim().takeIf { it.isNotBlank() } ?: return
        val userDose = userDrugDose.trim().takeIf { it.isNotBlank() } ?: ""
        val expirationDate = selectedExpirationDate.value ?: return
        val drugCost = drugCost
        val costCurrencyId = selectedCurrency.value?.id
        val amountPurchased = amountPurchased.trim().takeIf { it.isNotBlank() } ?: ""
        val purchaseDate = selectedPurchaseDate.value
        val isRemovable = isDrugRemovable.value
        val isOffLabel = isDrugOffLabel.value
        val vetContactId = selectedOffLabelVetContact.value?.id
        val meatWithdrawalUnitsId = selectedMeatWithdrawalUnits.value?.id
        val meatWithdrawal = meatWithdrawal ?: 0
        val userMeatWithdrawal = userMeatWithdrawal?.takeIf { meatWithdrawal < it } ?: meatWithdrawal
        val milkWithdrawalUnitsId = selectedMilkWithdrawalUnits.value?.id
        val milkWithdrawal = milkWithdrawal ?: 0
        val userMilkWithdrawal = userMilkWithdrawal?.takeIf { milkWithdrawal < it } ?: milkWithdrawal

        withContext(Dispatchers.IO) {
            try {
                drugRepository.addDrug(
                    drugTypeId = drugTypeId,
                    officialDrugName = officialName,
                    genericDrugName = genericName,
                    drugLot = drugLot,
                    officialDrugDose = officialDose,
                    userDrugDose = userDose,
                    expirationDate = expirationDate,
                    isRemovable = isRemovable,
                    isOffLabel = isOffLabel,
                    offLabelVetContactId = vetContactId,
                    meatWithdrawal = meatWithdrawal,
                    userMeatWithdrawal = userMeatWithdrawal,
                    meatWithdrawalUnitsId = meatWithdrawalUnitsId,
                    milkWithdrawal = milkWithdrawal,
                    userMilkWithdrawal = userMilkWithdrawal,
                    milkWithdrawalUnitsId = milkWithdrawalUnitsId,
                    cost = drugCost,
                    currencyUnitsId = costCurrencyId,
                    amountPurchased = amountPurchased,
                    purchaseDate = purchaseDate,
                    timeStamp = LocalDateTime.now()
                )
                eventsChannel.send(SaveSucceededEvent)
                clearData()
            } catch(ex: Exception) {
                eventsChannel.send(SaveFailedEvent)
            }
        }
    }

    private fun clearData() {
        officialDrugName = ""
        genericDrugName = ""
        drugLot = ""
        drugCost = null
        amountPurchased = ""
        meatWithdrawal = null
        userMeatWithdrawal = null
        milkWithdrawal = null
        userMilkWithdrawal = null
        officialDrugDose = ""
        userDrugDose = ""
        _selectedDrugType.update { null }
        _isDrugRemovable.update { false }
        _selectedCurrency.update { null }
        _selectedPurchaseDate.update { null }
        _selectedExpirationDate.update { null }
        _selectedMeatWithdrawalUnits.update { null }
        _selectedMilkWithdrawalUnits.update { null }
        _selectedOffLabelVetContact.update { null }
        _isDrugOffLabel.update { false }
        viewModelScope.launch {
            eventsChannel.send(
                FieldValueChanged()
            )
        }
    }
}
