package com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system

import android.content.Context
import android.net.Uri
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.files.AppDirectories
import com.weyr_associates.animaltrakkerfarmmobile.app.core.Result
import com.weyr_associates.animaltrakkerfarmmobile.app.core.versionName
import com.weyr_associates.animaltrakkerfarmmobile.app.settings.ActiveDefaultSettings
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseBackupError.SourceDatabaseFileNotFound
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseBackupError.UnableToWriteBackupDatabaseFile
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseLoadError.LoadedDatabaseFailedQueryCheck
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseLoadError.UnableToReadFromDatabaseSourceFile
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseLoadError.UnableToWriteLoadedDatabaseFile
import com.weyr_associates.animaltrakkerfarmmobile.database.AnimalTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.InputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

sealed interface DatabaseLoadError {
    data object UnableToReadFromDatabaseSourceFile : DatabaseLoadError
    data object UnableToWriteLoadedDatabaseFile : DatabaseLoadError
    data object LoadedDatabaseFailedQueryCheck : DatabaseLoadError
}

sealed interface DatabaseBackupError {
    data object SourceDatabaseFileNotFound : DatabaseBackupError
    data object UnableToWriteBackupDatabaseFile : DatabaseBackupError
}

class DatabaseManager(context: Context) {

    companion object {
        @JvmStatic
        fun getInstance(context: Context): DatabaseManager {
            if (INSTANCE == null) {
                synchronized(INSTANCE_LOCK) {
                    if (INSTANCE == null) {
                        INSTANCE = DatabaseManager(context)
                    }
                }
            }
            return requireNotNull(INSTANCE)
        }

        private var INSTANCE: DatabaseManager? = null
        private val INSTANCE_LOCK = Any()

        private const val FILE_NAME_ATRKKR_SEED_DATABASE = "AnimalTrakker_V5.1.5_Seed_Database.sqlite"

        private const val FILE_NAME_ATRKKR_WORKING_DATABASE = "animaltrakker_db.sqlite"
        private const val FILE_NAME_ATRKKR_WORKING_DATABASE_JOURNAL = "$FILE_NAME_ATRKKR_WORKING_DATABASE-journal"

        private val BACKUP_TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")
    }

    private val appContext = context.applicationContext

    private val databaseFile: File get() = appContext.getDatabasePath(
        FILE_NAME_ATRKKR_WORKING_DATABASE
    )

    fun isDatabasePresent(): Boolean {
        return DatabaseHandler.isDatabasePresent(appContext)
    }

    fun isDatabaseFilePresent(): Boolean {
        return databaseFile.exists()
    }

    suspend fun loadSeedDatabase(): Result<Long, DatabaseLoadError> {
        return withContext(Dispatchers.IO) {
            try {
                appContext.assets.open(FILE_NAME_ATRKKR_SEED_DATABASE).use { inputStream ->
                    loadDatabaseFromInputStream(inputStream)
                }
            } catch(ex: Exception) {
                Result.Failure(UnableToReadFromDatabaseSourceFile)
            }
        }
    }

    suspend fun loadDatabaseFromUri(uri: Uri): Result<Long, DatabaseLoadError> {
        return withContext(Dispatchers.IO) {
            try {
                appContext.contentResolver.openInputStream(uri)?.use { inputStream ->
                    loadDatabaseFromInputStream(inputStream)
                } ?: Result.Failure(UnableToReadFromDatabaseSourceFile)
            } catch (ex: Exception) {
                return@withContext Result.Failure(UnableToReadFromDatabaseSourceFile)
            }
        }
    }

    suspend fun backupDatabaseToDocuments(): Result<File, DatabaseBackupError> {
        return withContext(Dispatchers.IO) {
            if (!databaseFile.exists()) {
                return@withContext Result.Failure(SourceDatabaseFileNotFound)
            }
            val dstFile = AppDirectories.databaseBackupsDirectory(appContext)
                .childAt(getDatabaseBackupFileName())
            try {
                databaseFile.copyTo(dstFile, overwrite = true)
                return@withContext Result.Success(dstFile)
            } catch(ex: Exception) {
                return@withContext Result.Failure(
                    when (ex) {
                        is NoSuchFileException -> SourceDatabaseFileNotFound
                        else -> UnableToWriteBackupDatabaseFile
                    }
                )
            }
        }
    }

    private fun loadDatabaseFromInputStream(inputStream: InputStream): Result<Long, DatabaseLoadError> {
        try {
            databaseFile.outputStream().use { outputStream ->
                inputStream.copyTo(outputStream)
            }
        } catch (ex: Exception) {
            return Result.Failure(UnableToWriteLoadedDatabaseFile)
        }
        try {
            DatabaseHandler.create(appContext).use { databaseHandler ->
                databaseHandler.readableDatabase.rawQuery(
                    AnimalTable.Sql.QUERY_COUNT_ALL_ANIMALS,
                    emptyArray()
                ).use { cursor ->
                    if (!cursor.moveToFirst()) {
                        return Result.Failure(LoadedDatabaseFailedQueryCheck)
                    }
                    return Result.Success(cursor.getLong(0))
                }
            }
        } catch (ex: Exception) {
            return Result.Failure(LoadedDatabaseFailedQueryCheck)
        }
    }

    private fun getDatabaseBackupFileName(): String {
        val activeDefaultSettingsId = ActiveDefaultSettings.from(appContext)
            .loadActiveDefaultSettingsId()
        val appVersion = appContext.versionName
        val timeStamp = LocalDateTime.now().format(BACKUP_TIMESTAMP_FORMAT)
        val (dbVersion, serialNumber) = DatabaseHandler.create(appContext).use {
            Pair(it.queryUserVersion(), it.querySerialNumber(activeDefaultSettingsId.toLong()))
        }
        return buildString {
            append(timeStamp)
            if (serialNumber != 0) {
                append("_AT_SERIAL_${serialNumber}")
            }
            if (dbVersion != -1L) {
                append("_DB${dbVersion}")
            }
            append("_VER_${appVersion}.sqlite")
        }
    }

    private fun File.childAt(path: String): File {
        return File(this, path)
    }
}
