package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import android.content.ContentValues
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.DrugRepository
import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.DrugTable
import com.weyr_associates.animaltrakkerfarmmobile.database.DrugTypeTable
import com.weyr_associates.animaltrakkerfarmmobile.database.Sql
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime

class DrugRepositoryImpl(private val databaseHandler: DatabaseHandler) : DrugRepository {
    override suspend fun queryDrugTypes(): List<DrugType> {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                DrugTypeTable.Sql.QUERY_ALL_DRUG_TYPES,
                emptyArray()
            )?.use { cursor ->
                cursor.readAllItems(DrugTypeTable::drugTypeFromCursor)
            } ?: emptyList()
        }
    }

    override suspend fun queryDrugsByType(drugTypeId: Int): List<Drug> {
        return withContext(Dispatchers.IO) {
            databaseHandler.readableDatabase.rawQuery(
                DrugTable.Sql.QUERY_AVAILABLE_DRUGS_BY_TYPE,
                arrayOf(drugTypeId.toString())
            )?.use { cursor ->
                cursor.readAllItems(DrugTable::drugFromCursor)
            } ?: emptyList()
        }
    }

    override suspend fun addDrug(
        drugTypeId: Int,
        officialDrugName: String,
        genericDrugName: String,
        drugLot: String,
        officialDrugDose: String,
        userDrugDose: String,
        expirationDate: LocalDate,
        isRemovable: Boolean,
        isOffLabel: Boolean,
        offLabelVetContactId: Int?,
        meatWithdrawal: Int?,
        userMeatWithdrawal: Int?,
        meatWithdrawalUnitsId: Int?,
        milkWithdrawal: Int?,
        userMilkWithdrawal: Int?,
        milkWithdrawalUnitsId: Int?,
        cost: Float?,
        currencyUnitsId: Int?,
        amountPurchased: String,
        purchaseDate: LocalDate?,
        timeStamp: LocalDateTime
    ): Long {
        return databaseHandler.writableDatabase.run {
            val timeStampString = Sql.formatDateTime(timeStamp)
            val rowId: Long
            beginTransaction()
            try {
                rowId = insertOrThrow(
                    DrugTable.NAME,
                    null,
                    ContentValues().apply {
                        put(DrugTable.Columns.TYPE_ID, drugTypeId)
                        put(DrugTable.Columns.OFFICIAL_NAME, officialDrugName)
                        put(DrugTable.Columns.GENERIC_NAME, genericDrugName)
                        put(DrugTable.Columns.LOT, drugLot)
                        put(DrugTable.Columns.OFFICIAL_DRUG_DOSAGE, officialDrugDose)
                        put(DrugTable.Columns.USER_DRUG_DOSAGE, userDrugDose)
                        put(DrugTable.Columns.EXPIRATION_DATE, Sql.formatDate(expirationDate))
                        put(DrugTable.Columns.IS_REMOVABLE, Sql.booleanValue(isRemovable))
                        put(DrugTable.Columns.OFF_LABEL, Sql.booleanValue(isOffLabel))
                        offLabelVetContactId?.let {
                            put(DrugTable.Columns.OFF_LABEL_VET_CONTACT_ID, it)
                        } ?: put(DrugTable.Columns.OFF_LABEL_VET_CONTACT_ID, Sql.NULLISH)
                        meatWithdrawal?.let {
                            put(DrugTable.Columns.MEAT_WITHDRAWAL, it)
                        } ?: put(DrugTable.Columns.MEAT_WITHDRAWAL, 0)
                        userMeatWithdrawal?.let {
                            put(DrugTable.Columns.USER_MEAT_WITHDRAWAL, it)
                        } ?: put(DrugTable.Columns.USER_MEAT_WITHDRAWAL, 0)
                        meatWithdrawalUnitsId?.let {
                            put(DrugTable.Columns.MEAT_WITHDRAWAL_UNITS_ID, it)
                        } ?: put(DrugTable.Columns.MEAT_WITHDRAWAL_UNITS_ID, Sql.NULLISH)
                        milkWithdrawal?.let {
                            put(DrugTable.Columns.MILK_WITHDRAWAL, it)
                        } ?: put(DrugTable.Columns.MILK_WITHDRAWAL, 0)
                        userMilkWithdrawal?.let {
                            put(DrugTable.Columns.USER_MILK_WITHDRAWAL, it)
                        } ?: put(DrugTable.Columns.USER_MILK_WITHDRAWAL, 0)
                        milkWithdrawalUnitsId?.let {
                            put(DrugTable.Columns.MILK_WITHDRAWAL_UNITS_ID, it)
                        } ?: put(DrugTable.Columns.MILK_WITHDRAWAL_UNITS_ID, Sql.NULLISH)
                        cost?.let {
                            put(DrugTable.Columns.COST, it)
                        } ?: put(DrugTable.Columns.COST, Sql.NULLISH)
                        currencyUnitsId?.let {
                            put(DrugTable.Columns.COST_UNITS_ID, it)
                        } ?: put(DrugTable.Columns.COST_UNITS_ID, Sql.NULLISH)
                        put(DrugTable.Columns.AMOUNT_PURCHASED, amountPurchased)
                        purchaseDate?.let {
                            put(DrugTable.Columns.PURCHASE_DATE, Sql.formatDate(it))
                        } ?: put(DrugTable.Columns.PURCHASE_DATE, Sql.NULLISH)
                        put(DrugTable.Columns.DISPOSE_DATE, Sql.NULLISH)
                        put(DrugTable.Columns.IS_GONE, Sql.booleanValue(false))
                        put(DrugTable.Columns.USER_TASK_NAME, Sql.NULLISH)
                        put(DrugTable.Columns.CREATED, timeStampString)
                        put(DrugTable.Columns.MODIFIED, timeStampString)
                    }
                )
                setTransactionSuccessful()
            } finally {
                endTransaction()
            }
            rowId
        }
    }
}
