package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.OwnerUnion
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.OwnerRepository

class OwnerRepositoryImpl(private val databaseHandler: DatabaseHandler) :
    com.weyr_associates.animaltrakkerfarmmobile.app.repository.OwnerRepository {

    override fun queryOwners(): List<Owner> {
        return databaseHandler.readableDatabase.rawQuery(OwnerUnion.Sql.SQL_QUERY_ALL_OWNERS, null)
            ?.use { it.readAllItems(OwnerUnion::ownerFromCursor) } ?: emptyList()
    }

    override fun queryOwner(ownerId: Int, typeId: Int): Owner? {
        return databaseHandler.readableDatabase.rawQuery(
            OwnerUnion.Sql.SQL_QUERY_OWNER_BY_ID_AND_TYPE,
            arrayOf(ownerId.toString(), typeId.toString())
        )?.use { it.readFirstItem(OwnerUnion::ownerFromCursor) }
    }
}
