package com.weyr_associates.animaltrakkerfarmmobile.app.repository.database

import com.weyr_associates.animaltrakkerfarmmobile.app.storage.database.system.DatabaseHandler
import com.weyr_associates.animaltrakkerfarmmobile.database.IdTypeTable
import com.weyr_associates.animaltrakkerfarmmobile.database.readAllItems
import com.weyr_associates.animaltrakkerfarmmobile.database.readFirstItem
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType
import com.weyr_associates.animaltrakkerfarmmobile.app.repository.IdTypeRepository

class IdTypeRepositoryImpl(private val databaseHandler: DatabaseHandler) :
    com.weyr_associates.animaltrakkerfarmmobile.app.repository.IdTypeRepository {

    companion object {
        const val SQL_QUERY_ID_TYPES =
            """SELECT * FROM ${IdTypeTable.NAME}
                WHERE ${IdTypeTable.Columns.ID} != ${IdType.ID_TYPE_ID_NAME}
                ORDER BY ${IdTypeTable.Columns.ORDER}"""

        const val SQL_QUERY_ID_TYPES_FOR_SEARCH =
            """SELECT * FROM ${IdTypeTable.NAME}
                ORDER BY ${IdTypeTable.Columns.ORDER}"""

        const val SQL_QUERY_ID_TYPE_BY_ID =
            """SELECT * FROM ${IdTypeTable.NAME}
                WHERE ${IdTypeTable.Columns.ID} = ?"""
    }

    override fun queryIdTypes(): List<IdType> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_TYPES, null)
            ?.use { it.readAllItems(itemReader = IdTypeTable::idTypeFromCursor) } ?: emptyList()
    }

    override fun queryIdTypesForSearch(): List<IdType> {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_TYPES_FOR_SEARCH, null)
            ?.use { it.readAllItems(itemReader = IdTypeTable::idTypeFromCursor) } ?: emptyList()
    }

    override fun queryForIdType(id: Int): IdType? {
        return databaseHandler.readableDatabase.rawQuery(SQL_QUERY_ID_TYPE_BY_ID, arrayOf(id.toString()))
            ?.use { it.readFirstItem(itemReader = IdTypeTable::idTypeFromCursor) }
    }
}
