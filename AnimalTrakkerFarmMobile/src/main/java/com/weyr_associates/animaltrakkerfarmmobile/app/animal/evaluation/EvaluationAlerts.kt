package com.weyr_associates.animaltrakkerfarmmobile.app.animal.evaluation

import android.annotation.SuppressLint
import com.weyr_associates.animaltrakkerfarmmobile.model.BasicEvalTrait
import com.weyr_associates.animaltrakkerfarmmobile.model.CustomEvalTrait
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTrait
import com.weyr_associates.animaltrakkerfarmmobile.model.EvalTraitOption
import com.weyr_associates.animaltrakkerfarmmobile.model.SavedEvaluation
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitsEvalTrait

object EvaluationAlerts {
    fun simpleSort(evaluationEntries: EvaluationEntries): String {
        val sortOptionId = evaluationEntries.extractOptionIdForOptionTraitId(
            EvalTrait.OPTION_TRAIT_ID_SIMPLE_SORT
        )
        return when (sortOptionId) {
            EvalTraitOption.ID_SIMPLE_SORT_KEEP -> "Keep"
            EvalTraitOption.ID_SIMPLE_SORT_SHIP -> "Ship"
            EvalTraitOption.ID_SIMPLE_SORT_CULL -> "Cull"
            EvalTraitOption.ID_SIMPLE_SORT_OTHER -> "Other"
            else -> "Unknown Sort"
        }
    }

    fun simpleLambing(evaluationEntries: EvaluationEntries): String {
        return with(evaluationEntries) {
            val numWetherLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_WETHER_LAMBS_BORN)
            val numEweLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_EWE_LAMBS_BORN)
            val numRamLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_RAM_LAMBS_BORN)
            val numUnkSexLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_UNK_SEX_LAMBS_BORN)
            val numStillbornLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_STILL_BORN_LAMBS_BORN)
            val numAbortedLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_ABORTED_LAMBS)
            val numAdoptedLambs = extractScoreForTraitId(EvalTrait.TRAIT_ID_ADOPTED_LAMBS)

            buildString {
                appendLine("Lambed Already")
                appendLine("$numWetherLambs Wether Lambs")
                appendLine("$numEweLambs Ewe Lambs")
                appendLine("$numRamLambs Ram Lambs")
                appendLine("$numUnkSexLambs Unknown Sex Lambs")
                appendLine("$numStillbornLambs Stillborn Lambs")
                appendLine("$numAbortedLambs Aborted Lambs")
                append("$numAdoptedLambs Adopted Lambs")
            }
        }
    }

    fun summarizeEvaluation(evaluation: SavedEvaluation, evaluationEntries: EvaluationEntries): String {
        return buildString {
            appendLine("-- ${evaluation.name} --")
            addAlertLineItemFor(evaluation.trait01, evaluationEntries.trait01Score)
            addAlertLineItemFor(evaluation.trait02, evaluationEntries.trait02Score)
            addAlertLineItemFor(evaluation.trait03, evaluationEntries.trait03Score)
            addAlertLineItemFor(evaluation.trait04, evaluationEntries.trait04Score)
            addAlertLineItemFor(evaluation.trait05, evaluationEntries.trait05Score)
            addAlertLineItemFor(evaluation.trait06, evaluationEntries.trait06Score)
            addAlertLineItemFor(evaluation.trait07, evaluationEntries.trait07Score)
            addAlertLineItemFor(evaluation.trait08, evaluationEntries.trait08Score)
            addAlertLineItemFor(evaluation.trait09, evaluationEntries.trait09Score)
            addAlertLineItemFor(evaluation.trait10, evaluationEntries.trait10Score)
            addAlertLineItemFor(evaluation.trait11, evaluationEntries.trait11Score)
            addAlertLineItemFor(evaluation.trait12, evaluationEntries.trait12Score)
            addAlertLineItemFor(evaluation.trait13, evaluationEntries.trait13Score)
            addAlertLineItemFor(evaluation.trait14, evaluationEntries.trait14Score)
            addAlertLineItemFor(evaluation.trait15, evaluationEntries.trait15Score)
            addAlertLineItemFor(evaluation.trait16, evaluationEntries.trait16OptionId)
            addAlertLineItemFor(evaluation.trait17, evaluationEntries.trait17OptionId)
            addAlertLineItemFor(evaluation.trait18, evaluationEntries.trait18OptionId)
            addAlertLineItemFor(evaluation.trait19, evaluationEntries.trait19OptionId)
            addAlertLineItemFor(evaluation.trait20, evaluationEntries.trait20OptionId)
        }
    }

    private fun StringBuilder.addAlertLineItemFor(basicEvalTrait: BasicEvalTrait, traitScore: Int) {
        if (!basicEvalTrait.isEmpty) { appendLine("${basicEvalTrait.name}: $traitScore") }
    }

    @SuppressLint("DefaultLocale")
    private fun StringBuilder.addAlertLineItemFor(unitsEvalTrait: UnitsEvalTrait, traitScore: Float) {
        if (!unitsEvalTrait.isEmpty) {
            val traitScoreFormatted = String.format("%.2f", traitScore)
            appendLine(
                "${unitsEvalTrait.name}: $traitScoreFormatted ${unitsEvalTrait.units.abbreviation}"
            )
        }
    }

    private fun StringBuilder.addAlertLineItemFor(optionEvalTrait: CustomEvalTrait, traitOptionId: Int) {
        if (!optionEvalTrait.isEmpty) {
            val optionName = optionEvalTrait.options.firstOrNull { it.id == traitOptionId }?.name ?: ""
            appendLine("${optionEvalTrait.name}: $optionName")
        }
    }
}
