package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalNoteTable {

    const val NAME = "animal_note_table"

    object Columns {
        const val ID = "id_animalnoteid"
        const val ANIMAL_ID = "id_animalid"
        const val NOTE_TEXT = "note_text"
        const val NOTE_DATE = "note_date"
        const val NOTE_TIME = "note_time"
        const val PREDEFINED_NOTE_ID = "id_predefinednotesid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
