package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugType

object DrugTypeTable {

    const val NAME = "drug_type_table"

    fun drugTypeFromCursor(cursor: Cursor): DrugType {
        return DrugType(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_drugtypeid"
        const val NAME = "drug_type"
        const val ORDER = "drug_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_ALL_DRUG_TYPES =
            """SELECT * FROM ${DrugTypeTable.NAME}
                ORDER BY ${Columns.ORDER}"""
    }
}
