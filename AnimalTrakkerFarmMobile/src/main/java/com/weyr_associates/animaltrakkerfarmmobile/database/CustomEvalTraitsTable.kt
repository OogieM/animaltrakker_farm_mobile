package com.weyr_associates.animaltrakkerfarmmobile.database

object CustomEvalTraitsTable {

    const val NAME = "custom_evaluation_traits_table"

    object Columns {
        const val ID = "id_customevaluationtraitsid"
        const val TRAIT_ID = "id_evaluationtraitid"
        const val ITEM = "custom_evaluation_item"
        const val ORDER = "custom_evaluation_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
