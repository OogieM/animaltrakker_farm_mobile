@file:JvmName("Cursors")

package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

/**
 * Treats underlying integer values as booleans.
 *
 * @param columnIndex The index of the column from which to retrieve a boolean value.
 *
 * @return True if the underlying integer is greater than 0; false otherwise.
 */
fun Cursor.getBoolean(columnIndex: Int): Boolean = 0 < getInt(columnIndex)

/**
 * Retrieves a boolean value from the [Cursor] based on column name.  Relies
 * on an underlying integral value in the column.  Should only be called with
 * columns that are guaranteed to be non-null.
 *
 * @param columnName The column name from which to retrieve a boolean value.
 * @return True if the underlying integer is greater than 0; false otherwise.
 */
fun Cursor.getBoolean(columnName: String): Boolean =
    getBoolean(getColumnIndexOrThrow(columnName))

/**
 * Retrieves a boolean value from the [Cursor] based on column name.  Relies
 * on an underlying integral value in the column.  Can be called for columns
 * that allow null values.
 *
 * @param columnName The column name from which to retrieve a boolean value.
 * @return True if the underlying integer is greater than 0; false if it is less
 * than or equal to 0, and null if the underlying value is null.
 */
fun Cursor.getOptBoolean(columnName: String): Boolean? =
    nullOrValueAtIndex(columnName) { columnIndex -> getBoolean(columnIndex) }

/**
 * Retrieves an integer value from the [Cursor] based on column name.
 * Should only be called with columns that are guaranteed to be non-null.
 *
 * @param columnName The column name from which to retrieve an integer value.
 * @return The integer value for the given [columnName]
 */
fun Cursor.getInt(columnName: String): Int =
    getInt(getColumnIndexOrThrow(columnName))

/**
 * Retrieves an integer value from the [Cursor] based on column name.
 * Can be called for columns that allow null values.
 *
 * @param columnName The column name from which to retrieve an integer value.
 * @return The integer value for the given [columnName], or null if the
 * underlying value is null.
 */
fun Cursor.getOptInt(columnName: String): Int? =
    nullOrValueAtIndex(columnName) { columnIndex -> getInt(columnIndex) }

/**
 * Retrieves a float value from the [Cursor] based on column name.
 * Should only be called with columns that are guaranteed to be non-null.
 *
 * @param columnName The column name from which to retrieve a float value.
 * @return The float value for the given [columnName]
 */
fun Cursor.getFloat(columnName: String): Float =
    getFloat(getColumnIndexOrThrow(columnName))

/**
 * Retrieves a float value from the [Cursor] based on column name.
 * Can be called for columns that allow null values.
 *
 * @param columnName The column name from which to retrieve an float value.
 * @return The float value for the given [columnName], or null if the
 * underlying value is null.
 */
fun Cursor.getOptFloat(columnName: String): Float? =
    nullOrValueAtIndex(columnName) { columnIndex -> getFloat(columnIndex) }

/**
 * Retrieves a string value from the [Cursor] based on column name.
 * Should only be called with columns that are guaranteed to be non-null.
 *
 * @param columnName The column name from which to retrieve a string value.
 * @return The string value for the given [columnName]
 */
fun Cursor.getString(columnName: String): String =
    getString(getColumnIndexOrThrow(columnName))

fun Cursor.getLocalDate(columnName: String): LocalDate {
    return LocalDate.parse(getString(columnName), Sql.FORMAT_DATE)
}

fun Cursor.getOptLocalDate(columnName: String): LocalDate? {
    return if (!isNull(columnName)) {
        getString(columnName).takeIf { it.isNotBlank() }
            ?.let { LocalDate.parse(it, Sql.FORMAT_DATE) }
    } else { null }
}

fun Cursor.getLocalTime(columnName: String): LocalTime {
    return if (isNull(columnName)) Sql.DEFAULT_TIME
    else getString(columnName).takeIf { it.isNotEmpty() }
        ?.let { LocalTime.parse(it, Sql.FORMAT_TIME) } ?: Sql.DEFAULT_TIME
}

fun Cursor.getLocalDateTime(columnName: String): LocalDateTime {
    return LocalDateTime.parse(getString(columnName), Sql.FORMAT_DATETIME)
}

/**
 * Retrieves a string value from the [Cursor] based on column name.
 * Can be called for columns that allow null values.
 *
 * @param columnName The column name from which to retrieve an string value.
 * @return The string value for the given [columnName], or null if the
 * underlying value is null.
 */
fun Cursor.getOptString(columnName: String): String? =
    nullOrValueAtIndex(columnName) { columnIndex -> getString(columnIndex) }

/**
 * Indicates whether or not the specified column
 * contains a null value.
 *
 * @param columnName The column name to check for null values.
 * @return true if the column contains a null value, false if
 * it does not.
 */
fun Cursor.isNull(columnName: String): Boolean {
    return isNull(getColumnIndexOrThrow(columnName))
}
/**
 * Converts column name to index and checks if the column is null.
 * This is a private helper to reduce boiler plate for providing
 * optional value lookup in [Cursor]s.
 *
 * @param columnName The name of the column in question.
 *
 * @param accessor A block given the resulting index of the column
 * to look up a value in the case a value is present.
 *
 * @return null if column is null, otherwise result of [accessor]
 */
private fun <T> Cursor.nullOrValueAtIndex(columnName: String, accessor: Cursor.(Int) -> T): T? {
    val columnIndex = getColumnIndexOrThrow(columnName)
    return if (!isNull(columnIndex)) accessor.invoke(this, columnIndex) else null
}

fun <T> Cursor.readAllItems(itemReader: (Cursor) -> T): List<T> {
    return takeIf { it.moveToFirst() }?.readList(itemReader) ?: emptyList()
}

fun <T> Cursor.readFirstItem(itemReader: (Cursor) -> T): T? {
    return takeIf { it.moveToFirst() }?.readItem(itemReader)
}

fun <T> Cursor.readList(itemReader: (Cursor) -> T): List<T> {
    assert(!isBeforeFirst && !isAfterLast)
    return buildList {
        do {
            add(readItem(itemReader))
        }
        while (moveToNext())
    }
}

fun <T> Cursor.readItem(itemReader: (Cursor) -> T): T {
    assert(!isBeforeFirst && !isAfterLast)
    return itemReader.invoke(this)
}
