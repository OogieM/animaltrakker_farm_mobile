package com.weyr_associates.animaltrakkerfarmmobile.database

object CompanyPremiseTable {

    const val NAME = "company_premise_table"

    object Columns {
        const val ID = "id_companypremiseid"
        const val COMPANY_ID = "id_companyid"
        const val PREMISE_ID = "id_premiseid"
        const val USAGE_START = "start_premise_use"
        const val USAGE_END = "end_premise_use"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
