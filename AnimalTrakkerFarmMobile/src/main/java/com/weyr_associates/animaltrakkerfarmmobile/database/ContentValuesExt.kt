package com.weyr_associates.animaltrakkerfarmmobile.database

import android.content.ContentValues

fun ContentValues.putOrNullish(columnName: String, value: Int, shouldPutValue: Boolean) {
    if (shouldPutValue) {
        put(columnName, value)
    } else {
        put(columnName, Sql.NULLISH)
    }
}

fun ContentValues.putOrNullish(columnName: String, value: Int, shouldPutValue: () -> Boolean) {
    putOrNullish(columnName, value, shouldPutValue())
}
