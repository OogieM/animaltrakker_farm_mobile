package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalFlockPrefixTable {

    const val NAME = "animal_flock_prefix_table"

    object Columns {
        const val ID = "id_animalflockprefixid"
        const val ANIMAL_ID = "id_animalid"
        const val FLOCK_PREFIX_ID = "id_flockprefixid"
        const val REGISTRY_COMPANY_ID = "id_registry_id_companyid"
    }
}
