package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalRegistrationTable {

    const val NAME = "animal_registration_table"

    object Columns {
        const val ID = "id_animalregistrationid"
        const val ANIMAL_ID = "id_animalid"
        const val ANIMAL_NAME = "animal_name"
        const val REGISTRATION_NUMBER = "registration_number"
        const val ID_REGISTRY_COMPANY_ID = "id_registry_id_companyid"
        const val ANIMAL_REGISTRATION_TYPE_ID = "id_animalregistrationtypeid"
        const val FLOCK_BOOK_ID = "id_flockbookid"
        const val REGISTRATION_DATE = "registration_date"
        const val REGISTRATION_DESCRIPTION = "registration_description"
        const val BREEDER_CONTACT_ID = "id_breeder_id_contactid"
        const val BREEDER_COMPANY_ID = "id_breeder_id_companyid"
        const val CREATED = "created"
        const val MODIFIED = "modified"

        const val BREEDER_ID = "id_breederid"
        const val BREEDER_NAME = "breeder_name"
        const val BREEDER_TYPE_ID = "breeder_type_id"
    }
}
