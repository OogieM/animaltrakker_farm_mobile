package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.UnitOfMeasure

object UnitsTable {

    const val NAME = "units_table"

    fun unitOfMeasureFromCursor(cursor: Cursor): UnitOfMeasure {
        return UnitOfMeasure(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            type = UnitOfMeasure.Type(
                id = cursor.getInt(UnitsTypeTable.Columns.ID),
                name = cursor.getString(UnitsTypeTable.Columns.NAME)
            ),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_unitsid"
        const val NAME = "units_name"
        const val ABBREVIATION = "units_abbrev"
        const val TYPE_ID = "id_unitstypeid"
        const val ORDER = "units_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_UNITS_OF_MEASURE =
            """SELECT * FROM ${UnitsTable.NAME}
                JOIN ${UnitsTypeTable.NAME} ON
                    ${UnitsTypeTable.NAME}.${UnitsTypeTable.Columns.ID} =
                    ${UnitsTable.NAME}.${UnitsTable.Columns.TYPE_ID}
                ORDER BY ${Columns.ORDER}"""

        const val QUERY_UNITS_OF_MEASURE_BY_TYPE =
            """SELECT * FROM ${UnitsTable.NAME}
                JOIN ${UnitsTypeTable.NAME} ON
                    ${UnitsTypeTable.NAME}.${UnitsTypeTable.Columns.ID} =
                    ${UnitsTable.NAME}.${UnitsTable.Columns.TYPE_ID}
                WHERE ${UnitsTable.NAME}.${UnitsTable.Columns.TYPE_ID} = ?
                ORDER BY ${Columns.ORDER}"""

        const val QUERY_UNIT_OF_MEASURE_BY_ID =
            """SELECT * FROM ${UnitsTable.NAME}
                JOIN ${UnitsTypeTable.NAME} ON
                    ${UnitsTypeTable.NAME}.${UnitsTypeTable.Columns.ID} =
                    ${UnitsTable.NAME}.${UnitsTable.Columns.TYPE_ID}
                WHERE ${UnitsTable.Columns.ID} = ?"""
    }
}
