package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.ItemEntry

object SavedEvaluationTable {

    const val NAME = "saved_evaluations_table"

    fun itemEntryForSavedEvaluationFromCursor(cursor: Cursor): ItemEntry {
        return ItemEntry(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME)
        )
    }

    object Columns {
        const val ID = "id_savedevaluationstableid"
        const val NAME = "evaluation_name"
        const val IS_SYSTEM_ONLY = "is_system_only"
        const val SUMMARIZE_IN_ALERT = "add_alert_summary"
        const val CONTACT_ID = "saved_evaluation_id_contactid"
        const val COMPANY_ID = "saved_evaluation_id_companyid"
        const val TRAIT_01_ID = "trait_name01"
        const val TRAIT_02_ID = "trait_name02"
        const val TRAIT_03_ID = "trait_name03"
        const val TRAIT_04_ID = "trait_name04"
        const val TRAIT_05_ID = "trait_name05"
        const val TRAIT_06_ID = "trait_name06"
        const val TRAIT_07_ID = "trait_name07"
        const val TRAIT_08_ID = "trait_name08"
        const val TRAIT_09_ID = "trait_name09"
        const val TRAIT_10_ID = "trait_name10"
        const val TRAIT_11_ID = "trait_name11"
        const val TRAIT_12_ID = "trait_name12"
        const val TRAIT_13_ID = "trait_name13"
        const val TRAIT_14_ID = "trait_name14"
        const val TRAIT_15_ID = "trait_name15"
        const val TRAIT_16_ID = "trait_name16"
        const val TRAIT_17_ID = "trait_name17"
        const val TRAIT_18_ID = "trait_name18"
        const val TRAIT_19_ID = "trait_name19"
        const val TRAIT_20_ID = "trait_name20"
        const val TRAIT_11_UNITS_ID = "trait_units11"
        const val TRAIT_12_UNITS_ID = "trait_units12"
        const val TRAIT_13_UNITS_ID = "trait_units13"
        const val TRAIT_14_UNITS_ID = "trait_units14"
        const val TRAIT_15_UNITS_ID = "trait_units15"
        const val TRAIT_01_OPTIONAL = "trait_name01_optional"
        const val TRAIT_02_OPTIONAL = "trait_name02_optional"
        const val TRAIT_03_OPTIONAL = "trait_name03_optional"
        const val TRAIT_04_OPTIONAL = "trait_name04_optional"
        const val TRAIT_05_OPTIONAL = "trait_name05_optional"
        const val TRAIT_06_OPTIONAL = "trait_name06_optional"
        const val TRAIT_07_OPTIONAL = "trait_name07_optional"
        const val TRAIT_08_OPTIONAL = "trait_name08_optional"
        const val TRAIT_09_OPTIONAL = "trait_name09_optional"
        const val TRAIT_10_OPTIONAL = "trait_name10_optional"
        const val TRAIT_11_OPTIONAL = "trait_name11_optional"
        const val TRAIT_12_OPTIONAL = "trait_name12_optional"
        const val TRAIT_13_OPTIONAL = "trait_name13_optional"
        const val TRAIT_14_OPTIONAL = "trait_name14_optional"
        const val TRAIT_15_OPTIONAL = "trait_name15_optional"
        const val TRAIT_16_OPTIONAL = "trait_name16_optional"
        const val TRAIT_17_OPTIONAL = "trait_name17_optional"
        const val TRAIT_18_OPTIONAL = "trait_name18_optional"
        const val TRAIT_19_OPTIONAL = "trait_name19_optional"
        const val TRAIT_20_OPTIONAL = "trait_name20_optional"
        const val TRAIT_01_DEFERRED = "trait_name01_deferred"
        const val TRAIT_02_DEFERRED = "trait_name02_deferred"
        const val TRAIT_03_DEFERRED = "trait_name03_deferred"
        const val TRAIT_04_DEFERRED = "trait_name04_deferred"
        const val TRAIT_05_DEFERRED = "trait_name05_deferred"
        const val TRAIT_06_DEFERRED = "trait_name06_deferred"
        const val TRAIT_07_DEFERRED = "trait_name07_deferred"
        const val TRAIT_08_DEFERRED = "trait_name08_deferred"
        const val TRAIT_09_DEFERRED = "trait_name09_deferred"
        const val TRAIT_10_DEFERRED = "trait_name10_deferred"
        const val TRAIT_11_DEFERRED = "trait_name11_deferred"
        const val TRAIT_12_DEFERRED = "trait_name12_deferred"
        const val TRAIT_13_DEFERRED = "trait_name13_deferred"
        const val TRAIT_14_DEFERRED = "trait_name14_deferred"
        const val TRAIT_15_DEFERRED = "trait_name15_deferred"
        const val TRAIT_16_DEFERRED = "trait_name16_deferred"
        const val TRAIT_17_DEFERRED = "trait_name17_deferred"
        const val TRAIT_18_DEFERRED = "trait_name18_deferred"
        const val TRAIT_19_DEFERRED = "trait_name19_deferred"
        const val TRAIT_20_DEFERRED = "trait_name20_deferred"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_SAVED_EVALUATIONS_FOR_CONTACT_USER =
            """SELECT * FROM ${SavedEvaluationTable.NAME}
                WHERE ${SavedEvaluationTable.Columns.IS_SYSTEM_ONLY} = 0 
                AND ${SavedEvaluationTable.Columns.CONTACT_ID} = ?
                ORDER BY ${SavedEvaluationTable.Columns.NAME}
            """

        const val QUERY_SAVED_EVALUATIONS_FOR_COMPANY_USER =
            """SELECT * FROM ${SavedEvaluationTable.NAME}
                WHERE ${SavedEvaluationTable.Columns.IS_SYSTEM_ONLY} = 0 
                AND ${SavedEvaluationTable.Columns.COMPANY_ID} = ?
                ORDER BY ${SavedEvaluationTable.Columns.NAME}
            """
    }
}
