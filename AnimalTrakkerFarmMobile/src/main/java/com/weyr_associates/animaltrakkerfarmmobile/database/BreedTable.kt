package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Breed

object BreedTable {

    const val NAME = "breed_table"

    @JvmStatic
    fun breedFromCursor(cursor: Cursor): Breed {
        return Breed(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER),
            speciedId = cursor.getInt(Columns.SPECIES_ID)
        )
    }

    object Columns {
        const val ID = "id_breedid"
        const val NAME = "breed_name"
        const val ABBREVIATION = "breed_abbrev"
        const val ORDER = "breed_display_order"
        const val SPECIES_ID = "id_speciesid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
