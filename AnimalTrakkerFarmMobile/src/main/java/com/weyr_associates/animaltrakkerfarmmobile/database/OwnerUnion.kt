package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Owner

object OwnerUnion {

    fun ownerFromCursor(cursor: Cursor): Owner {
        return Owner(
            id = cursor.getInt(Columns.ID),
            type = Owner.Type.fromId(cursor.getInt(Columns.TYPE)),
            name = cursor.getString(Columns.NAME),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Sql {
        const val SQL_QUERY_ALL_OWNERS =
            """SELECT ${Columns.ID},
                ${Columns.TYPE},
                ${Columns.NAME},
                ROW_NUMBER() OVER (
                    ORDER BY ${Columns.TYPE} DESC,
                        company_display_order ASC,
                        contact_display_order ASC
                ) AS ${Columns.ORDER}
                FROM (
                    SELECT ${CompanyTable.Columns.ID} AS ${Columns.ID},
                        CAST(1 AS INTEGER) AS ${Columns.TYPE},
                        ${CompanyTable.Columns.NAME} AS ${Columns.NAME},
                        ROW_NUMBER() OVER (
                            ORDER BY ${CompanyTable.Columns.NAME} ASC
                        ) AS company_display_order,
                        NULL as contact_display_order
                    FROM company_table
                    UNION
                    SELECT ${ContactTable.Columns.ID} AS ${Columns.ID},
                        CAST(0 AS INTEGER) AS ${Columns.TYPE},
                        ${ContactTable.Columns.FIRST_NAME} ||
                        ' ' ||
                        ${ContactTable.Columns.LAST_NAME} AS ${Columns.NAME},
                        NULL AS company_display_order,
                        ROW_NUMBER() OVER (
                            ORDER BY ${ContactTable.Columns.LAST_NAME} ASC,
                                ${ContactTable.Columns.FIRST_NAME} ASC
                        ) AS contact_display_order
                    FROM ${ContactTable.NAME}
                )"""

        const val SQL_QUERY_OWNER_BY_ID_AND_TYPE =
                    """SELECT * FROM ($SQL_QUERY_ALL_OWNERS)
                        WHERE ${Columns.ID} = ? AND ${Columns.TYPE} = ?"""
    }

    object Columns {
        const val ID = "id_ownerid"
        const val TYPE = "owner_type"
        const val NAME = "owner_name"
        const val ORDER = "owner_display_order"
    }
}
