package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalTissueSampleTakenTable {

    const val NAME = "animal_tissue_sample_taken_table"

    object Columns {
        const val ID = "id_animaltissuesampletakenid"
        const val ANIMAL_ID = "id_animalid"
        const val SAMPLE_TYPE_ID = "id_tissuesampletypeid"
        const val SAMPLE_DATE = "tissue_sample_date"
        const val SAMPLE_TIME = "tissue_sample_time"
        const val CONTAINER_TYPE_ID = "id_tissuesamplecontainertypeid"
        const val CONTAINER_ID = "tissue_sample_container_id"
        const val CONTAINER_EXP_DATE = "tissue_sample_container_exp_date"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
