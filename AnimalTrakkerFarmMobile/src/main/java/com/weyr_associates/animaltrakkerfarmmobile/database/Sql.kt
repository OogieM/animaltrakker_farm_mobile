package com.weyr_associates.animaltrakkerfarmmobile.database

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

object Sql {

    const val QUERY_SQLITE_USER_VERSION =
        """SELECT user_version FROM pragma_user_version"""

    const val QUERY_ATRKKR_SERIAL_NUMBER =
        """SELECT ${DefaultSettingsTable.Columns.USER_SYSTEM_SERIAL_NUMBER}
            FROM ${DefaultSettingsTable.NAME}
            WHERE ${DefaultSettingsTable.Columns.ID} = ?
            LIMIT 1"""
    /**
     * Use this as opposed to NULL, even for integral
     * columns that do not have a value or are otherwise nullish.
     * Keeps the codebase in sync with web and desktop database
     * manipulations.
     */
    const val NULLISH = ""
    const val ID_NULL = 0

    const val ESCAPE_CHAR = '\\'
    const val ESCAPE_CLAUSE = "ESCAPE '$ESCAPE_CHAR'"

    val FORMAT_DATE: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val FORMAT_TIME: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")
    val FORMAT_DATETIME: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    val DEFAULT_TIME = LocalTime.of(0, 0, 0)

    const val FLOAT_PRECISION_FOR_TRAITS = 2

    /**
     * Escapes SQLite LIKE wildcard characters and
     * the escape character itself.
     *
     * Use for arguments that will determine
     * the pattern for LIKE usages in Sql.
     */
    fun escapeWildcards(string: String): String {
        return string.replace(
            "$ESCAPE_CHAR",
            "$ESCAPE_CHAR$ESCAPE_CHAR"
        ).replace(
            "_",
            "${ESCAPE_CHAR}_"
        ).replace(
            "%",
            "$ESCAPE_CHAR%"
        )
    }

    fun booleanValue(value: Boolean): Int {
        return if (value) 1 else 0
    }

    fun formatDate(date: LocalDate): String {
        return date.format(FORMAT_DATE)
    }

    fun formatDate(dateTime: LocalDateTime): String {
        return dateTime.format(FORMAT_DATE)
    }

    fun formatTime(time: LocalTime): String {
        return time.format(FORMAT_TIME)
    }

    fun formatTime(dateTime: LocalDateTime): String {
        return dateTime.format(FORMAT_TIME)
    }

    fun formatDateTime(dateTime: LocalDateTime): String {
        return dateTime.format(FORMAT_DATETIME)
    }

    fun floatWithPrecision(value: Float, places: Int): String {
        require(0 < places)
        return String.format("%.${places}f" ,value)
    }

    fun floatForUnitEvalTrait(value: Float) = floatWithPrecision(value, FLOAT_PRECISION_FOR_TRAITS)
}
