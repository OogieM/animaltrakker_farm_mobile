package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.IdType

object IdTypeTable {

    const val NAME = "id_type_table"

    fun idTypeFromCursor(cursor: Cursor): IdType {
        return IdType(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_idtypeid"
        const val NAME = "id_type_name"
        const val ABBREVIATION = "id_type_abbrev"
        const val ORDER = "id_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
