package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.TissueSampleContainerType

object TissueSampleContainerTypeTable {

    const val NAME = "tissue_sample_container_type_table"

    fun tissueSampleContainerTypeFrom(cursor: Cursor): TissueSampleContainerType {
        return TissueSampleContainerType(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_tissuesamplecontainertypeid"
        const val NAME = "tissue_sample_container_name"
        const val ABBREVIATION = "tissue_sample_container_abbrev"
        const val ORDER = "tissue_sample_container_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPES =
            """SELECT * FROM ${TissueSampleContainerTypeTable.NAME}
                ORDER BY ${TissueSampleContainerTypeTable.Columns.ORDER}"""

        const val SQL_QUERY_TISSUE_SAMPLE_CONTAINER_TYPE_BY_ID =
            """SELECT * FROM ${TissueSampleContainerTypeTable.NAME}
                WHERE ${TissueSampleContainerTypeTable.Columns.ID} = ?"""
    }
}
