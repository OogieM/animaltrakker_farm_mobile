package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Species

object SpeciesTable {

    const val NAME = "species_table"

    fun speciesFromCursor(cursor: Cursor): Species {
        return Species(
            id = cursor.getInt(Columns.ID),
            commonName = cursor.getString(Columns.COMMON_NAME)
        )
    }

    object Columns {
        const val ID = "id_speciesid"
        const val COMMON_NAME = "species_common_name"
        const val GENERIC_NAME = "species_generic_name"
        const val SCIENTIFIC_FAMILY = "species_scientific_family"
        const val SCIENTIFIC_SUBFAMILY = "species_scientific_sub_family"
        const val SCIENTIFIC_NAME = "species_scientific_name"
        const val EARLY_MALE_BREEDING_AGE_IN_DAYS = "early_male_breeding_age_days"
        const val EARLY_FEMALE_BREEDING_AGE_IN_DAYS = "early_female_breeding_age_days"
        const val EARLY_GESTATION_LENGTH_DAYS = "early_gestation_length_days"
        const val LATE_GESTATION_LENGTH_DAYS = "late_gestation_length_days"
        const val TYPICAL_GESTATION_LENGTH_DAYS = "typical_gestation_length_days"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val SQL_QUERY_SPECIES_BY_ID =
            """SELECT * FROM ${SpeciesTable.NAME}
                WHERE ${SpeciesTable.Columns.ID} = ?"""
    }
}
