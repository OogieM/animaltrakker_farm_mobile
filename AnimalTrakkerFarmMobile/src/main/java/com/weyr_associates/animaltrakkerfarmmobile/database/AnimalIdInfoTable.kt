package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalIdInfoTable {

    const val NAME = "animal_id_info_table"

    object Columns {
        const val ID = "id_animalidinfoid"
        const val ANIMAL_ID = "id_animalid"
        const val ID_TYPE_ID = "id_idtypeid"
        const val MALE_ID_COLOR_ID = "id_male_id_idcolorid"
        const val FEMALE_ID_COLOR_ID = "id_female_id_idcolorid"
        const val ID_LOCATION_ID = "id_idlocationid"
        const val DATE_ON = "id_date_on"
        const val TIME_ON = "id_time_on"
        const val DATE_OFF = "id_date_off"
        const val TIME_OFF = "id_time_off"
        const val NUMBER = "id_number"
        const val SCRAPIE_FLOCK_ID = "id_scrapieflockid"
        const val IS_OFFICIAL_ID = "official_id"
        const val REMOVE_REASON_ID = "id_idremovereasonid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
