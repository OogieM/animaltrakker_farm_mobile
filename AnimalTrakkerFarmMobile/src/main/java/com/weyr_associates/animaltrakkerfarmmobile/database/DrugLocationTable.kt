package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.DrugLocation

object DrugLocationTable {

    const val NAME = "drug_location_table"

    fun drugLocationFromCursor(cursor: Cursor): DrugLocation {
        return DrugLocation(
            id = cursor.getInt(Columns.ID),
            name = cursor.getString(Columns.NAME),
            abbreviation = cursor.getString(Columns.ABBREVIATION),
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_druglocationid"
        const val NAME = "drug_location_name"
        const val ABBREVIATION = "drug_location_abbrev"
        const val ORDER = "drug_location_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_ALL_DRUG_LOCATIONS =
            """SELECT * FROM ${DrugLocationTable.NAME}"""
    }
}
