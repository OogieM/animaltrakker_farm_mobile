package com.weyr_associates.animaltrakkerfarmmobile.database

object FlockPrefixTable {

    const val NAME = "flock_prefix_table"

    object Columns {
        const val ID = "id_flockprefixid"
        const val PREFIX = "flock_prefix"
        const val OWNER_CONTACT_ID = "id_prefixowner_id_contactid"
        const val OWNER_COMPANY_ID = "id_prefixowner_id_companyid"
        const val REGISTRY_COMPANY_ID = "id_registry_id_companyid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
