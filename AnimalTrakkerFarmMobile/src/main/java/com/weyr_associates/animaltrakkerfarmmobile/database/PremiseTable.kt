package com.weyr_associates.animaltrakkerfarmmobile.database

object PremiseTable {

    const val NAME = "premise_table"

    object Columns {
        const val ID = "id_premiseid"
        const val TYPE_ID = "id_premisetypeid"

    }
}
