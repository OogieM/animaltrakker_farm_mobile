package com.weyr_associates.animaltrakkerfarmmobile.database

object UnitsTypeTable {

    const val NAME = "units_type_table"

    object Columns {
        const val ID = "id_unitstypeid"
        const val NAME = "unit_type_name"
        const val ORDER = "units_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
