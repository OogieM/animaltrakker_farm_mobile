package com.weyr_associates.animaltrakkerfarmmobile.database

object AnimalOwnershipHistoryTable {

    const val NAME = "animal_ownership_history_table"

    object Columns {
        const val ID = "id_animalownershiphistoryid"
        const val ANIMAL_ID = "id_animalid"
        const val TRANSFER_DATE = "transfer_date"
        const val FROM_CONTACT_ID = "from_id_contactid"
        const val TO_CONTACT_ID = "to_id_contactid"
        const val FROM_COMPANY_ID = "from_id_companyid"
        const val TO_COMPANY_ID = "to_id_companyid"
        const val TRANSFER_REASON_ID = "id_transferreasonid"
        const val SELL_PRICE = "sell_price"
        const val SELL_PRICE_UNITS_ID = "sell_price_id_unitsid"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {

        const val SQL_CURRENT_ANIMAL_OWNERSHIP =
            """SELECT ${Columns.ID}, 
	            ${Columns.ANIMAL_ID}, 
	            MAX(${Columns.TRANSFER_DATE})
                    AS ${Columns.TRANSFER_DATE},
                ${Columns.TO_CONTACT_ID},
                ${Columns.TO_COMPANY_ID},
                ${Columns.FROM_CONTACT_ID},
                ${Columns.FROM_COMPANY_ID},
                ${Columns.TRANSFER_REASON_ID}, 
                ${Columns.SELL_PRICE}, 
                ${Columns.SELL_PRICE_UNITS_ID}
            FROM $NAME
            GROUP BY ${Columns.ANIMAL_ID}"""

        const val SQL_CURRENT_ANIMAL_OWNERSHIP_BY_ANIMAL_ID =
            """SELECT ${Columns.ID}, 
	            ${Columns.ANIMAL_ID}, 
	            MAX(${Columns.TRANSFER_DATE})
                    AS ${Columns.TRANSFER_DATE},
                ${Columns.TO_CONTACT_ID},
                ${Columns.TO_COMPANY_ID},
                ${Columns.FROM_CONTACT_ID},
                ${Columns.FROM_COMPANY_ID},
                ${Columns.TRANSFER_REASON_ID}, 
                ${Columns.SELL_PRICE}, 
                ${Columns.SELL_PRICE_UNITS_ID}
            FROM $NAME
            WHERE ${Columns.ANIMAL_ID} = ?1
            GROUP BY ${Columns.ANIMAL_ID}"""
    }
}
