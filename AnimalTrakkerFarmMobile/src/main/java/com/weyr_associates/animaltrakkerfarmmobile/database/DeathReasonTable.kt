package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Company
import com.weyr_associates.animaltrakkerfarmmobile.model.DeathReason
import com.weyr_associates.animaltrakkerfarmmobile.model.UserType

object DeathReasonTable {

    const val NAME = "death_reason_table"

    fun deathReasonFromCursor(cursor: Cursor): DeathReason {
        val contactId = cursor.getInt(Columns.CONTACT_ID)
        val companyId = cursor.getInt(Columns.COMPANY_ID)
        return DeathReason(
            id = cursor.getInt(Columns.ID),
            reason = cursor.getString(Columns.REASON),
            userId = when {
                contactId != 0 -> contactId
                companyId != 0 -> companyId
                else -> throw IllegalStateException("Unable to determine user id. Contact and company are both 0.")
            },
            userType = when {
                contactId != 0 -> UserType.CONTACT
                companyId != 0 -> UserType.COMPANY
                else -> throw IllegalStateException("Unable to determine user type. Contact and company are both 0.")
            },
            order = cursor.getInt(Columns.ORDER)
        )
    }

    object Columns {
        const val ID = "id_deathreasonid"
        const val REASON = "death_reason"
        const val CONTACT_ID = "id_contactid"
        const val COMPANY_ID = "id_companyid"
        const val ORDER = "death_reason_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_DEATH_REASONS_ALL =
            """SELECT * FROM ${DeathReasonTable.NAME}"""

        const val ORDER_BY_DEATH_REASON_ORDER =
            """ORDER BY ${Columns.ORDER} ASC"""

        const val QUERY_DEATH_REASONS_FOR_CONTACT_USER_AND_DEFAULTS =
            """$QUERY_DEATH_REASONS_ALL
                WHERE ${Columns.CONTACT_ID} = ?
                    OR ${Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_DEATH_REASON_ORDER"""

        const val QUERY_DEATH_REASONS_FOR_COMPANY_USER_AND_DEFAULTS =
            """$QUERY_DEATH_REASONS_ALL
                WHERE ${Columns.COMPANY_ID} = ?
                    OR ${Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_DEATH_REASON_ORDER"""

        const val QUERY_DEATH_REASONS_DEFAULTS_ONLY =
            """$QUERY_DEATH_REASONS_ALL
                WHERE ${Columns.COMPANY_ID} = ${Company.ID_GENERIC}
                $ORDER_BY_DEATH_REASON_ORDER"""
    }
}
