package com.weyr_associates.animaltrakkerfarmmobile.database

object BirthTypeTable {

    const val NAME = "birth_type_table"

    object Columns {
        const val ID = "id_birthtypeid"
        const val NAME = "birth_type"
        const val ABBREVIATION = "birth_type_abbrev"
        const val ORDER = "birth_type_display_order"
        const val CREATED = "created"
        const val MODIFIED = "modified"

        const val REAR_TYPE_NAME = "rear_type_name"
    }
}
