package com.weyr_associates.animaltrakkerfarmmobile.database

import android.database.Cursor
import com.weyr_associates.animaltrakkerfarmmobile.model.Drug

object DrugTable {

    const val NAME = "drug_table"

    fun drugFromCursor(cursor: Cursor): Drug {
        return Drug(
            id = cursor.getInt(Columns.ID),
            typeId = cursor.getInt(Columns.TYPE_ID),
            officialName = cursor.getString(Columns.OFFICIAL_NAME),
            genericName = cursor.getString(Columns.GENERIC_NAME),
            lot = cursor.getString(Columns.LOT),
            isGone = cursor.getBoolean(Columns.IS_GONE),
            isRemovable = cursor.getBoolean(Columns.IS_REMOVABLE),
            meatWithdrawal = cursor.getInt(Columns.MEAT_WITHDRAWAL),
            meatWithdrawalUnitsId = cursor.getInt(Columns.MEAT_WITHDRAWAL_UNITS_ID),
            userMeatWithdrawal = cursor.getInt(Columns.USER_MEAT_WITHDRAWAL),
            milkWithdrawal = cursor.getInt(Columns.MILK_WITHDRAWAL),
            milkWithdrawalUnitsId = cursor.getInt(Columns.MILK_WITHDRAWAL_UNITS_ID),
            userMilkWithdrawal = cursor.getInt(Columns.USER_MILK_WITHDRAWAL),
            officialDrugDosage = cursor.getString(Columns.OFFICIAL_DRUG_DOSAGE),
            userDrugDosage = cursor.getString(Columns.USER_DRUG_DOSAGE),
            userTaskName = cursor.getString(Columns.USER_TASK_NAME)
        )
    }

    object Columns {
        const val ID = "id_drugid"
        const val TYPE_ID = "id_drugtypeid"
        const val OFFICIAL_NAME = "official_drug_name"
        const val GENERIC_NAME = "generic_drug_name"
        const val LOT = "drug_lot"
        const val EXPIRATION_DATE = "drug_expire_date"
        const val PURCHASE_DATE = "drug_purchase_date"
        const val AMOUNT_PURCHASED = "drug_amount_purchased"
        const val COST = "drug_cost"
        const val COST_UNITS_ID = "id_drug_cost_id_unitsid"
        const val DISPOSE_DATE = "drug_dispose_date"
        const val IS_GONE = "drug_gone"
        const val IS_REMOVABLE = "drug_removable"
        const val MEAT_WITHDRAWAL = "drug_meat_withdrawal"
        const val MEAT_WITHDRAWAL_UNITS_ID = "id_meat_withdrawal_id_unitsid"
        const val USER_MEAT_WITHDRAWAL = "user_meat_withdrawal"
        const val MILK_WITHDRAWAL = "drug_milk_withdrawal"
        const val MILK_WITHDRAWAL_UNITS_ID = "id_milk_withdrawal_id_unitsid"
        const val USER_MILK_WITHDRAWAL = "user_milk_withdrawal"
        const val OFFICIAL_DRUG_DOSAGE = "official_drug_dosage"
        const val OFF_LABEL = "off_label"
        const val OFF_LABEL_VET_CONTACT_ID = "off_label_vet_id_contactid"
        const val USER_DRUG_DOSAGE = "user_drug_dosage"
        const val USER_TASK_NAME = "user_task_name"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }

    object Sql {
        const val QUERY_AVAILABLE_DRUGS_BY_TYPE =
            """SELECT * FROM ${DrugTable.NAME}
                WHERE ${DrugTable.Columns.TYPE_ID} = ?
                AND ${DrugTable.Columns.IS_GONE} = 0"""
    }
}
