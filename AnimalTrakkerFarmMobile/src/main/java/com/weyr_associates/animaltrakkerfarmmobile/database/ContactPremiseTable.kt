package com.weyr_associates.animaltrakkerfarmmobile.database

object ContactPremiseTable {

    const val NAME = "contact_premise_table"

    object Columns {
        const val ID = "id_contactpremiseid"
        const val CONTACT_ID = "id_contactid"
        const val PREMISE_ID = "id_premiseid"
        const val USAGE_START = "start_premise_use"
        const val USAGE_END = "end_premise_use"
        const val CREATED = "created"
        const val MODIFIED = "modified"
    }
}
